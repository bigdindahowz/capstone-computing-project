# README #

The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
https://creativecommons.org/licenses/by/4.0/
You are free to Share - copy and redistribute the material in any medium or format
Adapt - remix, transform, and build upon the material
For any purpose, even commercially

### What is this repository for? ###

This repository was created for capstone project group 11.
Development took place semester 1 of 2018.
The project was to design and implement a system similar to blackboard for computing students.
The students can upload code for set assignemnts for units and the code would marked if possible and displayed to the lecturer/marker

### How do I get set up? ###

* api setup contained in api/README.md
* front end setup contained whiteboard/README.md
* sandbox setup contained on sandbox/README.md

* setup.sh
Run this script with root permissions.
Use 'api','website' or 'sandbox' arguments to instal the specific server type.

### Deployment instructions

It is avised that the api, website and sandbox run on 3 different servers as the load on 1 sever is too great just for one server.
This is due to the sandbox requiring large amount of resources.

