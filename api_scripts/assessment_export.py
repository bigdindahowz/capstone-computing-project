#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

"""
    The following module is used to export a list of student submissions
    It is designed to output the submissions into a specific direcoty structure dependant
        on the assessment and the student_id
    Currently it uses the assessment_id to get all related assessments

    It will output the assessments in a directory struture as such:
        -assessment_id
            -student_id
                -code_file
            -student_id
                -code_file
            -student_id
                -code_file
            -etc.....

"""
import sys
import os
import time
import requests
import shutil

from libs import signature_creator



# should not store these here.
# When used should have a constructor where they are passed by the application using this module
KEY = 'ae73a501-28cc-4807-9484-b9e9419d15ec'
SECRET = 'ed7e45d258c2b97f1e044d593957b6cbf6211fda05b0761527e0bfd5cf9ed5eb'

# api uri
uri = 'https://api.whiteboardccp.site/'

def GetAssessment(id):
    '''
    THIS METHOD CALLS THE API TO RETRIEVE AN ASSESSMENT BY id
    :param id: THE ASSESSMENT id
    :return: THE ASSESSMENT IN json FORMAT
    '''
    # NOTE: Must always include timestamp
    params = {'assessment_id': id, 'signature_timestamp': int(time.time())}
    # create signature
    signature = signature_creator.SignatureCreator(SECRET).sign_request(params)
    header = {"Authorization": KEY, "Signature": signature}
    # make the request
    result = requests.get(uri + 'assessment', params=params, headers=header)
    if result.status_code == 200:
        return eval(result.text)
    else:
        print("Could not retrive that assessment")
        return None


def GetSubmissions(id):
    '''
    THIS METHOD CALLS THE API TO RETRIEVE A SUBMISSIONS BY assessment id
    :param id: THE assessment id
    :return: array of SUBMISSIONs IN json FORMAT
    '''
    #NOTE: Must always include timestamp
    params = {'assessment_id': id,'signature_timestamp': int(time.time())}
    # create signature
    signature = signature_creator.SignatureCreator(SECRET).sign_request(params)
    header = {"Authorization": KEY, "Signature": signature}
    # make the request
    result = requests.get(uri+'submissions', params=params, headers=header)
    if result.status_code == 200:
        return eval(result.text)
    else:
        print("Could not retrive associated submissions")
        return None

"""
    Script Start
"""
# get the assessment id
assessment_id = input("From which assessment submissions do you wish to extract? Enter assessment ID: ")
assessment = GetAssessment(assessment_id)
if assessment is not None:
    # create directory for the assessment
    directory = input("Where do you want to extract the submissions to: ")
    # make directory if it doesnt exist
    if not os.path.exists(directory):
        os.makedirs(directory)
    # now get related submissions
    submissions = GetSubmissions(assessment_id)
    if submissions is not None:
        assessment_directory = directory + "/" + str(assessment_id)
        # make a directory for the assessment
        if os.path.exists(assessment_directory):
            shutil.rmtree(assessment_directory)
        os.makedirs(assessment_directory)
        # for each students submission
        for student_submission in submissions:
            # make a directory for them
            student_directory = assessment_directory + "/" + str(student_submission['student_id'])
            if os.path.exists(student_directory):
                shutil.rmtree(student_directory)
            os.makedirs(student_directory)
            # make a file for their code
            code_path = os.path.join(student_directory, "submission.txt")
            code_file = open(code_path, "w")
            # write code submission to file
            code_file.write(student_submission['submission'])
        print("Submissions extracted")
    else:
        print("Could not find any submissions")
else:
    print("Could not extract assessment")
