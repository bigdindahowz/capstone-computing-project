#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
"""
    The following module is used to import a list of students

    it takes in a csv file in the format of:
        student_id, student_name
    this is processed and sent to the API
    This module will ignore any lines which do not comply
    (eg. student_id is not an integer)

"""
import sys
import time
import requests
from libs import signature_creator

# should not store these here.
# When used should have a constructor where they are passed by the application using this module
KEY = 'ae73a501-28cc-4807-9484-b9e9419d15ec'
SECRET = 'ed7e45d258c2b97f1e044d593957b6cbf6211fda05b0761527e0bfd5cf9ed5eb'

# api uri
uri = 'https://api.whiteboardccp.site/'


def AddStudents(students):
    '''
    used to build and make the API post request
    :param enrolment:
    :return:
    '''
    # Put the parameters needed into the json
    params = dict(students=students, signature_timestamp=int(time.time()))
    # small bug work around to convert array of JSON to a string
    params['students'] = ",".join([str(element) for element in params['students']])
    # create signature
    signature = signature_creator.SignatureCreator(SECRET).sign_request(params)
    # form header
    header = {"Authorization": KEY, 'Signature': signature}
    # make the POST request
    request = requests.post(uri+'students', params=params, headers=header)
    if request.status_code == 200:
        print("Adding students Successful")
    else:
        print("Error making API request: " + request.text)


def ProcessFile(csvFile):
    '''
    Script to process the file once it is found
    :param csvFile:
    :return:
    '''

    # once file is open get all lines
    fileLines = csvFile.readlines()
    # initialise students
    students = []
    # each line should contain a student
    for studentInfo in fileLines:
        # if 50 students are hit then send them and continue
        if len(students) > 49:
            AddStudents(students)
            students = []
        studentInfo = studentInfo.split(',')
        # if there is id an name
        if len(studentInfo) == 2:
            # process student
            try:
                student_id = int(studentInfo[0])
                # make sure to remove '\n'
                name = studentInfo[1].rstrip()
                # form this info into a json
                student = dict(student_id=student_id, name=name)
                # add this enrolment to the list
                students.append(student)
            except ValueError:
                print("\tIgnoring student: " + ', '.join(map(str, studentInfo)))
        # else ignore
        else:
            print("\tIgnoring student: " + ', '.join(map(str, studentInfo)))

    # now the enrolment is gathered add it to the database using the api
    if len(students) == 0:
        print("Could not find any valid students")
    else:
        AddStudents(students)


"""
    Script Start
"""
# get the file name of the csv file from args if it was provided
if len(sys.argv)>1:
    fileName = sys.argv[1]
else:
    #else ask for it
    fileName = input("Please provide the csv file that holds the students: ...")

# try open the file, default is read
csvFile = open(fileName)
while(csvFile is None):
    fileName = input("Could not find the csv file that holds the students, please re-enter the path: ...")
    csvFile = open(fileName)
# process the file once its open
ProcessFile(csvFile)
