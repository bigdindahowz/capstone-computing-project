#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
"""
    The following module is used to import enrolments of students
        It is expected that it takes a .csv file with (student_id, unit_code, year,semester) as the file format
        eg.
        18322178,COMP4000,2017,1
        18153462,COMP3000,2017,2

        OR

        If the user wanted to enrol all the students in the same unit it could be set up as such
        '''
        unit_code, year, semester
        student_id
        student_id
        student_id
        student_id
        student_id
        etc...
        '''
        With the unit information in the first line, then a list of student_ids to be enrolled afterwards
"""

import sys
import time
import requests
from libs import signature_creator

# should not store these here.
# When used should have a constructor where they are passed by the application using this module
KEY = 'ae73a501-28cc-4807-9484-b9e9419d15ec'
SECRET = 'ed7e45d258c2b97f1e044d593957b6cbf6211fda05b0761527e0bfd5cf9ed5eb'

# api uri
uri = 'https://api.whiteboardccp.site/'

def AddEnrolment(enrolment):
    '''
    used to build and make the API post request
    :param enrolment:
    :return:
    '''
    # Put the parameters needed into the json
    params = dict(enrolment=enrolment, signature_timestamp=int(time.time()))
    # small bug work around to convert array of JSON to a string
    params['enrolment'] = ",".join([str(element) for element in params['enrolment']])
    # create signature
    signature = signature_creator.SignatureCreator(SECRET).sign_request(params)
    # form header
    header = {"Authorization": KEY, 'Signature': signature}
    # make the POST request
    request = requests.post(uri+'enrolment', params=params, headers=header)
    if request.status_code == 200:
        print("Enrolment Successful")
    else:
        print("Error making API request: " + request.text)


def ProcessIndividual(fileLines):
    '''

    :param fileLines:
    :return:
    '''
    # initialise the enrolment as empty
    enrolment = []
    # need to process each line separately
    for line in fileLines:
        # ensure the line has the right number of fields
        line = line.split(',')
        if len(line) == 4:
            # ensure that the student id is an int and the year and the semester
            try:
                student_id = int(line[0])
                unit_id = line[1]
                year = int(line[2])
                semester = int(line[3].rstrip())
                # form this info into a json
                student_enrolment = dict(student_id=student_id, unit_id=unit_id, year=year, semester=semester)
                # add this enrolment to the list
                enrolment.append(student_enrolment)
            except ValueError:
                print("\tIgnoring line: " + ', '.join(map(str, line)))
        else:
            print("\tIgnoring line: " + ', '.join(map(str, line)))
    # now the enrolment is gathered add it to the database using the api
    if len(enrolment) == 0:
        print("Could not find any valid enrolments")
    else:
        AddEnrolment(enrolment)


def ProcessByUnit(fileLines):
    '''
    used to add all students to the unit provided at the top of the file
    :param fileLines:
    :return:
    '''
    # get the unit Info
    unitArr = fileLines[0].split(',')
    unit = {'unit_id': unitArr[0], 'year': unitArr[1], 'semester': unitArr[2].rstrip()}
    # remove the unit line from the lines
    del fileLines[0]
    # initialise enrolment object
    enrolment = []
    # get each student throw away lines that are of bad format
    for student_id in fileLines:
        # ensure it is only the single student id on the line
        if len(student_id.split(',')) == 1:
            try:
                # ensure that the line is actually an int
                student_id = int(student_id.rstrip())
                # build an enrolment JSON and add it to the array
                # essentially just need to add the student_id to the unit JSON
                student_enrolment = unit
                student_enrolment['student_id'] = student_id
                # add the new enrolment to the enrolment list
                enrolment.append(student_enrolment)
            # if the line is not an int throw away
            except ValueError:
                print("\tIgnoring student id: " + student_id)

    # now the enrolment is gathered add it to the database using the api
    if len(enrolment) == 0:
        print("Could not find any valid students to enrol")
    else:
        AddEnrolment(enrolment)


def ProcessFile(csvFile):
    '''
    Script to process the file once it is found
    :param csvFile:
    :return:
    '''

    # once file is open get all lines
    fileLines = csvFile.readlines()
    # read the first line and determine format
    # break up by commas
    firstLine = fileLines[0].split(',')
    # if the first example
    if (len(firstLine) == 4):
        ProcessIndividual(fileLines)
    # second example
    elif (len(firstLine) == 3):
        if(len(fileLines) > 1):
            ProcessByUnit(fileLines)
        else:
            print("No one to enrol")
    # else it is bad formatting
    else:
        print("File format is not correct")
        return


"""
    Script Start
"""
# get the file name of the csv file from args if it was provided
if len(sys.argv)>1:
    fileName = sys.argv[1]
else:
    #else ask for it
    fileName = input("Please provide the csv file that holds the enrolment: ...")

# try open the file, default is read
csvFile = open(fileName)
while(csvFile is None):
    fileName = input("Could not find the csv file that holds the enrolment, please re-enter the path: ...")
    csvFile = open(fileName)
# process the file once its open
ProcessFile(csvFile)
