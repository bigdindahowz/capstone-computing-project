<<<<<<< HEAD
#!/bin/bash

#Configuration script for the whiteboard project which will install the necessary packages

#Ensures script runs as root
(( EUID != 0 )) && exec sudo -- "$0" "$@"

#Update package list and patch server
apt-get update -y
apt-get upgrade -y

#Instal python and pip
apt-get install python3-pip -y

#Install Django
pip3 install Django==2.0.3

#Update pip
pip3 install --upgrade pip

#Install Gunicorn
pip3 install gunicorn

#Install postfix unattended
DEBIAN_FRONTEND=noninteractive apt-get install postfix

#Add Docker gpg key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#Add Docker repo
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

#Update package manager
apt-get update

#Ensure Docker is installed from the correct repository
apt-cache policy docker-ce

#Install Docker
apt-get install -y docker-ce -y

#Install MySQL
apt-get install mysql-server -y

#Firewall Configuration

#Add INPUT and OUTPUT rules temporarily
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -F

#Add necessary rules
iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -p tcp --dport 25 -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 443 -j ACCEPT
iptables -I INPUT 1 -i lo -j ACCEPT

#Change default input rule back to drop
iptables -P INPUT DROP

#Make firewall rules persistent between reboots
echo iptables-persistent iptables-persistent/autosave_v4 boolean true | sudo debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean true | sudo debconf-set-selections
=======
#!/bin/bash

#Configuration script for the whiteboard project which will install the necessary packages

#Ensures script runs as root
(( EUID != 0 )) && exec sudo -- "$0" "$@"

#Update package list and patch server
apt-get update -y
apt-get upgrade -y

#Instal python and pip
apt-get install python3-pip -y

#Install Django
pip3 install Django==2.0.3

#Update pip
pip3 install --upgrade pip

#Install Gunicorn
pip3 install gunicorn

#Install postfix unattended
DEBIAN_FRONTEND=noninteractive apt-get install postfix

#Add Docker gpg key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#Add Docker repo
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

#Update package manager
apt-get update

#Ensure Docker is installed from the correct repository
apt-cache policy docker-ce

#Install Docker
apt-get install -y docker-ce -y

#Install MySQL
apt-get install mysql-server -y

#Firewall Configuration

#Add INPUT and OUTPUT rules temporarily
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -F

#Add necessary rules
iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -p tcp --dport 25 -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 443 -j ACCEPT
iptables -I INPUT 1 -i lo -j ACCEPT

#Change default input rule back to drop
iptables -P INPUT DROP

#Make firewall rules persistent between reboots
echo iptables-persistent iptables-persistent/autosave_v4 boolean true | sudo debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean true | sudo debconf-set-selections
>>>>>>> a5bb1976308dd1b7ae7431fea8f0379cfa441252
apt-get install iptables-persistent -y