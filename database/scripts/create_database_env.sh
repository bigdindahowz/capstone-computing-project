#!/bin/bash
#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

# This script executes a number of SQL scripts designed to create the database and tables required for the Whiteboard HTTPS API database

DB_NAME="whiteboard_api"
DB_USER="whiteboard_admin"
DB_PASS="password"

TEST_DB_NAME="whiteboard_api_test"
TEST_DB_USER="whiteboard_test"
TEST_DB_PASS="password"

echo "Rebuilding whiteboard API database"
echo "Warning: This will delete any existing instances of" $DB_NAME
echo "Continue?"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) break;;
		No ) exit;;
	esac
done


#Create whitboard database and associated user
mysql -uroot -p < create_db.sql

#Create tables
mysql $DB_NAME -u$DB_USER -p$DB_PASS < create_students.sql
mysql $DB_NAME -u$DB_USER -p$DB_PASS < create_units.sql
mysql $DB_NAME -u$DB_USER -p$DB_PASS < create_enrolment.sql
mysql $DB_NAME -u$DB_USER -p$DB_PASS < create_assessments.sql
mysql $DB_NAME -u$DB_USER -p$DB_PASS < create_submissions.sql
mysql $DB_NAME -u$DB_USER -p$DB_PASS < create_api_keys.sql

echo "Do you wish to add fake data to database?" 
select yn in "Yes" "No"; do
	case $yn in
		Yes ) break;;
		No ) exit;;
	esac
done
#fill test database with data
mysql $DB_NAME -u$DB_USER -p$DB_PASS < ../test_data/students_data.sql
mysql $DB_NAME -u$DB_USER -p$DB_PASS < ../test_data/units_data.sql
mysql $DB_NAME -u$DB_USER -p$DB_PASS < ../test_data/assessments_data.sql
mysql $DB_NAME -u$DB_USER -p$DB_PASS < ../test_data/submissions_data.sql


echo "Do you wish to create a new test database also?" 
select yn in "Yes" "No"; do
	case $yn in
		Yes ) break;;
		No ) exit;;
	esac
done

#Create whitboard test database and associated user
mysql -uroot -p < create_test_db.sql

#Create tables
mysql $TEST_DB_NAME -u$TEST_DB_USER -p$TEST_DB_PASS  < create_students.sql
mysql $TEST_DB_NAME -u$TEST_DB_USER -p$TEST_DB_PASS < create_units.sql
mysql $TEST_DB_NAME -u$TEST_DB_USER -p$TEST_DB_PASS < create_enrolment.sql
mysql $TEST_DB_NAME -u$TEST_DB_USER -p$TEST_DB_PASS < create_assessments.sql
mysql $TEST_DB_NAME -u$TEST_DB_USER -p$TEST_DB_PASS < create_submissions.sql
mysql $TEST_DB_NAME -u$TEST_DB_USER -p$TEST_DB_PASS < create_api_keys.sql
mysql $TEST_DB_NAME -u$TEST_DB_USER -p$TEST_DB_PASS < ../test_data/api_key_data.sql


