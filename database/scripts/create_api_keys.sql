/*#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#*/
DROP TABLE IF EXISTS api_keys CASCADE;
CREATE TABLE api_keys (
	api_key VARCHAR(50) PRIMARY KEY,
	issued_to VARCHAR(100),
	secret VARCHAR(64) NOT NULL
);

-- Default keys for administrator purposes
-- development key and secret
INSERT INTO api_keys VALUES('ae73a501-28cc-4807-9484-b9e9419d15ec', 'Development', 'ed7e45d258c2b97f1e044d593957b6cbf6211fda05b0761527e0bfd5cf9ed5eb');
-- Web app key and secret
INSERT INTO api_keys VALUES('d699542c-5e13-4cfe-892b-1dff8def6692', 'Web App', '4ec05d47bbed4d45f180eb78f5c11255b4b7c97db9e3f5f1dfbcf7c04085b4f4');
-- Backend/sandbox key and secret
INSERT INTO api_keys VALUES('6c04fea4-d5f0-4af7-9251-742d6de094e4', 'Backend', '4b5fb201f784b3ae98aff39749cb126c501b60f88a821231b17cca75f2b2a32c');

