/*#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#*/
USE whiteboard_users;


DROP TABLE IF EXISTS users CASCADE;
CREATE TABLE users (
	student_id  INT PRIMARY KEY,
	first_name VARCHAR(50),
    last_name VARCHAR(50),
    acc_type ENUM('student', 'lecturer', 'admin'),
    password CHAR(64)
);
