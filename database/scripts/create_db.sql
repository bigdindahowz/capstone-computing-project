/*#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#*/
DROP USER 'whiteboard_admin'@'localhost';
FLUSH PRIVILEGES;
CREATE USER 'whiteboard_admin'@'localhost' IDENTIFIED BY 'password';

DROP DATABASE IF EXISTS whiteboard_api;
CREATE DATABASE whiteboard_api;

GRANT ALL PRIVILEGES ON whiteboard_api.* TO 'whiteboard_admin'@'localhost';
