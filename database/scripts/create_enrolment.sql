/*#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#*/
DROP TABLE IF EXISTS enrolment;
CREATE TABLE enrolment (
	unit_id VARCHAR(10) NOT NULL,
	year INT NOT NULL,
	semester INT NOT NULL,
	student_id INT NOT NULL,

	FOREIGN KEY(unit_id, year, semester) REFERENCES units(unit_id, year, semester) ON DELETE CASCADE,
	FOREIGN KEY(student_id) REFERENCES students(student_id) ON DELETE CASCADE,
	PRIMARY KEY(unit_id, year, semester, student_id)
);





