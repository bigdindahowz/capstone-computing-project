/*#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#*/
DROP TABLE IF EXISTS assessments;
CREATE TABLE assessments (
	assessment_id INT NOT NULL AUTO_INCREMENT,
	unit_id VARCHAR(10) NOT NULL,
	year INT NOT NULL,
	semester INT NOT NULL,
	description VARCHAR(500) DEFAULT "",
	due_date DATETIME NOT NULL,
	submissions_permitted INT DEFAULT -1,
	marks INT DEFAULT 1,
	test_input VARCHAR(1000) DEFAULT "",
	expected_output VARCHAR(1000) DEFAULT "",
    language VARCHAR(50) DEFAULT "",

	FOREIGN KEY(unit_id, year, semester) REFERENCES units(unit_id, year, semester) ON DELETE CASCADE,
	PRIMARY KEY(assessment_id)
);

