/*#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#*/
DROP TABLE IF EXISTS submissions;
CREATE TABLE submissions (
    submission_id INT NOT NULL AUTO_INCREMENT,
	student_id INT NOT NULL,
	assessment_id INT NOT NULL,
	is_marked INT DEFAULT FALSE,
    mark INT DEFAULT 0,
	feedback VARCHAR(1000) DEFAULT "",
	submission VARCHAR(1000),
    running_output VARCHAR(1000) DEFAULT "",
    running_time INT DEFAULT 0,
    memory_usage INT DEFAULT 0,
    attempts_made INT DEFAULT 1,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

	FOREIGN KEY(student_id) REFERENCES students(student_id) ON DELETE CASCADE,
	FOREIGN KEY(assessment_id) REFERENCES assessments(assessment_id) ON DELETE CASCADE,

	PRIMARY KEY(submission_id)
);
