#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially

# README #

V 1.0

The following document will provide details on the design and the setup of the database required to store information for the Whiteboard web_app.

The database is currently tested and runs on ubuntu server using MySQL. There are severeal scripts that can be used to set up the required database and related table.
These scripts can be found in the 'scripts' directory.
Each *.sql file is used to modify the database.
There is however on bash file that can be used to create all tables required. This document is called create_database_env.bash.
To see the actuall order of setting up of the tables you can analyse this script.
####BUILDING THE DATABASE####
If create_database_env.bash is run with administrator privleges it will set up the database as required by the API.
It will initialy ask you if you wish to rebuild the database. this is due to the fact that the rebuilding of the database will wipe any existing data during the rebuild.
If you do accept it will ask for the administrator password
####TEST DATA####
It will then ask if you wish to add 'fake data'. This is useful if you want to populate the database with fake units, students and other information.
The exact fake data that is added can be found in the 'test_data' directory. Each script is appropriately named for the data that it adds. 
If at some point you wished to add extra test data these are the files that you would add to.
####TEST DATABASE####
Once the fake data is added or not added it will ask if you wish to build a test database.
This database is used by the API to run test cases. It is not needed for production, it should be set up if you are intending to make changes to the API however.


##Database Design##
###Tables###

//this table holds the api keys and their related access levels
####Api_keys####
*key <PK>
*issued_to
*secret
*permissions

//The students table is what holds the information regarding students.
####Students####
*student_id <PK>
*name

//Holds information regarding units
####Units####
*unit_id <PK>
*unit_name
*year
*semester

//Table for keeping track of enrolments, which students are in which units. Takes a foriegn key from both students and units
####Enrolment####
*student_id <PK><FK>
*unit_id <PK><FK>
*grade

//This table holds the information for assessments. It takes a foriegn key from units which keeps assignments specific to each unit.
####Assesments####
*assessment_id <PK>
*unit_id <PK><FK>
*description
*due_date
*submissions_permitted
*marks


//Submissions are the code that students submit for each assessment. 
####Submission####
*unit_id <PK><FK>
*assessment_id <PK><FK>
*student_id <PK><FK>
*is_marked
*mark
*feedback
*submission
*test_input
*expected_output


