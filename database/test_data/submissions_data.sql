/*#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#*/
/* Data to fill the submmissions table */


INSERT into submissions VALUES (5, 46912470, 3, 0, -1, '','#include <stdio.h>

int main() {
	//Simple c program to test whether the docker is working for executing C code.
	int a = 3;
	int b = 2;
	int c = 9;

	printf("%d\\n", a*b*c);

	return 0;
}','',0,0,1,'2018-05-10');
INSERT into submissions VALUES (6, 46912470, 4, 0, -1, '','#include <stdio.h>
#include <stdlib.h>

int main() {
	//Simple c program to test whether the docker is working for executing C code.
	int a = 18;
	int b = 9;
	int c = 43;

	printf("%d\\n", a*b*c);

	int i = 0;
	for (i = 0; i < 1000; i++)
	{
		malloc(1);
	}

	return 0;
}','',0,0,1,'2018-05-10');
INSERT into submissions VALUES (7, 46912470, 5, 0, -1, '','#include <stdio.h>

int main() {
	//Simple c program to test whether the docker is working for executing C code.
	int a = 3;
	int b = 2;
	int c = 9;

	printff("%d\n", a*b*c);

	return 0;
}','',0,0,1,'2018-05-10');
INSERT into submissions VALUES (8, 46912470, 6, 0, -1, '','#include <iostream>
using namespace std;

int main()
{
	cout << "Hello World!";
	return 0;
}
','',0,0,1,'2018-05-10');
INSERT into submissions VALUES (9, 46912470, 7, 0, -1, '','/* Basic Java class used as a test harness for the Sandbox */

public class JavaHelloWorld {
	public static void main(String[] args) {
		System.out.println("Hello world!");
	}
}','',0,0,1,'2018-05-10');
INSERT into submissions VALUES (10, 46912470, 8, 0, -1, '','def main():
    print("Hello world!")

if __name__ == \'__main__\':
    main()','',0,0,1,'2018-05-10');
INSERT into submissions VALUES (11, 46912470, 9, 0, -1, '','SELECT * FROM Students;','',0,0,1,'2018-05-10');

