/*#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#*/
INSERT INTO units (unit_id, unit_name, year, semester) VALUES 
("COMP1001","Object Oriented Program Design", 2016, 1),
("COMP1001","Object Oriented Program Design", 2016, 2),
("COMP1001","Object Oriented Program Design", 2017, 1),
("COMP1001","Object Oriented Program Design", 2017, 2),
("COMP1001","Object Oriented Program Design", 2018, 1),

("COMP1000","Unix & C Programming", 2015, 1),
("COMP1000","Unix & C Programming", 2016, 1),
("COMP1000","Unix & C Programming", 2017, 1),
("COMP1000","Unix & C Programming", 2018, 1),

("ISYS1001","Database Systems", 2010, 2),
("ISYS1001","Database Systems", 2011, 2),
("ISYS1001","Database Systems", 2012, 2),
("ISYS1001","Database Systems", 2013, 2),
("ISYS1001","Database Systems", 2014, 2),
("ISYS1001","Database Systems", 2015, 2),
("ISYS1001","Database Systems", 2016, 2),
("ISYS1001","Database Systems", 2017, 2),

("COMP2006","Operating Systems", 2015, 2),
("COMP2006","Operating Systems", 2017, 2);
