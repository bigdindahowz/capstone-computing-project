/*#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#*/
/*Randomly gennerated data from http://www.generatedata.com/#generator*/

INSERT INTO students (name,student_id) VALUES ("Blaine Vaughan","46912470"),("Ashely Schultz","04017062"),("Cassidy Woodard","55552114"),("Lareina Farrell","64298716"),("Kristen Hewitt","76055691"),("Mary Battle","68015638"),("Camden Burks","28862545"),("Aline Benson","25084818"),("Denise Garrison","00699243"),("Otto Collier","53171382");
INSERT INTO students (name,student_id) VALUES ("Valentine Tanner","94499915"),("Ezra Jenkins","92536724"),("Tobias Mercado","67530645"),("Blaze Knapp","34736196"),("Kato Mendoza","17648669"),("Eaton Kane","85472264"),("Rhiannon Bradford","61888249"),("Willow Clements","47695711"),("Constance Burch","39672729"),("Jacob Mueller","14775499");
INSERT INTO students (name,student_id) VALUES ("Boris Buckley","96194180"),("Cade Rodgers","67477950"),("Hedda Marsh","29630003"),("Heidi Wynn","42320105"),("MacKensie Vargas","98831954"),("Noel George","95453060"),("Medge Livingston","25906310"),("Kelly Benson","23457790"),("Hayden Branch","48868424"),("Tashya Wiggins","82489062");
INSERT INTO students (name,student_id) VALUES ("Coby Baldwin","79818988"),("Kyra Haney","03857505"),("Joelle Sosa","41622225"),("Raja Kirkland","58022274"),("Ayanna Chambers","12009931"),("Macaulay Chandler","79926437"),("Pamela Burnett","23970243"),("Keane Harmon","28078299"),("Wylie Head","44392364"),("Sarah Small","67643028");
INSERT INTO students (name,student_id) VALUES ("Quintessa Rosario","81333572"),("Yasir Shields","04929820"),("Arsenio Orr","56714523"),("Gavin Ferrell","00418730"),("Kamal Wells","26054194"),("Declan Harper","55938086"),("Alana Clements","11659705"),("Alden Wiley","30437864"),("Gage Black","73769260"),("Cheryl Gross","58885425");
INSERT INTO students (name,student_id) VALUES ("Colt Fletcher","25194258"),("Dane Bennett","38089428"),("Dean Davis","83358119"),("Lani Levine","65227277"),("Kyla Morrison","86258549"),("Arsenio Perez","25151644"),("Macy Chandler","70658285"),("Kareem Bowman","94923323"),("Rooney James","06952021"),("Caleb Fuller","19730376");
INSERT INTO students (name,student_id) VALUES ("Veronica Aguirre","65632833"),("Alexa Abbott","45193104"),("Barbara Carrillo","93046290"),("Jonas Page","84927931"),("Stacey Irwin","28133290"),("Halee Forbes","02927525"),("Karen Crane","13875109"),("Bradley Knox","13891363"),("Kenyon Ratliff","99052335"),("Beatrice Whitaker","28468933");
INSERT INTO students (name,student_id) VALUES ("Vanna Long","13313771"),("Harlan Marquez","85014989"),("Martin Erickson","03097858"),("Whitney Alston","75109649"),("Ivor Booker","83770914"),("Giselle Schultz","62746784"),("Whilemina Madden","87058021"),("Taylor Shaw","12682267"),("Abraham Mcclain","71954313"),("Kyla Terrell","06802720");
INSERT INTO students (name,student_id) VALUES ("Byron Horn","59285231"),("Calvin Marsh","75967635"),("Summer Holland","63183488"),("Ramona Bates","23023534"),("Yael Burgess","58701319"),("Kerry Hendricks","15140613"),("Hayley Reed","78658876"),("Murphy Kent","94681666"),("Dexter Stone","45004992"),("Asher Reyes","96264444");
INSERT INTO students (name,student_id) VALUES ("Louis Leblanc","29983063"),("Aquila Webster","11767726"),("Michael Wood","15091911"),("Jackson Lancaster","77948919"),("Sawyer Elliott","21705383"),("Abra Herrera","77673926"),("Brittany Brock","57144795"),("Philip Kramer","28285816"),("Tiger Clayton","17245907"),("Acton Frost","96747803");
