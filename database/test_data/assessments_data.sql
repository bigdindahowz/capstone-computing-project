/*#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#*/
/* Data to fill the assessments table */
INSERT INTO assessments VALUES (3,'COMP1000', 2018, 1, 'Program which tests basic math in C', '2018-05-01', -1, 100, '', '54', 'c');
INSERT INTO assessments VALUES (4,'COMP1000', 2018, 1, 'Program which tests memory leak detection in C', '2018-05-01', -1, 100, '', 'Memory leaks are present', 'c');
INSERT INTO assessments VALUES (5,'COMP1000', 2018, 1, 'Program which tests compilation error detection in C', '2018-05-01', -1, 100, '', 'Compilation error', 'c');
INSERT INTO assessments VALUES (6,'COMP1001', 2018, 1, 'Program which tests hello world in C++', '2018-05-01', -1, 100, '', 'Hello World!', 'cpp');
INSERT INTO assessments VALUES (7,'COMP1001', 2018, 1, 'Program which tests hello world in Java', '2018-05-01', -1, 100, '', 'Hello world!', 'java');
INSERT INTO assessments VALUES (8,'COMP1001', 2018, 1, 'Program which tests hello world in Python', '2018-05-01', -1, 100, '', 'Hello world!', 'python2');
INSERT INTO assessments VALUES (9,'ISYS1001', 2017, 2, 'Program which tests selecting data from MySQL database', '2018-05-01', -1, 100, 'CREATE table Students (StudentID int, StudentFirstname varchar(255), StudentLastname varchar(255));
INSERT INTO Students (StudentID, StudentFirstname, StudentLastname) VALUES (1, \'Chris\', \'Elliott\');
INSERT INTO Students (StudentID, StudentFirstname, StudentLastname) VALUES (2, \'Steph\', \'Travaglini\');
INSERT INTO Students (StudentID, StudentFirstname, StudentLastname) VALUES (3, \'George\', \'Haynes\');','StudentIDStudentFirstnameStudentLastname1ChrisElliott2StephTravaglini3GeorgeHaynes', 'sql');
