#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import time

from falcon import falcon

from libs import signature_creator
from tests import api_test_case



class TestAuthentication(api_test_case.APITestCase):

    def case_setup(self):
        self.param_dict = {'a': 'abc', 'b': 'abc', 'abc': 'a', "signature_timestamp": int(time.time())}
        self.signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(self.param_dict)
        self.header_dict = {"Authorization": self.TEST_KEY, "signature": self.signature}

    def test_valid(self):
        self.case_setup()
        # test valid
        result = self.simulate_get('/example/', params = self.param_dict, headers = self.header_dict)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_invalid_sig(self):
        # test invalid signature
        self.case_setup()
        self.header_dict["signature"] = "invalid"
        result = self.simulate_get('/example/', params=self.param_dict, headers=self.header_dict)
        json = result.json
        self.assertEqual(result.status, falcon.HTTP_401 )
        self.assertEqual(json['title'], 'Signature invalid')

    def test_missing_key(self):
        # test no key
        self.case_setup()
        header_dict = {"signature": "invalid-signature"}
        result = self.simulate_get('/example/', params = self.param_dict, headers = header_dict)
        json = result.json
        self.assertEqual(result.status, falcon.HTTP_401 )
        self.assertEqual(json['title'], 'Key required')

    def test_missing_sig(self):
        # test no sig
        self.case_setup()
        header_dict = {"Authorization": self.TEST_KEY}
        result = self.simulate_get('/example/', params = self.param_dict, headers = header_dict)
        json = result.json
        self.assertEqual(result.status, falcon.HTTP_401 )
        self.assertEqual(json['title'], 'Signature required')


    def test_stale_timestamp(self):
        # test stale timestamp
        self.case_setup()
        self.param_dict["signature_timestamp"] -= 10000
        result = self.simulate_get('/example/', params=self.param_dict, headers=self.header_dict)
        json = result.json
        self.assertEqual(result.status, falcon.HTTP_401)
        self.assertEqual(json['title'], 'Signature expired')


    def test_missing_timestamp(self):
        # test missing timestamp
        self.case_setup()
        self.param_dict = {'a': 'abc', 'b': 'abc', 'abc': 'a'}
        result = self.simulate_get('/example/', params=self.param_dict, headers=self.header_dict)
        json = result.json
        self.assertEqual(result.status, falcon.HTTP_401)
        self.assertEqual(json['title'], 'Timestamp required')
