#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import time
import falcon
import datetime
from libs import signature_creator
from tests import api_test_case


class TestSubmissions(api_test_case.APITestCase):

    def createUnit(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM units WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1')
        self.db.database_commit('DELETE FROM units WHERE unit_id=\'COMP3000\' AND year=2018 AND semester=1')
        # pre add value
        self.db.database_commit(
            'INSERT INTO units(unit_id, unit_name, year, semester) VALUES (\'COMP4000\', \'Database Systems\', 2018, 1)')
        self.db.database_commit(
            'INSERT INTO units(unit_id, unit_name, year, semester) VALUES (\'COMP3000\', \'Database Systems\', 2018, 1)')

    def createAssessment(self):
        assessments = []
        self.createUnit()
        # clear table
        self.db.database_commit("DELETE FROM assessments")
        # pre add value
        assessments.append( self.db.database_commit(
            'INSERT INTO assessments(unit_id, year, semester, due_date, submissions_permitted, marks, test_input, expected_output, description) '
            'VALUES (\'COMP4000\', 2018, 1,  \'%s\',  -1,  10, \'HELLO\', \'WORLD\', \'Test assessment\')' % datetime.datetime.now()))
        assessments.append( self.db.database_commit(
            'INSERT INTO assessments(unit_id, year, semester, due_date, submissions_permitted, marks, test_input, expected_output, description) '
            'VALUES (\'COMP3000\', 2018, 1,  \'%s\',  -1,  10, \'HELLO\', \'WORLD\', \'Test assessment\')' % datetime.datetime.now()))
        return assessments


    def createStudent(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM students WHERE student_id=99999999')
        self.db.database_commit('DELETE FROM students WHERE student_id=88888888')
        # pre add value
        self.db.database_commit('INSERT INTO students (student_id, name) VALUES (\'99999999\', \'Jimmy\')')
        self.db.database_commit('INSERT INTO students (student_id, name) VALUES (\'88888888\', \'Jane\')')

    def createSubmission(self, assessments):
        # clear the table
        self.db.database_commit("DELETE FROM submissions")
        self.db.database_commit(
            'INSERT INTO submissions(student_id, assessment_id, submission) '
            'VALUES (99999999, %s, \'printf("Hello world");\')' % assessments[0])
        self.db.database_commit(
            'INSERT INTO submissions(student_id, assessment_id, submission) '
            'VALUES (88888888, %s, \'printf("Hello world");\')' % assessments[0])
        self.db.database_commit(
            'INSERT INTO submissions(student_id, assessment_id, submission, is_marked) '
            'VALUES (99999999, %s, \'printf("Hello world");\', 1)' % assessments[1])

    def setupDatabase(self):
        createdAssessments = self.createAssessment()
        self.createStudent()
        self.createSubmission(createdAssessments)
        return createdAssessments


    def test_get_all(self):
        self.setupDatabase()
        params = {'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/submissions/', params=params, headers=header)
        self.assertEqual(3, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)


    def test_get_unmarked(self):
        self.setupDatabase()
        params = {'is_marked': 0, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/submissions/', params=params, headers=header)
        self.assertEqual(2, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_marked(self):
        self.setupDatabase()
        params = {'is_marked': 1, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/submissions/', params=params, headers=header)
        self.assertEqual(1, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_by_student(self):
        self.setupDatabase()
        params = {'student_id': 99999999, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/submissions/', params=params, headers=header)
        self.assertEqual(2, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

        params = {'student_id': 88888888, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/submissions/', params=params, headers=header)
        self.assertEqual(1, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_by_assessment(self):
        assessments = self.setupDatabase()
        params = {'assessment_id': assessments[0], 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/submissions/', params=params, headers=header)
        self.assertEqual(2, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_assessment_by_student(self):
        assessments = self.setupDatabase()
        params = {'assessment_id': assessments[0], 'student_id': 88888888,'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/submissions/', params=params, headers=header)
        self.assertEqual(1, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)


    def test_get_marked_assessment_by_student(self):
        assessments = self.setupDatabase()
        params = {'assessment_id': assessments[1], 'student_id': 99999999, 'is_marked': 1, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/submissions/', params=params, headers=header)
        self.assertEqual(1, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_non_existent_assessment(self):
        assessments = self.setupDatabase()
        params = {'assessment_id': -1, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/submissions/', params=params, headers=header)
        self.assertEqual(0, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_non_existent_student(self):
        assessments = self.setupDatabase()
        params = {'student_id': -1, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/submissions/', params=params, headers=header)
        self.assertEqual(0, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_invalid(self):
        assessments = self.setupDatabase()
        params = {}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/submissions/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_401)
