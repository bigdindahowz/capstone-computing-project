#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import time
import falcon

from tests import api_test_case
from libs import signature_creator, database


class TestStudent(api_test_case.APITestCase):

    def setupDatabase(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM students WHERE student_id=99999999')
        # pre add value
        self.db.database_commit('INSERT INTO students (student_id, name) VALUES (\'99999999\', \'Jimmy\')')

    def test_get_valid(self):
        self.setupDatabase()
        params = {'student_id': '99999999', 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/student/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_invalid(self):
        """
        Tested with no parameters (get student must have student id)
        :return:
        """
        params = {'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/student/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_valid(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM students WHERE student_id=99999999')
        params = {'student_id': 99999999, 'name': 'Jimmy','signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}

        result = self.simulate_post('/student/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)


    def test_post_already_exists(self):
        self.setupDatabase()
        params = {'student_id': 99999999, 'name': 'Jimmy', 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)

        header = {'Authorization': 'TEST_KEY', 'Signature': signature}

        result = self.simulate_post('/student/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_long_name(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM students WHERE student_id=99999999')
        # create long string
        name = ""
        for x in range(0,200):
            name += str(x)
        params = {'student_id': 99999999, 'name': name, 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}

        result = self.simulate_post('/student/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_invalid(self):
        params = {'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}

        result = self.simulate_post('/student/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_delete_valid(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM students WHERE student_id=99999999')
        # pre add value
        self.db.database_commit('INSERT INTO students (student_id, name) VALUES (\'99999999\', \'Jimmy\')')
        # tested against values added by test database initialisation
        params = {'student_id': '99999999', 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_delete('/student/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_delete_invalid_id(self):
        # remove from database if entry already exists
        self.db.database_commit('DELETE FROM students WHERE student_id=99999999')
        # tested against values added by test database initialisation
        params = {'student_id': '99999999', 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_delete('/student/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_delete_no_id(self):
        # tested against values added by test database initialisation
        params = {'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_delete('/student/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_put_valid(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM students WHERE student_id=99999999')
        # pre add value
        self.db.database_commit('INSERT INTO students (student_id, name) VALUES (\'99999999\', \'Jimmy\')')

        params = {'student_id': '99999999', 'name': 'Sally', 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        student = (self.simulate_get('/student/', params=params, headers=header)).json['name']
        # check the name before hande
        self.assertEqual(student, "Jimmy")
        result = self.simulate_put('/student/', params=params, headers=header)
        unit = (self.simulate_get('/student/', params=params, headers=header)).json['name']
        self.assertEqual(result.status, falcon.HTTP_200)
        # check the name changed
        self.assertEqual(unit, "Sally")

    def test_put_invalid(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM students WHERE student_id=99999999')
        # pre add value
        self.db.database_commit('INSERT INTO students (student_id, name) VALUES (\'99999999\', \'Jimmy\')')

        params = {'student_id': '99999999', 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_put('/student/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)
