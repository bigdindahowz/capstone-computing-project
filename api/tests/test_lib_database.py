#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

from tests import api_test_case
import sqlalchemy.exc
import falcon
from mock import MagicMock

class TestDatabase(api_test_case.APITestCase):
    def test_connection(self):
        try:
            self.db.engine.connect()
        except sqlalchemy.exc.OperationalError:
            self.fail("Could not connect to the test database")

    def test_where_clause_creator(self):
        database = self.db
        dic = {"test": "test", "testtwo": "and"}
        dict = {"test": "test", "testtwo": "and", "three": None}
        str = database.where_clause_creator(dic)
        str2 = database.where_clause_creator(dict)
        self.assertEqual(str, "WHERE test='test' AND testtwo='and'")
        self.assertEqual(str2, "WHERE test='test' AND testtwo='and'")

    def test_db_param_int(self):
        db = self.db

        req = MagicMock(spec=falcon.Request)
        req.get_param = MagicMock()

        # normal
        req.get_param.return_value = 5
        self.assertEqual(db.db_param_int(req, name="name"), 5)

        # invalid value
        req.get_param.return_value = "string"
        # Task 4.10: failUnlessRaises deprecated.
        self.assertRaises(falcon.HTTPInvalidParam, db.db_param_int, req, "name", 10, 15)

        # min max
        req.get_param.return_value = 5
        self.assertEqual(db.db_param_int(req, "name", min=2, max=10), 5)
        # Task 4.10: failUnlessRaises deprecated, replaced with assertRaises
        self.assertRaises(falcon.HTTPInvalidParam, db.db_param_int, req, "name", 10, 15)
        self.assertRaises(falcon.HTTPInvalidParam, db.db_param_int, req, "name", 1, 4)
        # cant test required as its offloaded to req.get_param, which is a falcon method

    def test_db_param_real(self):
        db = self.db

        req = MagicMock(spec=falcon.Request)
        req.get_param = MagicMock()

        # normal
        req.get_param.return_value = 5.01
        self.assertEqual(db.db_param_real(req, name="name"), 5.01)

        # invalid value
        req.get_param.return_value = "string"
        # Task 4.10: failUnlessRaises deprecated, replaced with assertRaises
        self.assertRaises(falcon.HTTPInvalidParam, db.db_param_real, req, name="name")

    def test_db_param_varchar(self):
        db = self.db

        req = MagicMock(spec=falcon.Request)
        req.get_param = MagicMock()

        # normal
        req.get_param.return_value = "varchar"
        self.assertEqual(db.db_param_varchar(req, name="name"), "varchar")

        # max length
        req.get_param.return_value = "varchar"
        self.assertEqual(db.db_param_varchar(req, "name", 10), "varchar")
        # Task 4.10: failUnlessRaises deprecated, replaced with assertRaises
        self.assertRaises(falcon.HTTPInvalidParam, db.db_param_varchar, req, "name", 5)

    def test_db_param_bool(self):
        db = self.db

        req = MagicMock(spec=falcon.Request)
        req.get_param = MagicMock()

        # normal true
        req.get_param.return_value = "T"
        self.assertEqual(db.db_param_bool(req, name="name"), True)

        req.get_param.return_value = "t"
        self.assertEqual(db.db_param_bool(req, name="name"), True)

        req.get_param.return_value = "1"
        self.assertEqual(db.db_param_bool(req, name="name"), True)

        req.get_param.return_value = "Y"
        self.assertEqual(db.db_param_bool(req, name="name"), True)

        req.get_param.return_value = "y"
        self.assertEqual(db.db_param_bool(req, name="name"), True)

        # normal false
        req.get_param.return_value = "F"
        self.assertEqual(db.db_param_bool(req, name="name"), False)

        req.get_param.return_value = "f"
        self.assertEqual(db.db_param_bool(req, name="name"), False)

        req.get_param.return_value = "0"
        self.assertEqual(db.db_param_bool(req, name="name"), False)

        req.get_param.return_value = "N"
        self.assertEqual(db.db_param_bool(req, name="name"), False)

        req.get_param.return_value = "n"
        self.assertEqual(db.db_param_bool(req, name="name"), False)

        # invalid
        req.get_param.return_value = "invalid"
        self.assertRaises(falcon.HTTPInvalidParam, db.db_param_bool, req, name="name")

        # invalid with default
        req.get_param.return_value = None
        self.assertEqual(db.db_param_bool(req, name="name", default=False), False)

    def test_db_set_clause(self):
        database = self.db
        dic = {"test": "test", "testtwo": "and"}
        dict = {"test": "test", "testtwo": "and", "three": None}
        str = database.set_clause_creator(dic)
        str2 = database.set_clause_creator(dict)
        self.assertEqual(str, "SET test='test', testtwo='and'")
        self.assertEqual(str2, "SET test='test', testtwo='and'")
