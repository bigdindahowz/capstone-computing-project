#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import time
import falcon
import datetime
import json
from tests import api_test_case
from libs import signature_creator


class TestSubmission(api_test_case.APITestCase):
    def createUnit(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM units')
        # pre add value
        self.db.database_commit(
            'INSERT INTO units(unit_id, unit_name, year, semester) VALUES (\'COMP4000\', \'Database Systems\', 2018, 1)')

    def createAssessment(self):
        self.createUnit()
        # clear table
        self.db.database_commit("DELETE FROM assessments")
        # pre add value
        assessment = self.db.database_commit(
            'INSERT INTO assessments(unit_id, year, semester, due_date, submissions_permitted, marks, test_input, expected_output, description) '
            'VALUES (\'COMP4000\', 2018, 1,  \'%s\',  -1,  10, \'HELLO\', \'WORLD\', \'Test assessment\')'
            % datetime.datetime.now())
        return assessment

    def createStudent(self):
        # clean up database if entry already exists
        student_id = 99999999
        self.db.database_commit('DELETE FROM students')
        # pre add value
        self.db.database_commit('INSERT INTO students (student_id, name) VALUES (\'%s\', \'Jimmy\')' % student_id)
        return student_id

    def createSubmission(self, assessment_id, student_id):
        # clear the table
        self.db.database_commit("DELETE FROM submissions")
        submission = self.db.database_commit(
             'INSERT INTO submissions(student_id, assessment_id, submission) '
             'VALUES (%s, %s, \'printf("Hello world");\')'
            % (student_id, assessment_id))
        return submission

    def setupDatabase(self):
        createdAssessment = self.createAssessment()
        createdStudent = self.createStudent()
        createdSubmission = self.createSubmission(createdAssessment, createdStudent)
        return createdSubmission

    def test_get(self):
        submission_id = self.setupDatabase()
        params = {'submission_id': submission_id, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/submission/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_invalid(self):
        submission_id = self.setupDatabase()
        params = {'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/submission/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post(self):
        assessment = self.createAssessment()
        student = self.createStudent()
        submission_code = "printf(\"Hello World\");"
        params = {'assessment_id': assessment,
                  'student_id': student,
                  'submission': submission_code,
                  'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_post('/submission/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_post_no_assessment(self):
        assessment = self.createAssessment()
        student = self.createStudent()
        submission_code = "printf(\"Hello World\");"
        params = {'student_id': student,
                  'submission': submission_code,
                  'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_post('/submission/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_invalid_assessment(self):
        student = self.createStudent()
        submission_code = "printf(\"Hello World\");"
        params = {'assessment_id': -1,
                  'student_id': student,
                  'submission': submission_code,
                  'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_post('/submission/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_no_student(self):
        assessment = self.createAssessment()
        submission_code = "printf(\"Hello World\");"
        params = {'assessment_id': assessment,
                  'submission': submission_code,
                  'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_post('/submission/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_invalid_student(self):
        assessment = self.createAssessment()
        submission_code = "printf(\"Hello World\");"
        params = {'assessment_id': assessment,
                  'student_id': -1,
                  'submission': submission_code,
                  'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_post('/submission/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_invalid(self):
        params = {'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_post('/submission/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_no_submission(self):
        assessment = self.createAssessment()
        student = self.createStudent()
        params = {'assessment_id': assessment,
                  'student_id': student,
                  'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_post('/submission/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_empty_submission(self):
        assessment = self.createAssessment()
        student = self.createStudent()
        params = {'assessment_id': assessment,
                  'student_id': student,
                  'submission': "",
                  'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_post('/submission/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_401)

    def test_put(self):
        # create submission and unit
        submission_id = self.setupDatabase()

        params = {'submission_id': submission_id,
                  'is_marked': 1,
                  'feedback': -1,
                  'mark': 100,
                  'running_output': ">> Hello World",
                  'running_time': 1,
                  'memory_usage': 10,
                  'attempts_made': 1,
                  'signature_timestamp': int(time.time())}

        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_put('/submission/', params=params, headers=header)
        self.assertEqual(result.json['mark'], 100)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_put_no_id(self):
        # create submission and unit
        submission_id = self.setupDatabase()

        params = {'is_marked': 1,
                  'feedback': -1,
                  'running_output': ">> Hello World",
                  'running_time': 1,
                  'memory_usage': 10,
                  'attempts_made': 1,
                  'signature_timestamp': int(time.time())}

        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_put('/submission/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_put_missing_submission(self):
        # create submission and unit
        submission_id = self.setupDatabase()

        params = {'submission_id': submission_id+1,
                  'is_marked': 1,
                  'feedback': -1,
                  'running_output': ">> Hello World",
                  'running_time': 1,
                  'memory_usage': 10,
                  'attempts_made': 1,
                  'signature_timestamp': int(time.time())}

        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_put('/submission/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_put_valid(self):
        # create submission and unit
        submission_id = self.setupDatabase()

        params = {'submission_id': submission_id,
                  'feedback': "Feedback",
                  'running_output': ">> Hello World",
                  'signature_timestamp': int(time.time())}

        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_put('/submission/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_put_no_update(self):
        # create submission and unit
        submission_id = self.setupDatabase()

        params = {'submission_id': submission_id,
                  'signature_timestamp': int(time.time())}

        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_put('/submission/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_delete_valid(self):
        submission = self.setupDatabase()
        params = {'submission_id': submission, 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_put('/submission/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

