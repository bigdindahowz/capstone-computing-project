#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import time
import falcon
import datetime
from libs import signature_creator
from tests import api_test_case

class TestAssessment(api_test_case.APITestCase):

    def createUnit(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM units WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1')
        # pre add value
        self.db.database_commit('INSERT INTO units(unit_id, unit_name, year, semester) VALUES (\'COMP4000\', \'Database Systems\', 2018, 1)')

    def createAssessment(self):
        # clear table
        self.db.database_commit("DELETE FROM assessments")
        # pre add value
        assessment = self.db.database_commit('INSERT INTO assessments(unit_id, year, semester, due_date, submissions_permitted, marks, test_input, expected_output, description) '
                                             'VALUES (\'COMP4000\', 2018, 1,  \'%s\',  -1,  10, \'HELLO\', \'WORLD\', \'Test assessment\')'
                                             % datetime.datetime.now())
        return assessment

    def setupDatabase(self):
        self.createUnit()
        createdAssessment = self.createAssessment()
        return createdAssessment

    def test_get(self):
        assessment_id = self.setupDatabase()
        params = {'assessment_id': assessment_id, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/assessment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_invalid_id(self):
        assessment_id = self.setupDatabase()
        params = {'assessment_id': -1, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/assessment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_get_no_id(self):
        assessment_id = self.setupDatabase()
        params = {'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/assessment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post(self):
        self.createUnit()
        params = {'unit_id': 'COMP4000',
                  'year': 2018,
                  'semester': 1,
                  'due_date': datetime.datetime.now(),
                  'submissions_permitted': 10,
                  'marks': 100,
                  'language': "C#",
                  'test_input': "HELLO",
                  'expected_output': "WORLD",
                  'description': "This is a test description",
                  'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_post('/assessment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_post_no_unit_id(self):
        self.createUnit()
        params = {
                  'year': 2018,
                  'semester': 1,
                  'due_date': datetime.datetime.now(),
                  'submissions_permitted': 10,
                  'marks': 100,
                  'language': "C#",
                  'test_input': "HELLO",
                  'expected_output': "WORLD",
                  'description': "This is a test description",
                  'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_post('/assessment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_no_descript(self):
        self.createUnit()
        params = {'unit_id': 'COMP4000',
                  'year': 2018,
                  'semester': 1,
                  'due_date': datetime.datetime.now(),
                  'submissions_permitted': 10,
                  'marks': 100,
                  'language': "C#",
                  'test_input': "HELLO",
                  'expected_output': "WORLD",
                  'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_post('/assessment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_post_invalid(self):
        self.createUnit()
        params = {'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_post('/assessment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_non_existing_unit(self):
        params = {'unit_id': 'NON4000',
                  'year': 2018,
                  'semester': 1,
                  'due_date': datetime.datetime.now(),
                  'submissions_permitted': 10,
                  'marks': 100,
                  'language': "C#",
                  'test_input': "HELLO",
                  'expected_output': "WORLD",
                  'description': "This is a test description",
                  'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_post('/assessment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_put_update(self):
        # create assessment and unit
        assessment_id = self.setupDatabase()

        params = {'assessment_id': assessment_id,
                  'due_date': datetime.datetime.now(),
                  'submissions_permitted': -1,
                  'marks': -1,
                  'test_input': "GOODBYE",
                  'expected_output': "WORLD",
                  'language': "Python",
                  'description': "This is an updated test description",
                  'signature_timestamp': int(time.time())}

        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_put('/assessment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_put_update_no_required(self):
        # create assessment and unit
        assessment_id = self.setupDatabase()

        params = {'assessment_id': assessment_id,
                  'signature_timestamp': int(time.time())}

        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_put('/assessment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_put_update_no_id(self):
        # create assessment and unit
        assessment_id = self.setupDatabase()

        params = {'due_date': datetime.datetime.now(),
                  'submissions_permitted': -1,
                  'marks': -1,
                  'test_input': "GOODBYE",
                  'expected_output': "WORLD",
                  'description': "This is an updated test description",
                  'signature_timestamp': int(time.time())}

        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_put('/assessment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_delete_valid(self):
        assessment = self.setupDatabase()
        params = {'assessment_id': assessment, 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': self.TEST_KEY, 'Signature': signature}

        result = self.simulate_put('/assessment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)
