#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import time
import falcon
import json
from libs import signature_creator
from tests import api_test_case


class TestStudents(api_test_case.APITestCase):
    def setupDatabase(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM students WHERE student_id=99999999')
        self.db.database_commit('DELETE FROM students WHERE student_id=88888888')
        self.db.database_commit('DELETE FROM students WHERE student_id=77777777')
        self.db.database_commit('DELETE FROM students WHERE student_id=66666666')
        # pre add value
        self.db.database_commit('INSERT INTO students (student_id, name) VALUES (\'99999999\', \'Jimmy\')')
        self.db.database_commit('INSERT INTO students (student_id, name) VALUES (\'88888888\', \'Sally\')')
        self.db.database_commit('INSERT INTO students (student_id, name) VALUES (\'77777777\', \'Jane\')')
        self.db.database_commit('INSERT INTO students (student_id, name) VALUES (\'66666666\', \'Bob\')')

    def test_get_student(self):
        self.setupDatabase()
        params = {'student_id': '99999999', 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/students/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)
        self.assertEqual(len(result.json), 1)

    def test_get_students(self):
        self.setupDatabase()
        params = {'student_id': [99999999,88888888,77777777], 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/students/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)
        self.assertEqual(len(result.json), 3)

    def test_get_students_by_name(self):
        self.setupDatabase()
        params = {'student_name': 'Jimmy', 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/students/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)
        self.assertEqual(len(result.json), 1)

    def test_get_students_by_name_and_id(self):
        self.setupDatabase()
        params = {'student_name': 'Jimmy', 'student_id': [99999999,88888888,77777777], 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/students/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)
        self.assertEqual(len(result.json), 1)

    def test_get_students_by_name_and_id_not_in_ids(self):
        self.setupDatabase()
        params = {'student_name': 'Jimmy', 'student_id': [88888888,77777777], 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/students/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)
        self.assertEqual(len(result.json), 0)

    def test_get_all_students(self):
        self.setupDatabase()
        params = {'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/students/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)
        self.assertEqual(len(result.json), 4)

    def test_post_students(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM students WHERE student_id=99999999')
        self.db.database_commit('DELETE FROM students WHERE student_id=88888888')
        self.db.database_commit('DELETE FROM students WHERE student_id=77777777')
        self.db.database_commit('DELETE FROM students WHERE student_id=66666666')

        students = [{'student_id': 99999999, 'name': "Jimmy"},{'student_id': 77777777, 'name': "Sam"}]
        params = {"students": students, 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_post('/students/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_200)

    def test_post_student(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM students WHERE student_id=99999999')
        students = {'student_id': 99999999, 'name': "Jimmy"}
        params = {"students": students,'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_post('/students/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_200)

    def test_post_already_exist(self):
        params = {"students": [{'student_id': 99999999, 'name': "Jimmy"}, {'student_id': 77777777, 'name': "Sam"}], 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_post('/students/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_200)

    def test_post_no_students(self):
        params = {'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_post('/students/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_empty_students(self):
        params = {"students": [], 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_post('/students/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_401)

    def test_post_invalid(self):
        params = {"students": [{'student_id': "5khyoi", 'name': "Jimmy"}, {'student_id': 77777777, 'name': "Sam"}], 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_post('/students/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_delete_valid(self):
        params = {"students": [99999999, 77777777], 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_delete('/students/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_200)

    def test_delete_invalid(self):
        params = {'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_delete('/students/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_400)

    def test_delete_empty(self):
        params = {"students": [], 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_delete('/students/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_401)
