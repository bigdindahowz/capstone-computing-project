#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import time
import falcon
from libs import signature_creator
from tests import api_test_case


class TestUnits(api_test_case.APITestCase):

    def setupDatabase(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM units')
        # pre add value
        self.db.database_commit(
            'INSERT INTO units(unit_id, unit_name, year, semester) VALUES (\'COMP4000\', \'Database Systems\', 2018, 1)')
        self.db.database_commit(
            'INSERT INTO units(unit_id, unit_name, year, semester) VALUES (\'COMP4000\', \'Database Systems\', 2018, 2)')
        self.db.database_commit(
            'INSERT INTO units(unit_id, unit_name, year, semester) VALUES (\'COMP4000\', \'Database Systems\', 2017, 1)')
        self.db.database_commit(
            'INSERT INTO units(unit_id, unit_name, year, semester) VALUES (\'COMP4000\', \'Database Systems\', 2017, 2)')
        self.db.database_commit(
            'INSERT INTO units(unit_id, unit_name, year, semester) VALUES (\'COMP3000\', \'Systems\', 2019, 1)')

    def test_get_all(self):
        self.setupDatabase()
        params = {'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/units/', params=params, headers=header)
        self.assertEqual(4, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_by_unit(self):
        self.setupDatabase()
        params = {'unit_id': 'COMP4000', 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/units/', params=params, headers=header)
        self.assertEqual(4, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_by_name(self):
        self.setupDatabase()
        params = {'unit_name': 'Database Systems', 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/units/', params=params, headers=header)
        self.assertEqual(4, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

        params = {'unit_name': 'Systems', 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/units/', params=params, headers=header)
        self.assertEqual(1, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

        params = {'unit_name': 'NOnAmE', 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/units/', params=params, headers=header)
        self.assertEqual(0, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_by_year(self):
        self.setupDatabase()
        params = {'year': 2018, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/units/', params=params, headers=header)
        self.assertEqual(2, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_by_sem(self):
        self.setupDatabase()
        params = {'semester': 2, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/units/', params=params, headers=header)
        self.assertEqual(2, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_by_year_and_sem(self):
        self.setupDatabase()
        params = {'year': 2018,'semester': 2, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/units/', params=params, headers=header)
        self.assertEqual(1, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_by_unit_and_sem(self):
        self.setupDatabase()
        params = {'unit_id': "COMP4000", 'semester': 2, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/units/', params=params, headers=header)
        self.assertEqual(2, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_single_unit(self):
        self.setupDatabase()
        params = {'unit_id': "COMP4000", 'year': 2018,'semester': 2, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/units/', params=params, headers=header)
        self.assertEqual(1, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_non_existent_unit(self):
        self.setupDatabase()
        params = {'unit_id': "COMP3000", 'year': 2018, 'semester': 2, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/units/', params=params, headers=header)
        self.assertEqual(0, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)
