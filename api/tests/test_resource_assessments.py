#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import time
import falcon
import datetime
from tests import api_test_case
from libs import signature_creator


class TestAssessments(api_test_case.APITestCase):

    def createUnit(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM units')
        # pre add value
        self.db.database_commit(
            'INSERT INTO units(unit_id, unit_name, year, semester) VALUES (\'COMP4000\', \'Database Systems\', 2018, 1)')
        self.db.database_commit(
            'INSERT INTO units(unit_id, unit_name, year, semester) VALUES (\'COMP3000\', \'Not Database Systems\', 2017, 1)')

    def createAssessments(self):
        # clear table
        self.db.database_commit("DELETE FROM assessments")
        # pre add value
        self.db.database_commit(
            'INSERT INTO assessments(unit_id, year, semester, due_date, submissions_permitted, marks, test_input, expected_output, description) '
            'VALUES (\'COMP4000\', 2018, 1,  \'%s\',  -1,  10, \'HELLO\', \'WORLD\', \'Test assessment\')'
            % datetime.datetime.now())
        self.db.database_commit(
            'INSERT INTO assessments(unit_id, year, semester, due_date, submissions_permitted, marks, test_input, expected_output, description) '
            'VALUES (\'COMP4000\', 2018, 1,  \'%s\',  -1,  10, \'HELLO\', \'WORLD\', \'Another Test assessment\')'
            % datetime.datetime.now())
        self.db.database_commit(
            'INSERT INTO assessments(unit_id, year, semester, due_date, submissions_permitted, marks, test_input, expected_output, description) '
            'VALUES (\'COMP3000\', 2017, 1,  \'%s\',  -1,  10, \'HELLO\', \'WORLD\', \'A Test assessment for a different unit\')'
            % datetime.datetime.now())

    def setupDatabase(self):
        self.createUnit()
        self.createAssessments()

    def test_get_all(self):
        self.setupDatabase()
        params = {'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/assessments/', params=params, headers=header)
        self.assertEqual(3, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_by_unit_id(self):
        self.setupDatabase()
        params = {'unit_id': "COMP4000", 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/assessments/', params=params, headers=header)
        self.assertEqual(2, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_by_unit(self):
        self.setupDatabase()
        params = {'unit_id': "COMP4000", 'year': 2018, 'semester': 1, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/assessments/', params=params, headers=header)
        self.assertEqual(2, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

        params = {'unit_id': "COMP3000", 'year': 2017, 'semester': 1, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/assessments/', params=params, headers=header)
        self.assertEqual(1, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)


    def test_get_by_year(self):
        self.setupDatabase()
        params = {'year': 2018, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/assessments/', params=params, headers=header)
        self.assertEqual(2, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

        params = {'year': 2017, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/assessments/', params=params, headers=header)
        self.assertEqual(1, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_non_existant_unit(self):
        self.setupDatabase()
        params = {'unit_id': "COMP9999", 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/assessments/', params=params, headers=header)
        self.assertEqual(0, len(result.json))
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_invalid_params(self):
        self.setupDatabase()
        params = {}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/assessments/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_401)
