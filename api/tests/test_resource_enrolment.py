#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import time
import json
import falcon
from libs import signature_creator
from tests import api_test_case


class TestEnrolment(api_test_case.APITestCase):

    def databaseAddStudents(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM students WHERE student_id=99999999')
        self.db.database_commit('DELETE FROM students WHERE student_id=88888888')
        self.db.database_commit('DELETE FROM students WHERE student_id=77777777')
        self.db.database_commit('DELETE FROM students WHERE student_id=66666666')
        # pre add value
        self.db.database_commit('INSERT INTO students (student_id, name) VALUES (\'99999999\', \'Jimmy\')')
        self.db.database_commit('INSERT INTO students (student_id, name) VALUES (\'88888888\', \'Sally\')')
        self.db.database_commit('INSERT INTO students (student_id, name) VALUES (\'77777777\', \'Jane\')')
        self.db.database_commit('INSERT INTO students (student_id, name) VALUES (\'66666666\', \'Bob\')')

    def databaseAddUnits(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM units WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1')
        self.db.database_commit('DELETE FROM units WHERE unit_id=\'COMP3000\' AND year=2018 AND semester=1')
        # pre add value
        self.db.database_commit('INSERT INTO units(unit_id, unit_name, year, semester) VALUES (\'COMP4000\', \'Database Systems\', 2018, 1)')
        self.db.database_commit('INSERT INTO units(unit_id, unit_name, year, semester) VALUES (\'COMP3000\', \'Design and Analysis of Algorithms\', 2018, 1)')

    def databaseAddEnrolment(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM enrolment WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1 AND student_id=99999999')
        self.db.database_commit('DELETE FROM enrolment WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1 AND student_id=88888888')
        self.db.database_commit('DELETE FROM enrolment WHERE unit_id=\'COMP3000\' AND year=2018 AND semester=1 AND student_id=88888888')
        self.db.database_commit('DELETE FROM enrolment WHERE unit_id=\'COMP3000\' AND year=2018 AND semester=1 AND student_id=77777777')
        # pre add value
        self.db.database_commit(
            'INSERT INTO enrolment(unit_id, student_id, year, semester) VALUES (\'COMP4000\', 99999999, 2018, 1)')
        self.db.database_commit(
            'INSERT INTO enrolment(unit_id, student_id, year, semester) VALUES (\'COMP4000\', 88888888, 2018, 1)')
        self.db.database_commit(
            'INSERT INTO enrolment(unit_id, student_id, year, semester) VALUES (\'COMP3000\', 88888888, 2018, 1)')
        self.db.database_commit(
            'INSERT INTO enrolment(unit_id, student_id, year, semester) VALUES (\'COMP3000\', 77777777, 2018, 1)')

    def setupDatabase(self):
        self.databaseAddStudents()
        self.databaseAddUnits()
        self.databaseAddEnrolment()



    def test_get_all(self):
        self.setupDatabase()
        params = {'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/enrolment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)
        self.assertEqual(len(result.json), 4)

    def test_get_by_student(self):
        self.setupDatabase()
        params = {'student_id': 88888888, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/enrolment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)
        self.assertEqual(len(result.json), 2)

    def test_get_by_unit(self):
        self.setupDatabase()
        params = {'unit_id': "COMP4000", 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/enrolment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)
        self.assertEqual(len(result.json), 2)

    def test_post_multiple_enrolment(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM enrolment WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1 AND student_id=99999999')
        self.db.database_commit('DELETE FROM enrolment WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1 AND student_id=88888888')
        self.db.database_commit('DELETE FROM enrolment WHERE unit_id=\'COMP3000\' AND year=2018 AND semester=1 AND student_id=88888888')
        self.db.database_commit('DELETE FROM enrolment WHERE unit_id=\'COMP3000\' AND year=2018 AND semester=1 AND student_id=77777777')

        enrolment = [{'student_id': 99999999, 'unit_id':"COMP4000", 'year': 2018, 'semester': 1},
                    {'student_id': 88888888, 'unit_id':"COMP4000", 'year': 2018, 'semester': 1},
                    {'student_id': 88888888, 'unit_id':"COMP3000", 'year': 2018, 'semester': 1},
                    {'student_id': 77777777, 'unit_id':"COMP3000", 'year': 2018, 'semester': 1}]

        params = {'enrolment': enrolment, 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_post('/enrolment/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_200)

    def test_post_single_enrolment(self):
        # clean up database if entry already exists
        self.db.database_commit(
            'DELETE FROM enrolment WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1 AND student_id=99999999')

        enrolment = {'student_id': 99999999, 'unit_id': "COMP4000", 'year': 2018, 'semester': 1}

        params = {'enrolment': enrolment, 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_post('/enrolment/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_200)

    def test_post_student_doesnt_exist(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM students WHERE student_id=99999999')

        enrolment = {'student_id': 99999999, 'unit_id': "COMP4000", 'year': 2018, 'semester': 1}
        params = {'enrolment': enrolment, 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_post('/enrolment/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_200)

    def test_post_unit_doesnt_exist(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM units WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1')

        enrolment = {'student_id': 99999999, 'unit_id': "FAKE4000", 'year': 2018, 'semester': 1}

        params = {'enrolment': enrolment, 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_post('/enrolment/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_combination_valid_and_invalid(self):
        self.setupDatabase()
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM units WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1')
        self.db.database_commit('DELETE FROM students WHERE student_id=99999999')

        enrolment = [{'student_id': 99999999, 'unit_id': "FAKE4000", 'year': 2018, 'semester': 1},
                                {'student_id': 99999999, 'unit_id': "COMP4000", 'year': 2018, 'semester': 1},
                                {'student_id': 77777777, 'unit_id':"COMP3000", 'year': 2018, 'semester': 1}]
        params = {'enrolment': enrolment, 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_post('/enrolment/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_200)

    def test_post_empty_enrolment(self):
        params = {'enrolment': [], 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_post('/enrolment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_401)

    def test_post_no_enrolment(self):
        params = {'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_post('/enrolment/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_400)

    def test_delete_valid(self):
        params = {'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}

        result = self.simulate_delete('/enrolment/')
        self.assertEqual(result.status, falcon.HTTP_200)


    def test_post_invalid(self):
        # clean up database if entry already exists
        self.db.database_commit(
            'DELETE FROM enrolment WHERE unit_id=\'COMP4000\' AND year="2018" AND semester=1 AND student_id=99999999')
        self.db.database_commit(
            'DELETE FROM enrolment WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1 AND student_id=88888888')
        self.db.database_commit(
            'DELETE FROM enrolment WHERE unit_id=\'COMP3000\' AND year=2018 AND semester=1 AND student_id=88888888')
        self.db.database_commit(
            'DELETE FROM enrolment WHERE unit_id=\'COMP3000\' AND year=2018 AND semester=1 AND student_id=77777777')

        enrolment = [{'student_id': 99999999, 'unit_id': "COMP4000", 'year': "2018a", 'semester': 1},
                                {'student_id': 88888888, 'unit_id': "COMP4000", 'year': 2018, 'semester': 1},
                                {'student_id': 88888888, 'unit_id': "COMP3000", 'year': 2018, 'semester': 1},
                                {'student_id': 77777777, 'unit_id': "COMP3000", 'year': 2018, 'semester': 1}]

        params = {'enrolment': enrolment, 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_post('/enrolment/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_400)


    def test_post_exists(self):
        self.setupDatabase()
        enrolment = [{'student_id': 99999999, 'unit_id': "COMP4000", 'year': 2018, 'semester': 1},
                                {'student_id': 88888888, 'unit_id': "COMP4000", 'year': 2018, 'semester': 1},
                                {'student_id': 88888888, 'unit_id': "COMP3000", 'year': 2018, 'semester': 1},
                                {'student_id': 77777777, 'unit_id': "COMP3000", 'year': 2018, 'semester': 1}]

        params = {'enrolment': enrolment, 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_post('/enrolment/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_200)

    def test_delete_valid(self):
        self.setupDatabase()
        enrolment = list([{'student_id': 99999999, 'unit_id': "COMP4000", 'year': 2018, 'semester': 1},
                    {'student_id': 88888888, 'unit_id': "COMP4000", 'year': 2018, 'semester': 1},
                    {'student_id': 88888888, 'unit_id': "COMP3000", 'year': 2018, 'semester': 1},
                    {'student_id': 77777777, 'unit_id': "COMP3000", 'year': 2018, 'semester': 1}])
        params = {'enrolment': enrolment, 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_delete('/enrolment/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_delete_valid__student_doesnt_exist(self):
        self.setupDatabase()
        params = {'enrolment': [{'student_id': 99999, 'unit_id': "COMP4000", 'year': 2018, 'semester': 1},
                                {'student_id': 88888, 'unit_id': "COMP4000", 'year': 2018, 'semester': 1},
                                {'student_id': 88888, 'unit_id': "COMP3000", 'year': 2018, 'semester': 1},
                                {'student_id': 77777, 'unit_id': "COMP3000", 'year': 2018, 'semester': 1}],
                  'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_delete('/enrolment/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_200)

    def test_delete_valid_unit_doesnt_exist(self):
        self.setupDatabase()
        enrolment = [{'student_id': 99999999, 'unit_id': "COMP400", 'year': 2018, 'semester': 1},
                                {'student_id': 88888888, 'unit_id': "COMP400", 'year': 2018, 'semester': 1},
                                {'student_id': 88888888, 'unit_id': "COMP300", 'year': 2018, 'semester': 1},
                                {'student_id': 77777777, 'unit_id': "COMP300", 'year': 2018, 'semester': 1}]
        params = {'enrolment': enrolment, 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_delete('/enrolment/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_200)

    def test_delete_invalid(self):
        self.setupDatabase()
        params = {'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_delete('/students/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_400)

    def test_delete_valid_single(self):
        self.setupDatabase()
        enrolment = {'student_id': 99999999, 'unit_id': "COMP400", 'year': 2018, 'semester': 1}
        params = {'enrolment': enrolment, 'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_delete('/enrolment/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_200)

    def test_delete_valid_single(self):
        self.setupDatabase()
        enrolment = {'student_id': 99999999, 'unit_id': "COMP400", 'year': 2018, 'semester': 1}
        params = {}
        params['enrolment'] = enrolment
        params['signature_timestamp'] = int(time.time())

        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}
        result = self.simulate_delete('/enrolment/', params=params, headers=header)

        self.assertEqual(result.status, falcon.HTTP_200)
