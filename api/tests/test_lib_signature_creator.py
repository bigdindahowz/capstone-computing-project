#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import unittest

from libs.signature_creator import SignatureCreator
from tests import api_test_case


class TestSignatureCreator(api_test_case.APITestCase):
    def test_sign_request(self):
        alphabetical_params = {'a': 'abc', 'b': 'abc', 'abc': 'a'}
        unalphabetical_params = {'b': 'abc', 'abc': 'a', 'a': 'abc'}

        auth = SignatureCreator(self.TEST_SECRET)

        self.assertEqual(auth.sign_request(alphabetical_params),
                         '32b116aae262190ae2fc4d2db2a73e162ee664b125d90d63386a006177a66921')
        self.assertEqual(auth.sign_request(unalphabetical_params),
                         '32b116aae262190ae2fc4d2db2a73e162ee664b125d90d63386a006177a66921')
