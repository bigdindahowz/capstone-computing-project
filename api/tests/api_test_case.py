#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

from falcon import testing
from libs import app, database



USERNAME = "whiteboard_test"
PASSWORD = "password"
HOST = "localhost"
DATABASE = "whiteboard_api_test"

db = database.Database(USERNAME,PASSWORD,HOST,DATABASE)
# initialize and return a `falcon.API` instance.
app = app.create_app(db)

class APITestCase(testing.TestCase):
    TEST_KEY = "test_key"
    TEST_SECRET = "testsecret"

    def setUp(self):
        super(APITestCase, self).setUp()
        self.db = db
        self.app = app

