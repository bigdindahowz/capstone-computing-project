#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import time
import falcon
from libs import signature_creator
from tests import api_test_case


class TestUnit(api_test_case.APITestCase):

    def setupDatabase(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM units WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1')
        # pre add value
        self.db.database_commit('INSERT INTO units(unit_id, unit_name, year, semester) VALUES (\'COMP4000\', \'Database Systems\', 2018, 1)')

    def test_get_valid(self):
        self.setupDatabase()
        params = {'unit_id': 'COMP4000', 'year':2018,'semester':1,'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/unit/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_get_missing_year(self):
        self.setupDatabase()
        params = {'unit_id': 'COMP4000', 'semester':1, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/unit/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_get_missing_year_and_semester(self):
        self.setupDatabase()
        params = {'unit_id': 'COMP4000', 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_get('/unit/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_valid(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM units WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1')
        params = {'unit_id': 'COMP4000', 'year': 2018, 'semester': 1, 'unit_name': "Database Systems",'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}

        result = self.simulate_post('/unit/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_post_invalid(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM units WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1')
        params = {'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}

        result = self.simulate_post('/unit/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_exists(self):
        self.setupDatabase()
        params = {'unit_id': 'COMP4000', 'year': 2018, 'semester': 1, 'unit_name': "Database Systems",
                  'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}

        result = self.simulate_post('/unit/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_missing_year(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM units WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1')
        params = {'unit_id': 'COMP4000', 'semester': 1, 'unit_name': "Database Systems",
                  'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}

        result = self.simulate_post('/unit/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_date(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM units WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1')
        params = {'unit_id': 'COMP4000', 'unit_name': "Database Systems",
                  'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}

        result = self.simulate_post('/unit/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_post_no_name(self):
        # clean up database if entry already exists
        self.db.database_commit('DELETE FROM units WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1')
        params = {'unit_id': 'COMP4000', 'year':2018, 'semester':1,'signature_timestamp': int(time.time())}
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        header = {'Authorization': 'TEST_KEY', 'Signature': signature}

        result = self.simulate_post('/unit/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_put_valid(self):
        self.setupDatabase()
        # params to update
        params = {'unit_id': 'COMP4000', 'year':2018, 'semester':1, 'unit_name': "New Unit Name",'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        unit = (self.simulate_get('/unit/', params=params, headers=header)).json['unit_name']
        # check the name before hande
        self.assertEqual(unit, "Database Systems")
        result = self.simulate_put('/unit/', params=params, headers=header)
        unit = (self.simulate_get('/unit/', params=params, headers=header)).json['unit_name']
        self.assertEqual(result.status, falcon.HTTP_200)
        # check the name changed
        self.assertEqual(unit, "New Unit Name")

    def test_put_invalid(self):
        self.setupDatabase()
        # params to update
        params = {'unit_id': 'COMP4000', 'year':2018, 'semester':1,'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_put('/unit/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_delete_valid(self):
        self.setupDatabase()
        params = {'unit_id': 'COMP4000', 'year':2018,'semester':1,'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_delete('/unit/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_delete_valid_again(self):
        self.setupDatabase()
        params = {'unit_id': 'COMP4000', 'year':2018,'semester':1, 'unit_name': "Database Systems",'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_delete('/unit/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_delete_invalid_id(self):
        self.setupDatabase()
        # predelete
        self.db.database_commit('DELETE FROM units WHERE unit_id=\'COMP4000\' AND year=2018 AND semester=1')
        params = {'unit_id': 'COMP4000', 'year': 2018, 'semester': 1, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_delete('/unit/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_delete_no_id(self):
        self.setupDatabase()
        params = {'year': 2018, 'semester': 1, 'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_delete('/unit/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)

    def test_delete_no_params(self):
        self.setupDatabase()
        params = {'signature_timestamp': int(time.time())}
        # create signature
        signature = signature_creator.SignatureCreator(self.TEST_SECRET).sign_request(params)
        # form header from signature and key
        header = {"Authorization": self.TEST_KEY, "signature": signature}
        # use falcon to simulate a request
        result = self.simulate_delete('/unit/', params=params, headers=header)
        self.assertEqual(result.status, falcon.HTTP_400)
