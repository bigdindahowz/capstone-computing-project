
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially


# README #

The following documentation is in relation to the API used to serve the whiteboard web app.

The API is built so that it can run as a service using gunicorn on any machine. It is used to host the connection to the database and in turn the data used for the whiteboard app.
The API is built using python in conjunction with the falcon framework to make things easier.
The documentation for falcon is great and would help a lot is you ever need develop or understand the code for the API.
You can find more info about falcon here:
https://falconframework.org/

The structure of file directory seperates the concerns. 
In the 'libs' directory you will find all of the libraries that are used by the API to perform secondary tasks such as creating signatues used for authentication and a library used to access the database.

The 'Resources' directory contains all of the files that are used for controlling the GET POST PUT and DELETE verbs. There are specific methods in each which perform sanitisation and data manipulation. There should be enough commenting in the files to understand what is going on here.

'test' is a folder that contains usecases to test the API. At the time of writting this all unit tests were passing 

The 'usecase' directory contains some files which show the average user how to access and use the api.
For more extensive documentation on how to use the api visit:
https://whiteboard_api.restlet.io
This site should provide you with all of the available calls to the API and what parameters need to be passed.

The API should ideally be run using gunicorn and nginx. The combination of these make it easy to host the API.
There are a few libraries that you will need to run the API, these are:
*falcon
*SQLAlchemy
*gunicorn

Once these are installed (preferably using a virtual environment) the api can be set up to be hosted using nginx.
To host the api as a service on your machine so that it will always be running follow this tutorial:
https://www.digitalocean.com/community/tutorials/how-to-deploy-falcon-web-applications-with-gunicorn-and-nginx-on-ubuntu-16-04

After the API is hosted you can encrypt it using HTTPS by following this tutorial:
https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-16-04

