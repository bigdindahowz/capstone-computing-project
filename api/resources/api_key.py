#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import json
import falcon
import uuid
import hmac
import hashlib

class ApiKey(object):
    def __init__(self, db):
        self.db = db

    def on_get(self, req, resp):
        key = {'Key':"Here is an API key, secret and who it is issued to"}

        resp.body = json.dumps(key)
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        issued_to = "temp"
        permissions = "R+W+M"
        #generate new api_key
        key = str(uuid.uuid4())
        #generate secret associated to the api key
        secret = hmac.new(str.encode(key), msg=str.encode(issued_to), digestmod=hashlib.sha256).hexdigest()
        #return new api key and secret
        resp.body = json.dumps({'key': key, 'secret': secret})
        resp.status = falcon.HTTP_200

    def on_put(self, req, resp):
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp):
        resp.status = falcon.HTTP_200
