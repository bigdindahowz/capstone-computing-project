#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import json
import falcon

class Units(object):
    def __init__(self, db):
        self.db = db

    def on_get(self, req, resp):
        # get the unit id to filter by
        unit_id = self.db.db_param_varchar(req, 'unit_id')
        # get the unit name to filter by
        unit_name = self.db.db_param_varchar(req, 'unit_name')
        # get the year
        year = self.db.db_param_int(req, 'year')
        # get the semester
        semester = self.db.db_param_int(req, 'semester')
        # if no student id given, return all
        if unit_id is None and year is None and semester is None and unit_name is None:
            query = "SELECT * FROM units"
        else:
            where_dict = {'unit_id': unit_id, 'year': year, 'semester': semester, 'unit_name': unit_name}
            # build a where...and claause for filtering
            where_clause = self.db.where_clause_creator(where_dict)
            query = "SELECT * FROM units %s" % where_clause

        # get students
        units = self.db.database_fetch_all(query)
        resp.body = units
        resp.status = falcon.HTTP_200
