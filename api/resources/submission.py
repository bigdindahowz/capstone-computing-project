#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import json
import falcon
from libs import utils

class Submission(object):
    def __init__(self, db):
        self.db = db

    def get_submission(self, id):
        """
        Used by this class to handle getting a single assessment by ID from the database
        :param id: student_id
        :return:
        """
        query = "SELECT * FROM submissions WHERE submission_id=\'%s\'" % id
        submission = self.db.database_fetch_one(query)
        return submission

    def get_assessment(self, id):
        """
        Used by this class to handle getting a single assessment by ID from the database
        :param id: student_id
        :return:
        """
        query = "SELECT * FROM assessments WHERE assessment_id=\'%s\'" % id
        assessment = self.db.database_fetch_one(query)
        return assessment

    def get_student(self, id):
        """
        Used by this class to handle getting a single student by ID from the database
        :param id: student_id
        :return:
        """
        query = "SELECT * FROM students WHERE student_id=\'%s\'" % id
        student = self.db.database_fetch_one(query)
        return student

    def on_get(self, req, resp):
        id = req.get_param('submission_id', required=True)
        submission = self.get_submission(id)
        if submission is None:
            raise falcon.HTTPBadRequest('Not found', 'Submission does not exist')
        # output
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(submission, default=utils.json_serial)

    def on_post(self, req, resp):
        assessment_id = self.db.db_param_int(req, 'assessment_id', required=True)
        student_id = self.db.db_param_int(req, 'student_id', required=True)

        # attempt to retrive assessment
        assessment = self.get_assessment(assessment_id)
        if assessment is None:
            raise falcon.HTTPInvalidParam('the provided assessment does not exist', 'Assessment')
        # attempt to retrive student
        student = self.get_student(student_id)
        if student is None:
            raise falcon.HTTPInvalidParam('the provided student does not exist', 'Student')

        # get actual submission code
        submission = self.db.db_param_varchar(req, 'submission', required=True)

        submisison_id = self.db.database_commit_prepared("INSERT INTO submissions (student_id, assessment_id, submission) VALUES (%s, %s, %s)" , (student_id, assessment_id, submission))
        new_submission = self.get_submission(submisison_id)

        resp.status = falcon.HTTP_200
        resp.body = json.dumps(new_submission, default=utils.json_serial)

    def on_put(self, req, resp):
        # get the submission
        submission_id = req.get_param('submission_id', required=True)
        submission = self.get_submission(submission_id)
        if submission is None:
            raise falcon.HTTPInvalidParam('the provided submission does not exist', 'Submission')

        # dictionary for parameters
        update_params = {}
        # get the remaining values that may need to be updated
        update_params['is_marked'] = self.db.db_param_bool(req, 'is_marked')
        update_params['mark'] = self.db.db_param_int(req, 'mark')
        update_params['feedback'] = self.db.db_param_varchar(req, 'feedback')
        update_params['submission'] = self.db.db_param_varchar(req, 'submission')
        update_params['running_output'] = self.db.db_param_varchar(req, 'running_output')
        update_params['running_time'] = self.db.db_param_int(req, 'running_time')
        update_params['memory_usage'] = self.db.db_param_int(req, 'memory_usage')
        update_params['attempts_made'] = self.db.db_param_int(req, 'attempts_made')

        set_clause = self.db.set_clause_creator(update_params)
        # if there actually was anyhtign to update
        if not set_clause == "":
            query = ("UPDATE submissions " + set_clause + " WHERE submission_id = %s") % submission_id
            # update query
            self.db.database_commit(query)

        # output
        out = self.get_submission(submission_id)

        resp.status = falcon.HTTP_200
        resp.body = json.dumps(out, default=utils.json_serial)

    def on_delete(self, req, resp):
        # get the submission
        submission_id = req.get_param('submission_id', required=True)
        submission = self.get_submission(submission_id)
        if submission is None:
            raise falcon.HTTPInvalidParam('the provided submission does not exist', 'Submission')

        self.db.database_commit('DELETE FROM submissions WHERE submission_id = \'%s\'' % submission['submission_id'])
