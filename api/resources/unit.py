#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import json
import falcon

class Unit(object):

    def __init__(self, db):
        self.db = db

    def get_unit(self, id, year, semester):
        """
        Used by this class to handle getting a single unit by ID, year, and semester from the database
        :param id: unit_id -String
        :param year: year -int
        :param semester: semester -int
        :return:
        """
        query = "SELECT * FROM units WHERE unit_id=\'%s\' AND year=%s AND semester=%s" % (id, year, semester)
        unit = self.db.database_fetch_one(query)
        return unit

    def on_get(self, req, resp):
        id = req.get_param('unit_id', required=True)
        year = req.get_param('year', required=True)
        semester = req.get_param('semester', required=True)
        unit = self.get_unit(id, year, semester)

        if unit is None:
            raise falcon.HTTPBadRequest('Not found', 'Unit does not exist')
        # output
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(unit)

    def on_post(self, req, resp):
        unit_id = self.db.db_param_varchar(req, 'unit_id', maxlength=8,required=True)
        # dont need name necessarily
        unit_name = self.db.db_param_varchar(req, 'unit_name', maxlength=150)
        year = self.db.db_param_int(req, 'year', required=True)
        semester = self.db.db_param_int(req, 'semester', required=True)

        unit = self.get_unit(unit_id, year, semester)

        if unit is not None:
            raise falcon.HTTPInvalidParam('the provided unit already exists', 'Unit')

        query = 'INSERT INTO units (unit_id, year, semester, unit_name) VALUES (\'%s\', \'%s\', \'%s\', \'%s\')' % (unit_id, year, semester, unit_name)
        # commit new entry to database
        self.db.database_commit(query)

        # output
        out = {
            'unit_id': unit_id,
            'unit_name': unit_name,
            'year': year,
            'semester': semester
        }
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(out)

    def on_put(self, req, resp):
        unit_id = self.db.db_param_varchar(req, 'unit_id', required=True)
        # need new name to update
        newUnit_name = self.db.db_param_varchar(req, 'unit_name', maxlength=150, required=True)
        year = self.db.db_param_int(req, 'year', required=True)
        semester = self.db.db_param_int(req, 'semester', required=True)

        unit = self.get_unit(unit_id, year, semester)

        if unit is None:
            raise falcon.HTTPInvalidParam('the provided unit doesn\'t exist', 'Unit')

        query = 'UPDATE units SET unit_name = \'%s\' WHERE unit_id = \'%s\' AND year = \'%s\' AND semester = \'%s\''  % (newUnit_name, unit['unit_id'], unit['year'], unit['semester'])
        # commit new entry to database
        self.db.database_commit(query)

        # output
        out = {
            'unit_id': unit_id,
            'unit_name': newUnit_name,
            'year': year,
            'semester': semester
        }
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(out)

    def on_delete(self, req, resp):
        id = req.get_param('unit_id', required=True)
        year = req.get_param('year', required=True)
        semester = req.get_param('semester', required=True)
        unit = self.get_unit(id, year, semester)

        if unit is None:
            raise falcon.HTTPBadRequest('Not found', 'Unit does not exist')
        # output
        query = "DELETE FROM units WHERE unit_id=\'%s\' AND year=%s and semester=%s" % (id, year, semester)
        self.db.database_commit(query)
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(unit)
