#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import json
import falcon

class Enrolment(object):
    def __init__(self, db):
        self.db = db

    def get_student(self, id):
        """
        Used by this class to handle getting a single student by ID from the database
        :param id: student_id
        :return:
        """
        query = "SELECT * FROM students WHERE student_id=\'%s\'" % id
        student = self.db.database_fetch_one(query)
        return student

    def get_unit(self, id, year, semester):
        """
        Used by this class to handle getting a single unit by ID, year, and semester from the database
        :param id: unit_id -String
        :param year: year -int
        :param semester: semester -int
        :return:
        """
        query = "SELECT * FROM units WHERE unit_id=\'%s\' AND year=%s AND semester=%s" % (id, year, semester)
        unit = self.db.database_fetch_one(query)
        return unit

    def on_get(self, req, resp):
        student_id = req.get_param('student_id')
        unit_id = req.get_param('unit_id')
        year = req.get_param('year')
        semester = req.get_param('semester')

        # only use params that are passes
        params = {}
        if student_id is not None:
            params['student_id'] = student_id
        if unit_id is not None:
            params['unit_id'] = unit_id
        if year is not None:
            params['year'] = year
        if semester is not None:
            params['semester'] = semester

        # check if empty
        clause = ""
        if params:
            clause = " "+self.db.where_clause_creator(params)
        # add WHERE-AND clause if it was made, otherwise it will be blank
        query = "SELECT * FROM enrolment%s" % clause

        enrolment = self.db.database_fetch_all(query)
        # output
        resp.status = falcon.HTTP_200
        resp.body = enrolment

    def on_post(self, req, resp):
        enrolment_query = None
        enrolment = self.db.db_param_json_list(req, 'enrolment', required=True)

        first = True
        # try to enter each enrolment
        for entry in enrolment:
            #sanitise ids and name
            unit_id = self.db.db_param_varchar(entry['unit_id'], name='unit_id',required=True)
            year = self.db.db_param_int(entry['year'], name='year', required=True)
            semester = self.db.db_param_int(entry['semester'], name='semester', required=True)
            student_id = self.db.db_param_int(entry['student_id'], name='student_id', required=True)
            #only continue if the unit already exists
            if self.get_unit(unit_id,year,semester) is not None:
                # create new student entry if the student doesnt exist
                if self.get_student(student_id) is None:
                    self.db.database_commit("INSERT INTO students (student_id) VALUES (%s)" % student_id)
                if first:
                    enrolment_query = "(\"%s\", %s, %s, %s)" % (unit_id, year, semester, student_id)
                    first = False
                else:
                    enrolment_query += ",(\"%s\", %s, %s, %s)" % (unit_id, year, semester, student_id)

        if enrolment_query is not None:
            # insert all values, ignore any duplicates or pre existing students
            query = "INSERT INTO enrolment(unit_id, year, semester, student_id) VALUES (\"%s\", %s, %s, %s) ON DUPLICATE KEY UPDATE unit_id=unit_id"  \
                                        % (unit_id, year, semester, student_id)
            self.db.database_commit(query)
            # output
            resp.status = falcon.HTTP_200
        else:
            raise falcon.HTTPInvalidParam('There was no valid entry sent, check that the units you used exist', 'Enrolment')

    def on_delete(self, req, resp):
        enrolment = req.get_param_as_list('enrolment', required=True)
        # build string for adding enrolment
        enrolment_string = '[' + ",".join(enrolment) + ']'
        enrolment = eval(enrolment_string)
        for entry in enrolment:
            #sanitise ids and name
            unit_id = self.db.db_param_varchar(entry['unit_id'], name='unit_id',required=True)
            year = self.db.db_param_int(entry['year'], name='year', required=True)
            semester = self.db.db_param_int(entry['semester'], name='semester', required=True)
            student_id = self.db.db_param_int(entry['student_id'], name='student_id', required=True)
            query = "DELETE FROM enrolment WHERE student_id = %s AND unit_id = \"%s\" AND year = %s AND semester = %s" % (student_id, unit_id, year, semester)

        self.db.database_commit(query)
        resp.status = falcon.HTTP_200
