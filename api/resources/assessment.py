#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import json
import falcon
from libs import utils

class Assessment(object):
    def __init__(self, db):
        self.db = db

    def get_assessment(self, id):
        """
        Used by this class to handle getting a single assessment by ID from the database
        :param id: student_id
        :return:
        """
        query = "SELECT * FROM assessments WHERE assessment_id=\'%s\'" % id
        assessment = self.db.database_fetch_one(query)
        return assessment

    def get_unit(self, id, year, semester):
        """
        Used by this class to handle getting a single unit by ID, year, and semester from the database
        :param id: unit_id -String
        :param year: year -int
        :param semester: semester -int
        :return:
        """
        query = "SELECT * FROM units WHERE unit_id=\'%s\' AND year=%s AND semester=%s" % (id, year, semester)
        unit = self.db.database_fetch_one(query)
        return unit

    def on_get(self, req, resp):
        id = req.get_param('assessment_id', required=True)
        assessment = self.get_assessment(id)

        if assessment is None:
            raise falcon.HTTPBadRequest('Not found', 'Assessment does not exist')

        # output
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(assessment, default=utils.json_serial)


    def on_post(self, req, resp):
        # ensure that the unit exists
        unit_id = self.db.db_param_varchar(req, 'unit_id', maxlength=8, required=True)
        year = self.db.db_param_int(req, 'year', required=True)
        semester = self.db.db_param_int(req, 'semester', required=True)
        # attempt to retrive unit
        unit = self.get_unit(unit_id, year, semester)
        if unit is None:
            raise falcon.HTTPInvalidParam('the provided unit does not exist', 'Unit')

        # get the remaining values
        due_date = self.db.db_param_varchar(req, 'due_date', required=True)
        submissions_permitted = self.db.db_param_int(req, 'submissions_permitted', required=True)
        marks = self.db.db_param_int(req, 'marks', required=True)
        test_input = self.db.db_param_varchar(req, 'test_input', required=True)
        expected_output = self.db.db_param_varchar(req, 'expected_output', required=True)
        language = self.db.db_param_varchar(req, 'language', required=True)
        description = self.db.db_param_varchar(req, 'description')

        query = 'INSERT INTO assessments (unit_id, year, semester, due_date, submissions_permitted, marks, language, test_input, expected_output, description) ' \
                '               VALUES (\'%s\', %s, %s,  \'%s\',  %s,  %s, \'%s\', \'%s\', \'%s\', \'%s\')' \
                                       % (unit_id, year, semester,due_date, submissions_permitted, marks, language, test_input, expected_output, description)
        # commit new entry to database
        new_id = self.db.database_commit(query)

        # output
        out = {
            'assessment_id': new_id,
            'unit_id': unit_id,
            'year': year,
            'semester': semester,
            'description': description,
            'due_date': due_date,
            'submissions_permitted': submissions_permitted,
            'marks': marks,
            'language': language,
            'test_input': test_input,
            'expected_output': expected_output
        }

        resp.status = falcon.HTTP_200
        resp.body = json.dumps(out, default=utils.json_serial)

    def on_put(self, req, resp):
        # get the assessment
        assessment_id = req.get_param('assessment_id', required=True)
        assessment = self.get_assessment(assessment_id)
        if assessment is None:
            raise falcon.HTTPInvalidParam('the provided assessment does not exist', 'Assessment')

        # dictionary for parameters
        update_params = {}
        # get the remaining values that may need to be updated
        update_params['due_date'] = self.db.db_param_varchar(req, 'due_date')
        update_params['submissions_permitted'] = self.db.db_param_int(req, 'submissions_permitted')
        update_params['marks'] = self.db.db_param_int(req, 'marks')
        update_params['test_input'] = self.db.db_param_varchar(req, 'test_input')
        update_params['expected_output'] = self.db.db_param_varchar(req, 'expected_output')
        update_params['description'] = self.db.db_param_varchar(req, 'description')
        update_params['language'] = self.db.db_param_varchar(req, 'language')

        set_clause = self.db.set_clause_creator(update_params)
        # if there actually was anyhtign to update
        if not set_clause == "":
            query = ("UPDATE assessments " + set_clause + " WHERE assessment_id = %s") % assessment_id
            # update query
            self.db.database_commit(query)

        # output
        out = self.get_assessment(assessment_id)

        resp.status = falcon.HTTP_200
        resp.body = json.dumps(out, default=utils.json_serial)


    def on_delete(self, req, resp):
        # get the assessment
        assessment_id = req.get_param('assessment_id', required=True)
        assessment = self.get_assessment(assessment_id)
        if assessment is None:
            raise falcon.HTTPInvalidParam('the provided assessment does not exist', 'Assessment')

        self.db.database_commit('DELETE FROM assessments WHERE assessment_id = \'%s\'' % assessment['assessment_id'])
