#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import json
import falcon

class Assessments(object):
    def __init__(self, db):
        self.db = db

    def on_get(self, req, resp):
        # get the assessment id to filter by
        unit_id = self.db.db_param_varchar(req, 'unit_id')
        # get the student is
        year = self.db.db_param_int(req, 'year')
        # get the semester
        semester = self.db.db_param_int(req, 'semester')

        # if no student id given, return all
        if unit_id is None and year is None and semester is None:
            query = "SELECT * FROM assessments"
        else:
            where_dict = {'unit_id': unit_id, 'year': year, 'semester': semester}
            # build a where...and clause for filtering
            where_clause = self.db.where_clause_creator(where_dict)
            query = "SELECT * FROM assessments %s" % where_clause

        # get assessments
        assessments = self.db.database_fetch_all(query)
        resp.body = json.dumps(eval(assessments))
        resp.status = falcon.HTTP_200

