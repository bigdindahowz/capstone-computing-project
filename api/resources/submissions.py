#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import json
import falcon
from libs import utils

class Submissions(object):
    def __init__(self, db):
        self.db = db

    def on_get(self, req, resp):
        # get the assessment id to filter by
        assessment_id = self.db.db_param_int(req, 'assessment_id')
        # get the student is
        student_id = self.db.db_param_int(req, 'student_id')
        # get the semester
        is_marked = self.db.db_param_int(req, 'is_marked')


        # if no student id given, return all
        if assessment_id is None and student_id is None and is_marked is None:
            query = "SELECT * FROM submissions ORDER BY created DESC"
        else:
            where_dict = {'assessment_id': assessment_id, 'student_id': student_id, 'is_marked': is_marked}
            # build a where...and clause for filtering
            where_clause = self.db.where_clause_creator(where_dict)
            query = "SELECT * FROM submissions %s ORDER BY created DESC" % where_clause

        # get students
        submissions = self.db.database_fetch_all(query)
        resp.body = json.dumps(eval(submissions))
        resp.status = falcon.HTTP_200
