#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import json
import falcon

class Students(object):

    def __init__(self, db):
        self.db = db

    def on_get(self, req, resp):
        # if the user passes a list of student ids
        student_ids = req.get_param_as_list('student_id')
        # if the user passes a name
        student_name = self.db.db_param_varchar(req, 'student_name')
        #if no student id given, return all
        if student_ids is None and student_name is None:
            query = "SELECT * FROM students"
        else:
            # make the query to find by name first if it exists
            if student_name is not None:
                where_dict = {'name': student_name}
                # build a where...and claause for filtering
                where_clause = self.db.where_clause_creator(where_dict)
                query = "SELECT * FROM students %s" % where_clause
                # if student_ids is not none then add that to the query
                if student_ids is not None:
                    students_string = ','.join(student_ids)
                    query += " AND student_id IN (%s)" % students_string
            # else if there are only student ids to filter by
            else:
                # create a string of passed student ids to gicve to the query
                students_string = ','.join(student_ids)
                query = "SELECT * FROM students WHERE student_id IN (%s)" % students_string

        # get students
        students = self.db.database_fetch_all(query)
        resp.body = students
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        # should be an array of arrays of students (best to convet to json.dumps() before sending) [{student_id:99999999, name:'jimmy'},{student_id:88888888,name:'sam'}]
        students = self.db.db_param_json_list(req, 'students', required=True)
        first = True
        for student in students:
            #sanitise ids and name
            id = self.db.db_param_int(student['student_id'], name='student_id',required=True)
            student_name = self.db.db_param_varchar(student['name'], name='student_id', maxlength=150, required=True)
            if first:
                students_query = "(%s,\"%s\")" % (id, student_name)
                first = False
            else:
                students_query += ",(%s,\"%s\")" % (id, student_name)
        # insert all values, ignore any duplicates or pre existing students
        query = "INSERT INTO students(student_id, name) VALUES %s ON DUPLICATE KEY UPDATE student_id=student_id" % students_query
        self.db.database_commit(query)

        resp.status = falcon.HTTP_200
        resp.body = json.dumps(students)

    def on_delete(self, req, resp):
        student_ids = req.get_param_as_list('students', required=True)
        sanitised_ids = []
        # sanitise ids
        for id in student_ids:
            id = self.db.db_param_int(id, name='student_id', required=True)
            sanitised_ids.append(id)

        query = "DELETE FROM students WHERE student_id IN %s" %  ("("+",".join(sanitised_ids)+")")
        self.db.database_commit(query)
        resp.status = falcon.HTTP_200
