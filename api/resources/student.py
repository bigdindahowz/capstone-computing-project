#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import json
import falcon

class Student(object):

    def __init__(self, db):
        self.db = db

    def get_student(self, id):
        """
        Used by this class to handle getting a single student by ID from the database
        :param id: student_id
        :return:
        """
        query = "SELECT * FROM students WHERE student_id=\'%s\'" % id
        student = self.db.database_fetch_one(query)
        return student

    def on_get(self, req, resp):
        id = req.get_param('student_id', required=True)
        student = self.get_student(id)

        if student is None:
            raise falcon.HTTPBadRequest('Not found', 'Student does not exist')

        # output
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(student)

    def on_post(self, req, resp):
        id = self.db.db_param_int(req, 'student_id', required=True)
        name = self.db.db_param_varchar(req, 'name', maxlength=150, required=True)
        student = self.get_student(id)

        if student is not None:
            raise falcon.HTTPInvalidParam('the provided student already exists','Student')

        query = 'INSERT INTO students (student_id, name) VALUES (\'%s\', \'%s\')' % (id, name)
        #commit new entry to database
        self.db.database_commit(query)

        # output
        out = {
            'student_id': id,
            'name': name,
        }
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(out)


    def on_put(self, req, resp):
        id = req.get_param('student_id', required=True)
        newName = req.get_param('name', required=True)
        newName = self.db.db_param_varchar(newName, name='name', maxlength=150, required=True)
        student = self.get_student(id)

        if student is None:
            raise falcon.HTTPInvalidParam('the provided student does not exist', 'Student')

        self.db.database_commit('UPDATE students SET name = \'%s\' WHERE student_id = \'%s\'' % (newName ,student['student_id']))
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp):
        id = req.get_param('student_id', required=True)
        student = self.get_student(id)

        if student is None:
            raise falcon.HTTPInvalidParam('the provided student does not exist', 'Student')

        self.db.database_commit('DELETE FROM students WHERE student_id = \'%s\'' % student['student_id'])
        resp.status = falcon.HTTP_200
