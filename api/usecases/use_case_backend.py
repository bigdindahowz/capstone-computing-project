
#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import json
# needed to get timestamp (ensures that requests are new and not outdated)
import time
# module used to help form and perform RESTful requests
import requests
# custom library to sign requests
# You can go check out how it works if you like
# Should also just be able to steal it and use it in your app, make it easier
from libs import signature_creator




# API key and secret stored on your app (correlates to the ones stored on the VM database)
#  They are created when the database is initalised
TEST_KEY = "ae73a501-28cc-4807-9484-b9e9419d15ec"
TEST_SECRET = "ed7e45d258c2b97f1e044d593957b6cbf6211fda05b0761527e0bfd5cf9ed5eb"
# uri to where our api is hosted
uri = 'https://api.whiteboardccp.site/'


"""
THE FOLLOWING METHODS ARE METHODS THAT YOU COULD COPY TO YOUR SCRIPT
THEY ARE USED THROUGHOUT THIS USECASE
"""
def getSubmission(id):
    '''
    THIS METHOD CALLS THE API TO RETRIEVE A SUBMISSION BY id
    :param id: THE SUBMISSION id
    :return: THE SUBMISSION IN json FORMAT
    '''
    #NOTE: Must always include timestamp
    params = {'submission_id': id,'signature_timestamp': int(time.time())}
    # create signature
    signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
    header = {"Authorization": TEST_KEY, "Signature": signature}
    # make the request
    result = requests.get(uri+'submission', params=params, headers=header)
    return eval(result.text)

def getAssessment(id):
    '''
    THIS METHOD CALLS THE API TO RETRIEVE AN ASSESSMENT BY id
    :param id: THE ASSESSMENT id
    :return: THE ASSESSMENT IN json FORMAT
    '''
    # NOTE: Must always include timestamp
    params = {'assessment_id': id, 'signature_timestamp': int(time.time())}
    # create signature
    signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
    header = {"Authorization": TEST_KEY, "Signature": signature}
    # make the request
    result = requests.get(uri + 'assessment', params=params, headers=header)
    print(result.text)
    return eval(result.text)

def updateSubmissionMarks(submission_id,marks):
    """
    Can use this method to update the marks for a submission
    :param marks: new marks value
    :return: None
    """
    # NOTE: Must always include timestamp
    params = {'submission_id': submission_id, 'marks': marks, 'signature_timestamp': int(time.time())}
    # create signature
    signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
    header = {"Authorization": TEST_KEY, "Signature": signature}
    # make the request, PUT is updatepp
    requests.put(uri + 'submission', params=params, headers=header)


def updateSubmissionFeedback(submission_id,feedback):
    """
    Can use this method to update the marks for a submission
    :param marks: new marks value
    :return: None
    """
    # NOTE: Must always include timestamp
    params = {'submission_id': submission_id, 'feedback': feedback, 'signature_timestamp': int(time.time())}
    # create signature
    signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
    header = {"Authorization": TEST_KEY, "Signature": signature}
    # make the request, PUT is updatepp
    requests.put(uri + 'submission', params=params, headers=header)


def updateSubmissionRunningTime(submission_id, running_time):
    """
    Can use this method to update the marks for a submission
    :param marks: new marks value
    :return: None
    """
    # NOTE: Must always include timestamp
    params = {'submission_id': submission_id, 'running_time': running_time, 'signature_timestamp': int(time.time())}
    # create signature
    signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
    header = {"Authorization": TEST_KEY, "Signature": signature}
    # make the request, PUT is updatepp
    requests.put(uri + 'submission', params=params, headers=header)

def updateSubmissionMemoryUsage(submission_id, memory_usage):
    """
    Can use this method to update the marks for a submission
    :param marks: new marks value
    :return: None
    """
    # NOTE: Must always include timestamp
    params = {'submission_id': submission_id, 'memory_usage': memory_usage, 'signature_timestamp': int(time.time())}
    # create signature
    signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
    header = {"Authorization": TEST_KEY, "Signature": signature}
    # make the request, PUT is update
    requests.put(uri + 'submission', params=params, headers=header)

def updateSubmissionRunningOutput(submission_id, running_output):
    """
    Can use this method to update the marks for a submission
    :param marks: new marks value
    :return: None
    """
    # NOTE: Must always include timestamp
    params = {'submission_id': submission_id, 'running_output': running_output, 'signature_timestamp': int(time.time())}
    # create signature
    signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
    header = {"Authorization": TEST_KEY, "Signature": signature}
    # make the request, PUT is update
    requests.put(uri + 'submission', params=params, headers=header)


def deleteSubmission(id):
    params = {'submission_id': id, 'signature_timestamp': int(time.time())}
    # create signature
    signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
    header = {"Authorization": TEST_KEY, "Signature": signature}
    # make the request, PUT is update
    res = requests.delete(uri + 'submission', params=params, headers=header)
    print(res.text)

'''
I'm going to write a use case for someone using the backend.
It's kinda how I would imagine the flow of events going in the sandbox...
'''

"""
QUEUE GETS SUBMISSION ID FROM SOMEWHERE AND FIRES UP
SANDBOX WITH THE SUBMISSION ID (STORED IN THE QUEUE I ASSUME)
"""
submission_id_to_be_marked = 1

"""
*INSIDE SANDBOX*
 WITH THE SUBMISSION ID THE SANDBOX RETRIVES THE SUBMISSION
 (COULD MAKE A METHOD TO WRAP THIS)...'getSubmission(id)'
"""
# getting the submission in json format
submission = getSubmission(submission_id_to_be_marked)
print("Retrived submission: %s" % submission)

"""
NOW THAT YOU HAVE THE SUBMISSION YOU CAN EXTRACT THE CODE FROM IT
YOU CAN ALSO EXTRACT THE ASSESSMENT_ID AND THEREFORE THE ASSESSMENT IT IS RELATED TO
"""
# extract the 'submisison' from the submission object
submission_code = submission['submission']
print("The code for the submission is: %s" % submission_code)

# Try to get the related assessment to get marking details
assessment_id = submission['assessment_id']
related_assessment = getAssessment(assessment_id)
print("The assessment this submission is to be marked against is: %s" % related_assessment)
print("The submission should be marked in the language: %s" % related_assessment['language'])
print("The submission should be tested against these inputs: %s" % related_assessment['test_input'])
print("The submission should output this: %s" % related_assessment['expected_output'])

"""
HERE IS PROBABLY WHERE YOU WOULD DO THE MARKING IN THE SANDBOX
"""
# this is where the marking etc... is done
print("Do some processing with the above information...")
print("...Processing done")

"""
NOW THAT IT HAS BEEN MARKED YOU SHOULD UPDATE THE FIELDS IN THE DATABASE THROUGH THE API
"""
# after marking you need to update the information in the database
print("The student got 1/%s marks" % related_assessment['marks'])
print("updating the submission details")
# update marks here
updateSubmissionMarks(submission['submission_id'], 1)
# update the feedback created from the marking
updateSubmissionFeedback(submission['submission_id'], "The submmision was marked with the sandbox")
# update how long it took to run (in seconds or whatever)
updateSubmissionRunningTime(submission['submission_id'], 3)
# update memeory usage
updateSubmissionMemoryUsage(submission['submission_id'], 1000)
# update what was output from running the program
updateSubmissionRunningOutput(submission['submission_id'], "HELLO WORLD")

"""
THE SUBMISSION OBJECT SHOULD NOW BE UPDATED
"""
updated_submission = getSubmission(submission_id_to_be_marked)
print("Updated submission: %s" % updated_submission)

print("deleting submission")
deleteSubmission(submission_id_to_be_marked)
