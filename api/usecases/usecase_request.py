#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import json
# needed to get timestamp (ensures that requests are new and not outdated)
import time
# module used to help form and perform RESTful requests
import requests
# custom library to sign requests
# You can go check out how it works if you like
# Should also just be able to steal it and use it in your app, make it easier
from libs import signature_creator


# API key and secret stored on your app (correlates to the ones stored on the VM database)
#  They are created when the database is initalised
TEST_KEY = "ae73a501-28cc-4807-9484-b9e9419d15ec"
TEST_SECRET = "ed7e45d258c2b97f1e044d593957b6cbf6211fda05b0761527e0bfd5cf9ed5eb"
# uri to where our api is hosted
uri = 'https://api.whiteboardccp.site/'



# create one student
params = {'student_id': 16589944, 'name': "John Lewis", 'signature_timestamp': int(time.time())}
signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
header = {'Authorization': TEST_KEY, 'Signature': signature}
result = requests.post(uri+'student', params=params, headers=header)
print(result.json)
print(result.text)



"""#################################################
GET REQUESTS
################################################"""
#NOTE: Must always include timestamp
params = {'assessment_id': 1,'signature_timestamp': int(time.time())}
# create signature
signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
header = {"Authorization": TEST_KEY, "Signature": signature}
# make the request
result = requests.get(uri+'assessment', params=params, headers=header)
print(result.text)


# A request for getting students. This will GET students with ids seen below
#NOTE: Must always include timestamp
params = {'student_id': [418730, 28285816, 29630003], 'signature_timestamp': int(time.time())}
# create signature
signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
# form header from signature and key
# the API will use your key to get the secret from the database and then decode the signature
# the decoded signature should then match the parameter that were sent
# this is how the authentication works
header = {"Authorization": TEST_KEY, "Signature": signature}
# make the request
result = requests.get(uri+'students', params=params, headers=header)
print(result.text)


# A request for getting enrolment.
#NOTE: Must always include timestamp
params = {'enrolment': [{'student_id': 28285816, 'unit_id': 'COMP1000', 'year': 2017, 'semester': 1}],
          'signature_timestamp': int(time.time())}
params['enrolment'] = ",".join( [ str(element) for element in params['enrolment'] ] )
signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
header = {"Authorization": TEST_KEY, "Signature": signature}
# make the request
result = requests.post(uri+'enrolment', params=params, headers=header)
print(result.text)

params = {'student_id': 28285816, 'signature_timestamp': int(time.time())}
# create signature
signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
# form header from signature and key
# the API will use your key to get the secret from the database and then decode the signature
# the decoded signature should then match the parameter that were sent
# this is how the authentication works
header = {"Authorization": TEST_KEY, "Signature": signature}
# make the request
result = requests.get(uri+'enrolment', params=params, headers=header)
print(result.text)


# A request for getting enrolment.
#NOTE: Must always include timestamp
params = {'unit_id': 'COMP1000', 'signature_timestamp': int(time.time())}
# create signature
signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
# form header from signature and key
# the API will use your key to get the secret from the database and then decode the signature
# the decoded signature should then match the parameter that were sent
# this is how the authentication works
header = {"Authorization": TEST_KEY, "Signature": signature}
# make the request
result = requests.get(uri+'enrolment', params=params, headers=header)
print(result.text)

# A request for getting ALL students.
# by not including any student ids, it will return all students (still need the time stamp)
#NOTE: Must always include timestamp
params = {'signature_timestamp': int(time.time())}
# create signature
signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
# form header from signature and key
header = {"Authorization": TEST_KEY, "Signature": signature}
# make the request
result = requests.get(uri+'students', params=params, headers=header)
print(result.text)

# A request for getting ONE student.
#NOTE: Must always include timestamp
params = {'student_id': 29630003, 'signature_timestamp': int(time.time())}
# create signature
signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
# form header from signature and key
header = {"Authorization": TEST_KEY, "Signature": signature}
# make the request
# NOTE: the resource for one student is different to that for many ('student' VS 'students')
result = requests.get(uri+'student', params=params, headers=header)
print(result.text)


"""#################################################
POST REQUESTS = Create
################################################"""
# create a new unit
# Put the parameters needed into the json
params = {'unit_id': 'TEST4000', 'year': 2018, 'semester': 1, 'unit_name': "TEST UNIT",
          'signature_timestamp': int(time.time())}
# create signature
signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
# form header
header = {"Authorization": TEST_KEY, 'Signature': signature}
# make the request
# I also made a delete request here (need all the primary keys in the parameters to do so (Unit code, year, semester))
# I did this so that you can keep running these use case without getting a HTTP-400 message due to trying to add an already existing entry
requests.delete(uri+'unit', params=params, headers=header)
result = requests.post(uri+'unit', params=params, headers=header)
print(result.text)


# POST multiple students at once
# here is the multiple post of students (NOTE the format the student object is in)
students = [{'student_id': 99999999, 'name': "Jimmy"}, {'student_id': 77777777, 'name': "Sam"}]
params = {"students": json.dumps(students), 'signature_timestamp': int(time.time())}
signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
header = {'Authorization': TEST_KEY, 'Signature': signature}
result = requests.post(uri+'students', params=params, headers=header)
print(result.json)
# going to do a quick DELETE request to clean up
params = {"students": [99999999, 77777777], 'signature_timestamp': int(time.time())}
signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
header = {'Authorization': TEST_KEY, 'Signature': signature}
result = requests.delete(uri+'students', params=params, headers=header)

# create one student
params = {'student_id': 99999999, 'name': "Jimmy", 'signature_timestamp': int(time.time())}
signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
header = {'Authorization': TEST_KEY, 'Signature': signature}
result = requests.post(uri+'student', params=params, headers=header)
print(result.json)
print(result.text)
params = {"students": [99999999], 'signature_timestamp': int(time.time())}
signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
header = {'Authorization': TEST_KEY, 'Signature': signature}
result = requests.delete(uri+'students', params=params, headers=header)
