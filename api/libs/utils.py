#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
from datetime import datetime, time


def json_serial(val):
    """JSON serializer for handling non-primitive types (datetime so far)"""

    if isinstance(val, datetime):
        text = val.isoformat()
        return text

    if isinstance(val, time):
        text = val.strftime('%H:%M:%S %Z')
        return text

    if val is None:
        return ''

    raise TypeError('Value not serializable')
