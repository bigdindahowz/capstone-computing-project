#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
import falcon
from libs import server_authentication
from resources import units, unit
from resources import students, student
from resources import assessments, assessment
from resources import submissions, submission
from resources import enrolment
from resources import api_key
from resources import testing

def add_routes(app,db):
    app.add_route('/units/', units.Units(db))
    app.add_route('/unit/', unit.Unit(db))
    app.add_route('/students/', students.Students(db))
    app.add_route('/student/', student.Student(db))
    app.add_route('/assessments/', assessments.Assessments(db))
    app.add_route('/assessment/', assessment.Assessment(db))
    app.add_route('/submissions/', submissions.Submissions(db))
    app.add_route('/submission/', submission.Submission(db))
    app.add_route('/enrolment/', enrolment.Enrolment(db))
    app.add_route('/apikey/', api_key.ApiKey(db))
    # for testing
    app.add_route('/example', testing.ExampleResource())

def create_app(db):
    app = falcon.API(media_type='application/json;  charset=UTF-8', middleware=[server_authentication.AuthMiddleware(db)])
    add_routes(app,db)
    return app
