#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
"""
for handling authentication by checking the request is signed with an API secret.
"""

import time
import falcon

from libs import signature_creator

class AuthMiddleware(object):
    SIGNATURE_VALID_THRESHOLD = 300  # 5 minutes

    def __init__(self, database, check=True):
        self.database = database
        self.check = True

    def process_request(self, req, resp):
        """
        This handles checking the request is authorised by checking the request signature

        :param req:
        :param resp:
        """
        api_key = req.get_header('Authorization')
        signature = req.get_header('Signature')

        # Guarantee that all requests are unique by making all requests require a timestamp that is recent
        if 'signature_timestamp' not in req.params:
            description = ('All requests must have a signature unix timestamp attached to them as a parameter.')
            raise falcon.HTTPUnauthorized('Timestamp required', description)
        else:
            server_time = int(time.time())
            request_time = int(req.params['signature_timestamp'])
            if (server_time - request_time > self.SIGNATURE_VALID_THRESHOLD):
                description = (
                    'The signature time is stale, current threshold: %s seconds' % self.SIGNATURE_VALID_THRESHOLD)
                raise falcon.HTTPUnauthorized('Signature expired', description)

        if api_key is None:
            description = ('You must provide an API key and a signature of the request as part of the request.')
            raise falcon.HTTPUnauthorized('Key required', description)

        if signature is None:
            description = ('You must provide an API key and a signature of the request as part of the request.')
            raise falcon.HTTPUnauthorized('Signature required', description)
        valid, exists, calculated = self.check_signature(api_key, signature, req)
        if not valid:
            if exists:
                    digest = ''
                    parameters = req.params
                    for key in sorted(parameters.keys()):
                        digest += str(key) + '\n'
                        digest += str(parameters[key]) + '\n'
                    description = (
                    'The calculated signature is not equal to the signature provided. Expected signature: %s Digest (as strings):\n%s' % (calculated, digest))
                    raise falcon.HTTPUnauthorized('Signature invalid', description)
            else:
                description = ('The provided API key does not exist')
                raise falcon.HTTPUnauthorized('Signature invalid', description)

    def check_signature(self, api_key, signature, req):
        """
        Get the secret from the database with a given API key, sign the request and compare it with the provided signature

        :param api_key: the api key sent with the request
        :param signature:  the provided signature to check
        :param req: the falcon request

        :rtype: boolean
        :return: whether or not the signature is valid
        """
        valid = False
        exists = False
        calculated = None
        parameters = falcon.uri.parse_query_string(req.query_string)
        record = self.database.database_fetch_one('SELECT * FROM api_keys WHERE api_key=\'%s\'' % api_key)
        if record is not None:
            exists = True
            auth = signature_creator.SignatureCreator(record['secret'])
            valid, calculated = auth.check_request(parameters, signature)
        return valid, exists, calculated
