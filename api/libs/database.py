#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#
'''Module used for handling the database'''

import sqlalchemy
import falcon
import json
import ast
from libs import utils


class Database(object):

    def __init__(self, username, password, host, database):
        self.database = database
        self.username = username
        self.password = password
        self.host = host

        # url for creating the databsase engine
        # in format "dialect+driver://username:password@host:port/database"
        database_url = 'mysql+pymysql://'+self.username+':'+self.password+'@'+self.host+'/'+self.database
        self.engine = sqlalchemy.create_engine(database_url)

    def database_commit(self, query):
        self.engine.execute(query)

        # return the last insert id ( will only return a valid value for inserts)
        if "INSERT" in query:
            created = self.engine.execute("SELECT LAST_INSERT_ID()").fetchone()
            if created is not None:
                return created[0]
        else:
            return None

    def database_commit_prepared(self, query, values):
        self.engine.execute(query, tuple(values))

        # return the last insert id ( will only return a valid value for inserts)
        if "INSERT" in query:
            created = self.engine.execute("SELECT LAST_INSERT_ID()").fetchone()
            if created is not None:
                return created[0]
        else:
            return None


    def database_fetch_all(self, query):
        results = self.engine.execute(query)
        result = results.fetchall()
        if result is not None:
            #convert to json
            result = json.dumps([dict(r) for r in result], default=utils.json_serial)
        return result

    def database_fetch_one(self, query):
        results = self.engine.execute(query)
        result = results.fetchone()
        if result is not None:
            #convert to dict
            result = dict(result)
        return result

    def where_clause_creator(self, dict):
        """
        Takes a dict of key/values and builds a where clause string (inclusive of where)
        No leading or trailing space ie 'WHERE col='val' AND col2='val2''
        :param dict:
        """
        clause = 'WHERE'
        first = True
        for key in dict:
            if not dict[key] is None:
                if first:
                    clause += ' %s=\'%s\'' % (key, dict[key])
                    first = False
                else:
                    clause += ' AND %s=\'%s\'' % (key, dict[key])
        return clause

    def set_clause_creator(self, dict):
        """
        Takes a dict of key/values and builds a set clause string
        No leading or trailing space ie 'SET (a,b,c,d)=(Va,Vb,Vc,Vd)'
        :param dict:
        """
        clause = "SET "
        first = True
        for key in dict:
            if not dict[key] is None:
                if first:
                    clause += "%s=\'%s\'" % (key, dict[key])
                    first = False
                else:
                    clause += ", %s=\'%s\'" % (key, dict[key])
        if first:
            clause = ""
        return clause

    def db_param_int(self, req, name="", min=None, max=None, required=False, default=None):
        """
        Take a parameter and converts it to its respective type, and checks constraints on it
        if the parameter breaks these constraints it will raise a HTTPInvalidParam exception

        :param req: the falcon request
        :param name: the name of the parameter
        :param min: the min value allowed, default None
        :param max: the max value allowed, default None
        :param required: whether or not the parameter is required
        :param default:  default value if the parameter is missing
        :return: the param after all checks are complete
        """
        # allows you to pass a request and handle it
        # allows you to pass a request and handle it
        if isinstance(req, falcon.request.Request):
            param = req.get_param(name, required=required, default=default)
        else:
            param = req

        if param is not None:
            try:
                value = int(param)
            except ValueError:
                raise falcon.HTTPInvalidParam('Invalid value', name)
            if max is not None and min is not None:
                if value > max or value < min:
                    raise falcon.HTTPInvalidParam('Invalid value', name)
        return param

    def db_param_real(self, req, name="", required=False, default=None):
        """
        Take a parameter and converts it to its respective type, and checks constraints on it
        if the parameter breaks these constraints it will raise a HTTPInvalidParam exception

        :param req: the falcon request
        :param name: the name of the parameter
        :param required: whether or not the parameter is required
        :param default:  default value if the parameter is missing
        :return: the param after all checks are complete
        """
        # allows you to pass a request and handle it
        if isinstance(req, falcon.request.Request):
            param = req.get_param(name, required=required, default=default)
        else:
            param = req

        if not param is None:
            try:
                float(param)
            except ValueError:
                raise falcon.HTTPInvalidParam('Invalid value', name)
        return param

    def db_param_varchar(self, req, name="", maxlength=None, required=False, default=None):
        """
        Take a parameter and converts it to its respective type, and checks constraints on it
        if the parameter breaks these constraints it will raise a HTTPInvalidParam exception

        :param req: the falcon request
        :param name: the name of the parameter
        :param maxlength: the max length allowed, default None
        :param required: whether or not the parameter is required
        :param default:  default value if the parameter is missing
        :return: the param after all checks are complete
        """
        # allows you to pass a request and handle it
        if isinstance(req, falcon.request.Request):
            param = req.get_param(name, required=required, default=default)
        else:
            param = req

        if not param is None:
            if not maxlength is None:
                if len(param) > maxlength:
                    raise falcon.HTTPInvalidParam('Value length exceeded', name)
        if param is None:
            param = default
        if param is not None:
            param = str(param).replace('\'', '')
        return param

    def db_param_bool(self, req, name="", required=False, default=None):
        """
        Checks a parameter is a valid boolean string, and returns the respective boolean of the type.
        If the paramater is none, the default will be set, otherwise an exception will be thrown if an unexpected value is provided.

        :param req: the falcon request
        :param name: name of paramater
        :param required: is it required
        :param default: default value
        :return:

        """
        # allows you to pass a request and handle it
        if isinstance(req, falcon.request.Request):
            param = req.get_param(name, required=required, default=default)
        else:
            param = req

        if param is not None:
            if param.lower() == 'yes' or param.lower() == 'y' or param.lower() == '1' or param.lower() == 't' or param.lower() == 'true':
                bool_val = 1
            elif param.lower() == 'no' or param.lower() == 'n' or param.lower() == '0' or param.lower() == 'f' or param.lower() == 'false':
                bool_val = 0
            else:
                raise falcon.HTTPInvalidParam('Invalid value given for paramater %s' % name, name)

        if param is None:
            bool_val = default
        return bool_val


    def db_param_json_list(self, req, name="", required=False, default=None):
        '''

        :param req:
        :param name:
        :param required:
        :param default:
        :return:
        '''
        if isinstance(req, falcon.request.Request):
            param = req.get_param_as_list(name, required=required)
        else:
            param = req

        param_list = []
        if param is not None:
            if isinstance(param, list):
                flat_string = ",".join(param)
                # evaluate
                params = eval(flat_string)
                # if the result is a tuple (should be a list)
                if isinstance(params, tuple):
                    param_list = list(params)
                # if an actual list
                elif isinstance(params, list):
                    param_list = params
                # else if it was a single json, add it to a list
                else:
                    param_list.append(params)

        return param_list
