#!/bin/sh

# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

if [ "$1" = "api" ]
then
	echo "not yet implemented"
elif [ "$1" = "website" ]
then
	echo "seting up website front end"
    sleep 2
elif [ "$1" = "sandbox" ]
then
	echo "setting up sandbox"
    sleep 2
    # Get docker
    apt-get update
    apt-get install python3.6
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    apt-get update
    apt-cache policy docker-ce
    apt-get install -y docker-ce

    # create docker containers
    cd Server\ Sync
    cd sandbox/Images/C
    docker build -t whiteboard_sandbox_c .
    cd ..
    cd C++
    docker build -t whiteboard_sandbox_cpp .
    cd ..
    cd Java
    docker build -t whiteboard_sandbox_java .
    cd ..
    cd MariaDB
    docker build -t whiteboard_sandbox_mysql .
    cd ..
    cd Python
    docker build -t whiteboard_sandbox_python .

    echo "\nNow run sandbox/SandboxQueue.py with python3"
else
	echo "use argument 'api', 'website' or 'sandbox' to setup the choosen section"
fi

