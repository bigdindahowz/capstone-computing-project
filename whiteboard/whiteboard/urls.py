#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

"""whiteboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls.static import static
from django.urls import include, path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth import views as logout_view
from django.conf import settings
from django.conf.urls import include, url
from app import views

urlpatterns = [	
	# ========== BASE LINKS ==========
	url(r'^$', views.index, name = "index"),
	path(r'^$', include('django.contrib.auth.urls')),
	url(r'^home/$', views.home, name = "home"),
	url(r'^settings/$', views.settings, name = "settings"),
	url(r'^registration/$', views.registration, name = "registration"),
	
	# ========== HOME LINKS ==========
	url(r'^assessments/$', views.assessments, name = "assessments"),
	url(r'^announcements/$', views.announcements, name = "announcements"),
	url(r'^unitmaterials/$', views.unitmaterials, name = "unitmaterials"),
	url(r'^submissionform/$', views.submissionform, name = "submissionform"),
	url(r'^viewassessmentsubmissions/$', views.viewassessmentsubmissions, name = "viewassessmentsubmissions"),
	
	# ========== SETTING LINKS ==========
	url(r'^assessmentsettings/$', views.assessmentsettings, name = "assessmentsettings"),
	url(r'^assessmentsettingshome/$', views.assessmentsettingshome, name = "assessmentsettingshome"),
	url(r'^submissionsettings/$', views.submissionsettings, name = "submissionsettings"),
	url(r'^submissionsettingshome/$', views.submissionsettingshome, name = "submissionsettingshome"),
	url(r'^submission_settings_assessments/$', views.submission_settings_assessments, name = "submission_settings_assessments"),
	url(r'^unitsettings/$', views.unitsettings, name = "unitsettings"),
	url(r'^enrolments/$', views.enrolments, name = "enrolments"),
	url(r'^students/$', views.students, name = "students"),
	url(r'^theme/$', views.theme, name = "theme"),
	
	# ========== ADDING FORM LINKS ==========
	url(r'^addunit/$', views.addunit, name = "addunit"),
	url(r'^addassessment/$', views.addassessment, name = "addassessment"),
	url(r'^addstudent/$', views.addstudent, name = "addstudent"),
	url(r'^setenrolment/$', views.setenrolment, name = "setenrolment"),
	
	# ========== REDIRECTION TO FORMS LINKS ==========
	url(r'^assessmentToForm/$', views.assessmentToForm, name = "assessmentToForm"),
	url(r'^assessmentToEdit/$', views.assessmentToEdit, name = "assessmentToEdit"),
	url(r'^submissionToEdit/$', views.submissionToEdit, name = "submissionToEdit"),
	url(r'^unitToEdit/$', views.unitToEdit, name = "unitToEdit"),
	url(r'^studentToEdit/$', views.studentToEdit, name = "studentToEdit"),
	
	# ========== RE-EVALUATING FORMS LINKS ==========
	url(r'^editunit/$', views.editunit, name = "editunit"),
	url(r'^editassessment/$', views.editassessment, name = "editassessment"),
	url(r'^editsubmission/$', views.editsubmission, name = "editsubmission"),
	url(r'^editstudent/$', views.editstudent, name = "editstudent"),
	url(r'^viewenrolments/$', views.viewenrolments, name = "viewenrolments"),
	
	# ========== DOWNLOAD LINKS ==========
	url(r'^downloadassessment/$', views.downloadassessment, name = "downloadassessment"),
	url(r'^download_csv_submissions/$', views.download_csv_submissions, name = "download_csv_submissions"),
	url(r'^download_xml_assessments/$', views.download_xml_assessments, name = "download_xml_assessments"),
	url(r'^upload_csv_students/$', views.upload_csv_students, name = "upload_csv_students"),
	url(r'^upload_csv_units/$', views.upload_csv_units, name = "upload_csv_units"),
	url(r'^upload_xml_assessments/$', views.upload_xml_assessments, name = "upload_xml_assessments"),
	
	# ========== ERROR LINKS ==========
	url(r'^error400/$', views.error400, name = "error400"),
	url(r'^error403/$', views.error403, name = "error403"),
	url(r'^error404/$', views.error404, name = "error404"),
	url(r'^error500/$', views.error500, name = "error500"),
	url(r'^error503/$', views.error503, name = "error503"),
	
	url(r'^admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
