# README #
### Whiteboard website/front end###

### Requirements ###
    python3
    libpqdev
    python3-pip
    apache2
    libapache2-mod-wsgi
    Django version 2.0.6

### How do I get set up? ###
*Step 1: download requisites and django, run these with root permissions

    apt-get update
    apt-get install python3 libpq-dev
    apt-get install python3-pip
    apt-get install apache2 apache2-utils libexpat1 ssl-cert
    apt-get install libapache2-mod-wsgi
    pip3 install Django==2.0.6
    
*Step 2: running from within whiteboard folder

    python3 manager.py runserver [ip:port]
    
    e.g.for local ip
    python3 manager.py runserver 0:8000

