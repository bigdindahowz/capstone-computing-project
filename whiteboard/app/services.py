#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.template import loader
from xml.dom import minidom

import time

from app import signature_creator

import requests

TEST_KEY = "ae73a501-28cc-4807-9484-b9e9419d15ec"
TEST_SECRET = "ed7e45d258c2b97f1e044d593957b6cbf6211fda05b0761527e0bfd5cf9ed5eb"

uri = 'https://api.whiteboardccp.site/'

null = None

def make_req(verb, resource, params={}):
	if(resource == 'enrolment' and verb == 'POST') or (resource == 'enrolment' and verb == 'DELETE'):
		params['signature_timestamp'] = int(time.time())
		params['enrolment'] = ",".join( [ str(element) for element in params['enrolment'] ] )
	else:
		params['signature_timestamp'] = int(time.time())
	
	signature = signature_creator.SignatureCreator(TEST_SECRET).sign_request(params)
	header = {'Authorization': TEST_KEY, 'Signature': signature}
	
	if verb == 'GET':
		resp = requests.get(uri+resource, params=params, headers=header)
	elif verb == 'POST':
		resp = requests.post(uri+resource, params=params, headers=header)
	elif verb == 'PUT':
		resp = requests.put(uri+resource, params=params, headers=header)
	elif verb == 'DELETE':
		resp = requests.delete(uri+resource, params=params, headers=header)
		
	if resp.text is not '':
		resp = eval(resp.text)
		
	return resp
	
# ==================== GET METHODS ====================	 
def get_student(student_id):
	params = {'student_id': student_id}
	student = make_req('GET', student, params)
	return student	
	
def get_students(student_id = '', student_name = ''):
	params = {}
	if (student_id is '') & (student_name is ''):
		students_list = make_req('GET', 'students')
	else:
		if student_id is not '':
			params['student_id'] = student_id
		if student_name is not '':
			params['student_name'] = student_name
		students_list = make_req('GET', 'students', params)
	return students_list

def get_unit(unit_id, year, semester):
	params = {'unit_id': unit_id, 'year': year, 'semester': semester}
	unit = make_req('GET', 'unit', params)
	return unit

def get_units(unit_id = '', unit_name = '', year = '', semester = ''):
	params = {}
	if (unit_id is '') & (unit_name is '') & (year is '') & (semester is ''):
		units_list = make_req('GET', 'units')
	else:
		if unit_id is not '':
			params['unit_id'] = unit_id
		if unit_name is not '':
			params['unit_name'] = unit_name
		if year is not '':
			params['year'] = year
		if semester is not '':
			params['semester'] = semester
		units_list = make_req('GET', 'units', params)
	return units_list

def get_assessment(assessment_id):
	if (assessment_id is ''):
		assessment = make_req('GET', 'assessment')
	else:
		params = {'assessment_id': assessment_id}
		assessment = make_req('GET', 'assessment', params)
	return assessment

def get_assessments(unit_id = '', year = '', semester = ''):
	params = {}
	if (unit_id is '') & (year is '') & (semester is ''):
		assessments_list = make_req('GET', 'assessments')
	else:
		if unit_id is not '':
			params['unit_id'] = unit_id
		if year is not '':
			params['year'] = year
		if semester is not '':
			params['semester'] = semester
		assessments_list = make_req('GET', 'assessments', params)
	return assessments_list
	
def get_enrolments(student_id = '', unit_id = ''):
	params = {}
	if (student_id is '') & (unit_id is ''):
		enrolments = make_req('GET', 'enrolment')
	else:
		if (student_id is not ''):
			params['student_id'] = student_id
		if (unit_id is not ''):
			params['unit_id'] = unit_id
		enrolments = make_req('GET', 'enrolment', params)
	return enrolments

def get_submission(submission_id):
	params = {'submission_id': submission_id}
	submission = make_req('GET', 'submission', params)
	return submission

def get_submissions(assessment_id = '', student_id = ''):
	params = {}
	if (assessment_id is '') & (student_id is ''):
		submissions_list = make_req('GET', 'submissions')
	else:
		if (assessment_id is not ''):
			params['assessment_id'] = assessment_id
		if (student_id is not ''):
			params['student_id'] = student_id
		submissions_list = make_req('GET', 'submissions', params)
	return submissions_list
	
# ==================== POST METHODS ====================	
def create_student(student_id, name):
	params = {'student_id': student_id, 'name': name}
	resp = make_req('POST', 'student', params)
	
def create_students(students):
	params = {'students': students}
	resp = make_req('POST', 'students', params)

def create_unit(unit_id, unit_name, year, semester):
	params = {'unit_id': unit_id, 'unit_name': unit_name, 'year': year, 'semester': semester}
	resp = make_req('POST', 'unit', params)

def create_assessment(unit_id, due_date, submissions_permitted, marks, year, semester, test_input, expected_output, language, description = ''):
	params = {'unit_id': unit_id, 'due_date': due_date, 'submissions_permitted': submissions_permitted, 'marks': marks, 'year': year, 'semester': semester, 'test_input': test_input, 'expected_output': expected_output, 'language': language}
	if (description is not ''):
		params['description'] = description
	resp = make_req('POST', 'assessment', params)
	
def set_enrolment(enrolment):
	params = {'enrolment': enrolment}
	resp = make_req('POST', 'enrolment', params)

def create_submission(assessment_id, student_id, submission):
	params = {'assessment_id': assessment_id, 'student_id': student_id, 'submission': submission}
	resp = make_req('POST', 'submission', params)
	
# ==================== DELETE METHODS ====================
def delete_students(students):
	params = {'students': students}
	resp = make_req('DELETE', 'students', params)
	
def delete_student(student_id):
	params = {'student_id': student_id}
	resp = make_req('DELETE', 'student', params)

def delete_unit(unit_id, year, semester):
	params = {'unit_id': unit_id, 'year': year, 'semester': semester}
	resp = make_req('DELETE', 'unit', params)

def delete_assessment(assessment_id):
	params = {'assessment_id': assessment_id}
	resp = make_req('DELETE', 'assessment', params)
	
def delete_enrolment(enrolment):
	params = {'enrolment': enrolment}
	resp = make_req('DELETE', 'enrolment', params)

def delete_submission(submission_id):
	params = {'submission_id': submission_id}
	resp = make_req('DELETE', 'submission', params)
	
# ==================== PUT METHODS ====================
def update_student(student_id, name):
	params = {'student_id': student_id, 'name': name}
	resp = make_req('PUT', 'student', params)

def update_unit(unit_id, year, semester, unit_name):
	params = {'unit_id': unit_id, 'year': year, 'semester': semester, 'unit_name': unit_name}
	resp = make_req('PUT', 'unit', params)
	
def update_assessment(assessment_id, description = '', due_date = '', submissions_permitted = '', marks = '', test_input = '', expected_output = '', language = ''):
	params = {}
	params['assessment_id'] = assessment_id
	if (description is not ''):
			params['description'] = description
	if (due_date is not ''):
			params['due_date'] = due_date
	if (submissions_permitted is not ''):
			params['submissions_permitted'] = submissions_permitted
	if (marks is not ''):
			params['marks'] = marks
	if (test_input is not ''):
			params['test_input'] = test_input
	if (expected_output is not ''):
			params['expected_output'] = expected_output
	if (language is not ''):
			params['language'] = language
	resp = make_req('PUT', 'assessment', params)
	
def update_submission(submission_id, submission = '', mark = '', feedback = '', is_marked = '', running_time = '', running_output = '', memory_usage = ''):
	params = {}
	params['submission_id'] = submission_id
	if (submission is not ''):
			params['submission'] = submission
	if (mark is not ''):
			params['mark'] = mark
	if (feedback is not ''):
			params['feedback'] = feedback
	if (is_marked is not ''):
			params['is_marked'] = is_marked
	if (running_time is not ''):
			params['running_time'] = running_time
	if (running_output is not ''):
			params['running_output'] = running_output
	if (memory_usage is not ''):
			params['memory_usage'] = memory_usage
	resp = make_req('PUT', 'submission', params)

# ==================== SEARCH METHODS ====================	
def search_assessments(filteroption, search, unit_id = '', year = '', semester = ''):
	assessments_list = []
	if(search is not ''):
		if(filteroption == 'search_assessment_id'):
			assessment = get_assessment(search)
			assessments_list.append(assessment)
	else:
		assessments_list = get_assessments(unit_id, year, semester)
	return assessments_list
	
def search_units(filteroption, search):
	units_list = []
	if(search is not ''):
		if(filteroption == 'search_unit_id'):
			units_list = get_units(search)
		elif(filteroption == 'search_unit_name'):
			units_list = get_units('', search)
		elif(filteroption == 'search_year'):
			units_list = get_units('', '', search)
		elif(filteroption == 'search_semester'):
			summer = 'Summer'
			semesterOne = "Semester1"
			semesterTwo = "Semester2"
			search = search.replace(" ", "")
			
			if(search.casefold() == summer.casefold()) or (search == '3'):
				units_list = get_units('', '', '', '3')
			if(search.casefold() == semesterTwo.casefold()) or (search == '2'):
				units_list = get_units('', '', '', '2')
			if(search.casefold() == semesterOne.casefold()) or (search == '1'):
				units_list = get_units('', '', '', '1')
	return units_list
	
def search_submissions(filteroption, search, assessment_id = ''):
	submissions_list = []
	submission = None
	if(search is not ''):
		if(filteroption == 'search_student_id'):
				submissions_list = get_submissions('', search)
		elif(filteroption == 'search_submission_id'):
				submission = get_submission(search)
				submissions_list = []
				submissions_list.append(submission)
	else:
		submissions_list = get_submissions(assessment_id)
	return submissions_list
	
def search_students(filteroption, search):
	students_list = []
	if(search is not ''):
		if(filteroption == 'search_student_id'):
			students_list = get_students(search)
		elif(filteroption == 'search_student_name'):
			print('here')
			students_list = get_students('', search)
	return students_list
	
# ==================== UPLOAD AND DOWNLOAD METHODS ====================	
def submissions_to_csv(submissions_list):
	f = open("submission_CSV.txt", "w+")
	
	for submissions in submissions_list:
		assessment_id = submissions['assessment_id']
		student_id = submissions['student_id']
		submission = submissions['submission']
		mark = submissions['mark']
		is_marked = submissions['is_marked']
		submission_id = submissions['submission_id']
		memory_usage = submissions['memory_usage']
		attempts_made = submissions['attempts_made']
		feedback = submissions['feedback']
		running_time = submissions['running_time']
		running_output = submissions['running_output']
		created = submissions['created']
		f.write("%d,%d,%s,%d,%d,%d,%d,%d,%s,%d,%s,%s\n" % (assessment_id, student_id, submission, mark, is_marked, submission_id, memory_usage, attempts_made, feedback, running_time, running_output, created))
		
	f.close()
	
def assignments_to_xml(assessment_id, unit_id, year, semester, description, due_date, submissions_permitted, marks, test_input, expected_output, language):
	f = open("assessment_XML.xml", "w+")
	
	f.write("<%s>\n" % ("assessment"))
	f.write("	<%s>%s</%s>\n" % ("assessment_id",assessment_id,"assessment_id"))
	f.write("	<%s>%s</%s>\n" % ("unit_id",unit_id,"unit_id"))
	f.write("	<%s>%s</%s>\n" % ("year",year,"year"))
	f.write("	<%s>%s</%s>\n" % ("semester",semester,"semester"))
	f.write("	<%s>%s</%s>\n" % ("description",description,"description"))
	f.write("	<%s>%s</%s>\n" % ("due_date",due_date,"due_date"))
	f.write("	<%s>%s</%s>\n" % ("submissions_permitted",submissions_permitted,"submissions_permitted"))
	f.write("	<%s>%s</%s>\n" % ("marks",marks,"marks"))
	f.write("	<%s>%s</%s>\n" % ("test_input",test_input,"test_input"))
	f.write("	<%s>%s</%s>\n" % ("expected_output",expected_output,"expected_output"))
	f.write("	<%s>%s</%s>\n" % ("language",language,"language"))
	f.write("</%s>" % ("assessment"))
	
	f.close()
		
def processCSVStudents(csvFile):
	fileLines = csvFile.readlines()
	for studentInfo in fileLines:
		studentInfo = studentInfo.decode('utf-8').split(',')
		if len(studentInfo) == 2:
			try:
				student_id = int(studentInfo[0])
				name = studentInfo[1].rstrip()
				create_student(student_id, name)
			except ValueError:
				print("\tIgnoring student: " + ', '.join(map(str, studentInfo)))
		else:
			print("\tIgnoring student: " + ', '.join(map(str, studentInfo)))
		
def processCSVUnits(csvFile):
	print('=================')
	fileLines = csvFile.readlines()
	for unitInfo in fileLines:
		print(unitInfo)
		unitInfo = unitInfo.decode('utf-8').split(',')
		print(unitInfo)
		if len(unitInfo) == 4:
			try:
				unit_id = unitInfo[0]
				unit_name = unitInfo[1]
				year = int(unitInfo[2])
				semester = int(unitInfo[3])
				create_unit(unit_id, unit_name, year, semester)
			except ValueError:
				print("\tIgnoring unit: " + ', '.join(map(str, unitInfo)))
		else:
			print("\tIgnoring unit: " + ', '.join(map(str, unitInfo)))

def processXMLAssessments(xmlFile):
	try:
		xmldoc = minidom.parse(xmlFile)
		unit_id = xmldoc.getElementsByTagName('unit_id')
		year = xmldoc.getElementsByTagName('year')
		semester = xmldoc.getElementsByTagName('semester')
		description = xmldoc.getElementsByTagName('description')
		due_date = xmldoc.getElementsByTagName('due_date')
		submissions_permitted = xmldoc.getElementsByTagName('submissions_permitted')
		marks = xmldoc.getElementsByTagName('marks')
		test_input = xmldoc.getElementsByTagName('test_input')
		expected_output = xmldoc.getElementsByTagName('expected_output')
		language = xmldoc.getElementsByTagName('language')
		
		unit_id = unit_id[0].firstChild.nodeValue
		year = year[0].firstChild.nodeValue
		semester = semester[0].firstChild.nodeValue
		description = description[0].firstChild.nodeValue
		due_date = due_date[0].firstChild.nodeValue
		submissions_permitted = submissions_permitted[0].firstChild.nodeValue
		marks = marks[0].firstChild.nodeValue
		test_input = test_input[0].firstChild.nodeValue
		expected_output = expected_output[0].firstChild.nodeValue
		language = language[0].firstChild.nodeValue
		
		create_assessment(unit_id, due_date, submissions_permitted, marks, year, semester, test_input, expected_output, language, description)
	except ValueError:
		print("\t Cannot add unit")