#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

import re

# Checks code for illegal arguments
def parse_snippet(code):
	error = 0
	index = 0
	stack = []
	words = re.split(' |\n',code)
	for cur in words:
		print(cur)
		# modify stack accordingly
		if '{' == cur:
			stack.append(cur)
		if '}' == cur:
			if not stack:
				error = 1
			else:
				if stack.pop() is "switch":
					stack.pop()
		# check for illegal funcitions
		if "goto" in cur:
			error = 2
		if "switch" in cur:
			stack.append("switch")
		if "break" in cur:
			if len(stack) >= 2:
				last = stack.pop() 
				last2 = stack.pop() 
				if last2 != "switch":
					error = 1
				stack.append(last)
				stack.append(last2)
			else:
				error = 1
		++index
	if stack:
		error = 1
	return error


def test_code(code):
	error = parse_snippet(code)
	if error == 0:
		print("correct")
	else:
		print("fail")
