#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

import hashlib
import hmac
import ast

class SignatureCreator:
    def __init__(self,secret):
        self.secret = secret

    def sign_request(self, parameters):
        """
        Sign a request of parameters with this instances secret,
        HMAC sha256 is used to sign the request.

        :param parameters: dict of string key pairs {'key':'value', 'key':'value''}
        :return: signature digest
        :rtype: byte array hex digest
        """

        digest = hmac.new(str.encode(self.secret), digestmod=hashlib.sha256)
        for key in sorted(parameters.keys()):
            digest.update(str.encode((str(key))))
            digest.update(str.encode(str(parameters[key])))

        return digest.hexdigest()

    def check_request(self, parameters, checkSignature):
        """
        Signs a request of parameters and this instances secret and checks
        to see if it matches a provided signature.

        :param parameters:
        :param checkSignature:
        :return: boolean whether the provided signature matches a calculated signature with parameters
        """

        # flatten
        flatten = {}
        for parameter in parameters:
            if isinstance(parameters[parameter], list):
                flat_string = ",".join(parameters[parameter])
                # if the result is a single dictionary
                param_list = eval(flat_string)
                # if the result is a tuple (should be a list)
                if isinstance(param_list, tuple):
                    param_list = list(param_list)
                flatten[parameter] = param_list
            else:
                flatten[parameter] = parameters[parameter]

        signature = self.sign_request(flatten)
        # return true if signature is equal to the signature to check
        return signature == checkSignature, signature
