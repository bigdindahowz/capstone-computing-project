#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app import services

from django.shortcuts import redirect, render
import os

# Create your views here.
from django.contrib import messages
from django.contrib.auth.models import Group
from django.contrib.auth.views import LoginView
from django.contrib.auth import login, logout, authenticate
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader, Context
from django.conf import settings
from wsgiref.util import FileWrapper
from .forms import *
from django.contrib.auth.decorators import login_required, user_passes_test

# ========== BASE LINKS ==========
def index(request):
	if request.user.is_authenticated:
		enrolled = []
		units_list = []
		
		enrolments_list = services.get_enrolments()
		
		for enrolments in enrolments_list:
			if(request.user.username.isdigit()):
				if (int(enrolments['student_id']) == int(request.user.username)):
					enrolled.append(enrolments)
					
		for units_enrolled in enrolled:
			unit = services.get_unit(units_enrolled['unit_id'], units_enrolled['year'], units_enrolled['semester'])
			units_list.append(unit)	

		c = {
			'units_list': units_list,
		}
		return render(request, 'home.html', c)
	form = LoginForm(request.POST or None)
	if request.method == "POST":
		if form.is_valid():
			username = form.cleaned_data.get('username')
			password = form.cleaned_data.get('password')
			user = authenticate(username=username, password=password)
			if user:
				login(request, user)
				enrolled = []
				units_list = []
				
				enrolments_list = services.get_enrolments()
				
				for enrolments in enrolments_list:
					if(request.user.username.isdigit()):
						if (int(enrolments['student_id']) == int(request.user.username)):
							enrolled.append(enrolments)
							
				for units_enrolled in enrolled:
					unit = services.get_unit(units_enrolled['unit_id'], units_enrolled['year'], units_enrolled['semester'])
					units_list.append(unit)	

				c = {
					'units_list': units_list,
				}
				return render(request, 'home.html', c)
			else:
				form.add_error(None, "Username or password incorrect")
				return render(request, 'index.html', {'form': form})

	r = loader.get_template('index.html')
	return render(request, 'index.html', {'form': form})

@login_required
def loguserout(request):
	for sesskey in request.session.keys():
		del request.session[sesskey]
	logout_then_login(request)

@login_required
def home(request):
	r = loader.get_template('home.html')
	enrolled = []
	units_list = []
	
	if request.user.is_authenticated:
		enrolments_list = services.get_enrolments()

		for enrolments in enrolments_list:
			if(request.user.username.isdigit()):
				if (int(enrolments['student_id']) == int(request.user.username)):
					enrolled.append(enrolments)
					
		for units_enrolled in enrolled:
			unit = services.get_unit(units_enrolled['unit_id'], units_enrolled['year'], units_enrolled['semester'])
			units_list.append(unit)	

	c = {
		'units_list': units_list,
	}
	return render(request, 'home.html', c)

@login_required
def settings(request):
	r = loader.get_template('settings.html')
	return render(request, 'settings.html')

def registration(request):
	if request.user.is_authenticated:
		enrolled = []
		units_list = []
		
		enrolments_list = services.get_enrolments()
		
		for enrolments in enrolments_list:
			if(request.user.username.isdigit()):
				if (int(enrolments['student_id']) == int(request.user.username)):
					enrolled.append(enrolments)
					
		for units_enrolled in enrolled:
			unit = services.get_unit(units_enrolled['unit_id'], units_enrolled['year'], units_enrolled['semester'])
			units_list.append(unit)	

		c = {
			'units_list': units_list,
		}
		return render(request, 'home.html', c)
	form = RegistrationForm(request.POST or None)
	if request.method == "POST":
		if form.is_valid():
			form.save()
			username = form.cleaned_data.get('username')
			raw_password = form.cleaned_data.get('password1')
			user = authenticate(username=username, password=raw_password)
			if form.cleaned_data.get('status') == 'S':
				group = Group.objects.get(name='Students') 
				group.user_set.add(user)
			if form.cleaned_data.get('status') == 'L':
				group = Group.objects.get(name='Staff Members')
				group.user_set.add(user)
			if form.cleaned_data.get('status') == 'A':
				group = Group.objects.get(name='Whiteboard Administrators') 
				group.user_set.add(user)
			login(request, user)
			
			enrolled = []
			units_list = []
			
			enrolments_list = services.get_enrolments()
			
			for enrolments in enrolments_list:
				if(request.user.username.isdigit()):
					if (int(enrolments['student_id']) == int(request.user.username)):
						enrolled.append(enrolments)
						
			for units_enrolled in enrolled:
				unit = services.get_unit(units_enrolled['unit_id'], units_enrolled['year'], units_enrolled['semester'])
				units_list.append(unit)	

			c = {
				'units_list': units_list,
			}
			return render(request, 'home.html', c)

	r = loader.get_template('registration.html')
	return render(request, 'registration.html', {'form': form}) 

# Checks if the user is a student
def check_student(user):
	return user.groups.filter(name='Students').exists()

# Checks if the user is a teacher
def check_staff(user):
	return user.groups.filter(name__in=['Staff Members', 'Whiteboard Administrators']).exists()

# Checks if the user is an admin
def check_admin(user):
	return user.groups.filter(name='Whiteboard Administrators').exists()

# ========== HOME LINKS ==========
@login_required
def announcements(request):
	r = loader.get_template('announcements.html')
	return render(request, 'announcements.html')

@login_required
def assessments(request):
	r = loader.get_template('assessments.html')
	assessments_list = services.get_assessments()
	uform = UnitSettingsForm()
	
	if request.method == 'POST' and 'search_button' in request.POST:
		form = Search(request.POST)
		uform = UnitSettingsForm(request.POST)
		if(form.is_valid()):
			assessments_list = services.search_assessments(form.cleaned_data['filteroption'], form.cleaned_data['search'])
		elif uform.is_valid():
			assessments_list = services.search_assessments(form.cleaned_data['filteroption'], '', uform.cleaned_data['unit_id'], uform.cleaned_data['year'], uform.cleaned_data['semester'])
	elif request.method == 'POST' and 'assessments_button' in request.POST:
		uform = UnitSettingsForm(request.POST)
		if uform.is_valid():
			assessments_list = services.get_assessments(uform.cleaned_data['unit_id'], uform.cleaned_data['year'], uform.cleaned_data['semester'])
		else:
			return HttpResponseRedirect('/error404/')
	else:
		assessments_list = services.get_assessments()
		uform = UnitSettingsForm()
		
	c = {
		"assessments_list": assessments_list,
		'form': uform,
	}
	return render(request, 'assessments.html', c)
	
@login_required
def unitmaterials(request):
	r = loader.get_template('unitmaterials.html')
	return render(request, 'unitmaterials.html')

#=============	NEED TO LINK WITH PROFILE (STUDENT_ID)=============
@login_required
@user_passes_test(check_student, login_url='/home/', redirect_field_name=None)
def submissionform(request):
	r = loader.get_template('submissionform.html')
	form = SubmissionForm()
	uform = UnitSettingsForm()
	
	if request.method == 'POST' and 'submit_button' in request.POST:
		form = SubmissionForm(request.POST)
		if form.is_valid():
			if request.user.is_authenticated:
				services.create_submission(form.cleaned_data['assessment_id'], request.user.username, form.cleaned_data['answer'])
				uform = UnitSettingsForm(request.POST)
				if uform.is_valid():
					assessments_list = services.get_assessments(uform.cleaned_data['unit_id'], uform.cleaned_data['year'], uform.cleaned_data['semester'])
					c = {
						"assessments_list": assessments_list,
						'form': uform,
					}
					return render(request, 'assessments.html', c)
				else:
					return HttpResponseRedirect('/error403/')
			else:
				return HttpResponseRedirect('/error404/')
	elif request.method == 'POST' and 'cancel_button' in request.POST:
		uform = UnitSettingsForm(request.POST)
		if uform.is_valid():
			assessments_list = services.get_assessments(uform.cleaned_data['unit_id'], uform.cleaned_data['year'], uform.cleaned_data['semester'])
			c = {
				"assessments_list": assessments_list,
				'form': uform,
			}
			return render(request, 'assessments.html', c)
		else:
			return HttpResponseRedirect('/error403/')
	else:
		return HttpResponseRedirect('/error403/')
		
	c = {
		"form": form,
		"uform": uform,
	}
	return render(request, 'submissionform.html', c)
	
# ========== SETTING LINKS ==========
@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def assessmentsettings(request):
	r = loader.get_template('assessmentsettings.html')
	assessments_list = services.get_assessments()
	form = UnitSettingsForm()
	
	if request.method == 'POST' and 'search_button' in request.POST:
		sform = Search(request.POST)
		form = UnitSettingsForm(request.POST)
		if sform.is_valid():
			assessments_list = services.search_assessments(sform.cleaned_data['filteroption'], sform.cleaned_data['search'])
		elif form.is_valid():
			assessments_list = services.search_assessments(sform.cleaned_data['filteroption'], '', form.cleaned_data['unit_id'], form.cleaned_data['year'], form.cleaned_data['semester'])
		else:
			return HttpResponseRedirect('/error404/')
	elif request.method == 'POST' and 'assessments_button' in request.POST:
		form = UnitSettingsForm(request.POST)
		if form.is_valid():
			assessments_list = services.get_assessments(form.cleaned_data['unit_id'], form.cleaned_data['year'], form.cleaned_data['semester'])
		else:
			return HttpResponseRedirect('/error404/')
	elif request.method == 'POST' and 'add_button' in request.POST:
		form = AssessmentSettingsForm()
		uform = UnitSettingsForm(request.POST)
		c = {
			"form": form,
			"uform": uform,
		}
		return render(request, 'addassessment.html', c)
	else:
		return HttpResponseRedirect('/error403/')
	
	c = {
		"assessments_list": assessments_list,
		"form": form,
	}
	return render(request, 'assessmentsettings.html', c)
	
@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def assessmentsettingshome(request):
	r = loader.get_template('assessmentsettingshome.html')
	units_list = services.get_units()
	
	if request.method == 'POST' and 'search_button' in request.POST:
		form = Search(request.POST)
		if(form.is_valid()):
			units_list = services.search_units(form.cleaned_data['filteroption'], form.cleaned_data['search'])
		else:
			return HttpResponseRedirect('/error403/')
	else:
		units_list = services.get_units()
		
	c = {
		"units_list": units_list,
	}
	return render(request, 'assessmentsettingshome.html', c)

@login_required
@user_passes_test(check_admin, login_url='/home/', redirect_field_name=None)
def unitsettings(request):
	r = loader.get_template('unitsettings.html')
	units_list = services.get_units()
	
	if request.method == 'POST' and 'search_button' in request.POST:
		form = Search(request.POST)
		if form.is_valid():
			units_list = services.search_units(form.cleaned_data['filteroption'], form.cleaned_data['search'])
		else:
			return HttpResponseRedirect('/error403/')
	elif request.method == 'POST' and 'add_button' in request.POST:
		form = UnitSettingsForm()
		c = {
			"form": form,
		}
		return render(request, 'addunit.html', c)
	else:
		units_list = services.get_units()
	
	c = {
		"units_list": units_list,
	}
	return render(request, 'unitsettings.html', c)

@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def submissionsettings(request):
	r = loader.get_template('submissionsettings.html')
	submissions_list = services.get_submissions()
	form = UnitSettingsForm()
	aform = AssessmentInfo()
	
	if request.method == 'POST' and 'search_button' in request.POST:
		sform = Search(request.POST)
		aform = AssessmentInfo(request.POST)
		form = UnitSettingsForm(request.POST)
		if sform.is_valid() and aform.is_valid():
			submissions_list = services.search_submissions(sform.cleaned_data['filteroption'], sform.cleaned_data['search'])
		elif aform.is_valid():
			submissions_list = services.search_submissions(sform.cleaned_data['filteroption'], '', aform.cleaned_data['assessment_id'])
		else:
			return HttpResponseRedirect('/error404/')
	elif request.method == 'POST' and 'view_button' in request.POST:
		aform = AssessmentInfo(request.POST)
		form = UnitSettingsForm(request.POST)
		if aform.is_valid():
			submissions_list = services.get_submissions(aform.cleaned_data['assessment_id'])
			services.submissions_to_csv(submissions_list)
		else:
			return HttpResponseRedirect('/error404/')
	else:
		return HttpResponseRedirect('/error403/')
	
	c = {
		"submissions_list": submissions_list,
		"form": form,
		"aform": aform,
	}
	return render(request, 'submissionsettings.html', c)
	
@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def submissionsettingshome(request):
	r = loader.get_template('submissionsettingshome.html')
	units_list = services.get_units()
	
	if request.method == 'POST' and 'search_button' in request.POST:
		form = Search(request.POST)
		if form.is_valid():
			units_list = services.search_units(form.cleaned_data['filteroption'], form.cleaned_data['search'])
		else:
			return HttpResponseRedirect('/error403/')
	else:
		units_list = services.get_units()
		
	c = {
		"units_list": units_list,
	}
	return render(request, 'submissionsettingshome.html', c)
	
@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def submission_settings_assessments(request):
	r = loader.get_template('submission_settings_assessments.html')
	assessments_list = services.get_assessments()
	
	if request.method == 'POST' and 'search_button' in request.POST:
		sform = Search(request.POST)
		form = UnitSettingsForm(request.POST)
		if(sform.is_valid()):
			assessments_list = services.search_assessments(sform.cleaned_data['filteroption'], sform.cleaned_data['search'])
		elif form.is_valid():
			assessments_list = services.search_assessments(sform.cleaned_data['filteroption'], '', form.cleaned_data['unit_id'], form.cleaned_data['year'], form.cleaned_data['semester'])
		else:
			return HttpResponseRedirect('/error403/')
	elif request.method == 'POST' and 'view_button' in request.POST:
		form = UnitSettingsForm(request.POST)
		if form.is_valid():
			assessments_list = services.get_assessments(form.cleaned_data['unit_id'], form.cleaned_data['year'], form.cleaned_data['semester'])
		else:
			return HttpResponseRedirect('/error403/')
	else:
		return HttpResponseRedirect('/error404/')
		
	c = {
		"assessments_list": assessments_list,
		"form": form,
	}
	return render(request, 'submission_settings_assessments.html', c)
	
@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def enrolments(request):
	r = loader.get_template('enrolments.html')
	units_list = services.get_units()
	enrolments_list = services.get_enrolments()
	
	if request.method == 'POST' and 'search_button' in request.POST:
		form = Search(request.POST)
		if(form.is_valid()):
			units_list = services.search_units(form.cleaned_data['filteroption'], form.cleaned_data['search'])
		else:
			return HttpResponseRedirect('/error404/')
	else:
		units_list = services.get_units()
		
	c = {
		'units_list': units_list,
		'enrolments_list': enrolments_list,
	}
	return render(request, 'enrolments.html', c)
	
@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def students(request):
	r = loader.get_template('students.html')
	students_list = services.get_students()
	
	if request.method == 'POST' and 'search_button' in request.POST:
		form = Search(request.POST)
		if(form.is_valid()):
			students_list = services.search_students(form.cleaned_data['filteroption'], form.cleaned_data['search'])
		else:
			students_list = services.get_students()
	elif request.method == 'POST' and 'add_button' in request.POST:
		form = StudentInfo()
		c = {
			"form": form,
		}
		return render(request, 'addstudent.html')
	else:
		students_list = services.get_students()
	
	c = {
		'students_list': students_list,
	}
	return render(request, 'students.html', c)

@login_required
def theme(request):
	profile = request.user.profile

	form = ThemeForm(request.POST or None, instance = profile)
	if request.method == "POST":
		if form.is_valid():
			form.save()
			enrolled = []
			units_list = []
			
			enrolments_list = services.get_enrolments()
			
			for enrolments in enrolments_list:
				if(request.user.username.isdigit()):
					if (int(enrolments['student_id']) == int(request.user.username)):
						enrolled.append(enrolments)
						
			for units_enrolled in enrolled:
				unit = services.get_unit(units_enrolled['unit_id'], units_enrolled['year'], units_enrolled['semester'])
				units_list.append(unit)	

			c = {
				'units_list': units_list,
			}
			return render(request, 'home.html', c)
	r = loader.get_template('theme.html')
	return render(request, 'theme.html', {'form': form})

# ========== ADDING FORM LINKS ==========
@login_required
@user_passes_test(check_admin, login_url='/home/', redirect_field_name=None)
def addunit(request):
	r = loader.get_template('addunit.html')
	form = UnitSettingsForm()
	
	if request.method == 'POST' and 'submit_button' in request.POST:
		form = UnitSettingsForm(request.POST)
		if form.is_valid():
			exists = services.get_unit(form.cleaned_data['unit_id'], form.cleaned_data['year'], form.cleaned_data['semester'])
			if 'unit_id' in exists:
				return HttpResponseRedirect('/error503/')
			resp = services.create_unit(form.cleaned_data['unit_id'], form.cleaned_data['unit_name'], form.cleaned_data['year'], form.cleaned_data['semester'])
			messages.info(request, 'Your password has been changed successfully!')
			units_list = services.get_units()
			c = {
				"units_list": units_list,
			}
			return render(request, 'unitsettings.html', c)
		else:
			return HttpResponseRedirect('/error403/')
	elif request.method == 'POST' and 'cancel_button' in request.POST:
		units_list = services.get_units()
		
		c = {
			"units_list": units_list,
		}
		return render(request, 'unitsettings.html', c)
	else:
		return HttpResponseRedirect('/error403/')
		
	c = {
		"form": form,
	}
	return render(request, 'addunit.html', c)
	
@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def addassessment(request):
	r = loader.get_template('addassessment.html')
	form = AssessmentSettingsForm()
	uform = UnitSettingsForm()
	
	if request.method == 'POST' and 'submit_button' in request.POST:
		form = AssessmentSettingsForm(request.POST)
		if form.is_valid():
			resp = services.create_assessment(form.cleaned_data['unit_id'], form.cleaned_data['due_date'], form.cleaned_data['submissions_permitted'], form.cleaned_data['marks'], form.cleaned_data['year'], form.cleaned_data['semester'], form.cleaned_data['test_input'], form.cleaned_data['expected_output'], form.cleaned_data['language'], form.cleaned_data['description'])
			uform = UnitSettingsForm(request.POST)
			if uform.is_valid():
				assessments_list = services.get_assessments(uform.cleaned_data['unit_id'], uform.cleaned_data['year'], uform.cleaned_data['semester'])
				c = {
					"assessments_list": assessments_list,
					"form": uform,
				}
				return render(request, 'assessmentsettings.html', c)
			else:
				return HttpResponseRedirect('/error404/')
		else:
			return HttpResponseRedirect('/error403/')
	elif request.method == 'POST' and 'cancel_button' in request.POST:
		uform = UnitSettingsForm(request.POST)
		if uform.is_valid():
			assessments_list = services.get_assessments(uform.cleaned_data['unit_id'], uform.cleaned_data['year'], uform.cleaned_data['semester'])
			c = {
				"assessments_list": assessments_list,
				"form": uform,
			}
			return render(request, 'assessmentsettings.html', c)
	else:
		return HttpResponseRedirect('/error403/')
		
	c = {
		"form": form,
		"uform": uform,
	}
	return render(request, 'addassessment.html', c)
	
@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def addstudent(request):
	r = loader.get_template('addstudent.html')
	form = StudentInfo()
	
	if request.method == 'POST':
		form = StudentInfo(request.POST)
		if form.is_valid():
			resp = services.create_student(form.cleaned_data['student_id'], form.cleaned_data['name'])
			students_list = services.get_students()
			c = {
				'students_list': students_list,
			}
			return render(request, 'students.html', c)
	else:
		return HttpResponseRedirect('/error403/')
		
	c = {
		"form": form,
	}
	return render(request, 'addstudent.html')

@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def setenrolment(request):
	r = loader.get_template('setenrolment.html')
	form = StudentInfo()
	
	if request.method == 'POST' and 'submit_button' in request.POST:
		form = EnrolmentInfo(request.POST)
		if form.is_valid():
			params = {}
			params['unit_id'] = form.cleaned_data['unit_id']
			params['year'] = form.cleaned_data['year'] 
			params['semester'] = form.cleaned_data['semester'] 
			params['student_id'] = form.cleaned_data['student_id'] 
			enrolment = []
			enrolment.append(params)
			resp = services.set_enrolment(enrolment)
			
			uform = UnitSettingsForm(request.POST)
			new_student = []
			enrolments_list = services.get_enrolments()
			students_list = services.get_students()
			for students in students_list:
				for enrolments in enrolments_list:
					if (students['student_id'] == enrolments['student_id']) and (uform.cleaned_data['unit_id'] == enrolments['unit_id']) and (int(uform.cleaned_data['semester']) == int(enrolments['semester'])) and (int(uform.cleaned_data['year']) == int(enrolments['year'])):
						new_student.append(students)
			c = {
				'form': uform,
				'students_list': new_student,
			}
			return render(request, 'viewenrolments.html', c)
	if request.method == 'POST' and 'cancel_button' in request.POST:
		enrolments_list = services.get_enrolments()
		students_list = services.get_students()
		new_student = []

		uform = UnitSettingsForm(request.POST)
				
		if uform.is_valid():
			for students in students_list:
				for enrolments in enrolments_list:
					if (students['student_id'] == enrolments['student_id']) and (uform.cleaned_data['unit_id'] == enrolments['unit_id']) and (int(uform.cleaned_data['semester']) == int(enrolments['semester'])) and (int(uform.cleaned_data['year']) == int(enrolments['year'])):
						new_student.append(students)
		c = {
			'form': uform,
			'students_list': new_student,
		}
		return render(request, 'viewenrolments.html', c)
	else:
		return HttpResponseRedirect('/error403/')
		
	c = {
		"form": form,
	}
	return render(request, 'setenrolment.html')

# ========== REDIRECTION TO FORMS LINKS ==========
@login_required	
@user_passes_test(check_student, login_url='/home/', redirect_field_name=None)
def assessmentToForm(request):
	r = loader.get_template('submissionform.html')
	form = AssessmentInfo()
	uform = UnitSettingsForm()
	submissions_list = []
	
	if request.method == 'POST' and 'submit_button' in request.POST:
		form = AssessmentInfo(request.POST)
		uform = UnitSettingsForm(request.POST)
	elif request.method == 'POST' and 'view_button' in request.POST:
		aform = AssessmentInfo(request.POST)
		uform = UnitSettingsForm(request.POST)
		if aform.is_valid():
			submissions_list = services.get_submissions(aform.cleaned_data['assessment_id'], int(request.user.username))
		c = {
			"submissions_list": submissions_list,
			"aform": aform,
			"form": uform,
		}
		return render(request, 'viewassessmentsubmissions.html', c)
	elif request.method == 'POST' and 'download_button' in request.POST:
		form = AssessmentSettingsForm(request.POST)
		aform = AssessmentInfo(request.POST)
		uform = UnitSettingsForm(request.POST)
		print(form)
		if form.is_valid() and aform.is_valid():
			services.assignments_to_xml(aform.cleaned_data['assessment_id'], form.cleaned_data['unit_id'], form.cleaned_data['year'], form.cleaned_data['semester'], form.cleaned_data['description'], form.cleaned_data['due_date'], form.cleaned_data['submissions_permitted'], form.cleaned_data['marks'], form.cleaned_data['test_input'], form.cleaned_data['expected_output'], form.cleaned_data['language'])
			if uform.is_valid():
				return render(request, 'downloadassessment.html')
	else:
		return HttpResponseRedirect('/error404/')
		
	c = {
		"form": form,
		"uform": uform,
	}
	return render(request, 'submissionform.html', c)

@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def assessmentToEdit(request):
	r = loader.get_template('editassessment.html')
	form = AssessmentSettingsForm()
	
	if request.method == 'POST' and 'edit_button' in request.POST:
		form = AssessmentSettingsForm(request.POST)
		aform = AssessmentInfo(request.POST)
		uform = UnitSettingsForm(request.POST)
	elif request.method == 'POST' and 'remove_button' in request.POST:
		form = AssessmentSettingsForm(request.POST)
		aform = AssessmentInfo(request.POST)
		uform = UnitSettingsForm(request.POST)
		if aform.is_valid():
			resp = services.delete_assessment(aform.cleaned_data['assessment_id'])
			uform = UnitSettingsForm(request.POST)
			if uform.is_valid():
				assessments_list = services.get_assessments(uform.cleaned_data['unit_id'], uform.cleaned_data['year'], uform.cleaned_data['semester'])
				c = {
					"assessments_list": assessments_list,
					"form": uform,
				}
				return render(request, 'assessmentsettings.html', c)
		else:
			return HttpResponseRedirect('/error403/')
	elif request.method == 'POST' and 'download_button' in request.POST:
		form = AssessmentSettingsForm(request.POST)
		aform = AssessmentInfo(request.POST)
		uform = UnitSettingsForm(request.POST)
		print(form)
		if form.is_valid() and aform.is_valid():
			services.assignments_to_xml(aform.cleaned_data['assessment_id'], form.cleaned_data['unit_id'], form.cleaned_data['year'], form.cleaned_data['semester'], form.cleaned_data['description'], form.cleaned_data['due_date'], form.cleaned_data['submissions_permitted'], form.cleaned_data['marks'], form.cleaned_data['test_input'], form.cleaned_data['expected_output'], form.cleaned_data['language'])
			if uform.is_valid():
				return render(request, 'downloadassessment.html')
	else:
		return HttpResponseRedirect('/error403/')
		
	c = {
		"form": form,
		"aform": aform,
		"uform": uform,
	}
	return render(request, 'editassessment.html', c)

@login_required
@user_passes_test(check_admin, login_url='/home/', redirect_field_name=None)
def unitToEdit(request):
	r = loader.get_template('editunit.html')
	form = AssessmentSettingsForm()
	
	if request.method == 'POST' and 'edit_button' in request.POST:
		form = UnitSettingsForm(request.POST)	
	elif request.method == 'POST' and 'remove_button' in request.POST: 
		form = UnitSettingsForm(request.POST)
		if form.is_valid():
			resp = services.delete_unit(form.cleaned_data['unit_id'], form.cleaned_data['year'], form.cleaned_data['semester'])
			return HttpResponseRedirect("/unitsettings/")
		else:
			return HttpResponseRedirect('/error403/')
	else:
		return HttpResponseRedirect('/error403/')
	
	c = {
		"form": form,
	}
	return render(request, 'editunit.html', c)
	
@login_required
@user_passes_test(check_admin, login_url='/home/', redirect_field_name=None)
def submissionToEdit(request):
	r = loader.get_template('editsubmission.html')
	form = SubmissionInfo()
	oform = OutputInfo()
	uform = UnitSettingsForm()
	aform = AssessmentInfo()	
	
	if request.method == 'POST' and 'edit_button' in request.POST:
		form = SubmissionInfo(request.POST)
		oform = OutputInfo(request.POST)
		uform = UnitSettingsForm(request.POST)
		aform = AssessmentInfo(request.POST)
	elif request.method == 'POST' and 'remove_button' in request.POST: 
		form = SubmissionInfo(request.POST)
		oform = OutputInfo(request.POST)
		uform = UnitSettingsForm(request.POST)
		if form.is_valid():
			resp = services.delete_submission(form.cleaned_data['submission_id'])
			aform = AssessmentInfo(request.POST)
			uform = UnitSettingsForm(request.POST)
			if aform.is_valid():
				submissions_list = services.get_submissions(aform.cleaned_data['assessment_id'])
				c = {
					"submissions_list": submissions_list,
					"form": uform,
					"aform": aform,
				}
				return render(request, 'submissionsettings.html', c)
		else:
			return HttpResponseRedirect('/error403/')
	else:
		return HttpResponseRedirect('/error403/')
		
	c = {
		"form": form,
		"oform": oform,
		"uform": uform,
		"aform": aform,
	}
	return render(request, 'editsubmission.html', c)
	
@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def studentToEdit(request):
	r = loader.get_template('editstudent.html')
	form = StudentInfo()
	
	if request.method == 'POST' and 'edit_button' in request.POST:
		form = StudentInfo(request.POST)
	elif request.method == 'POST' and 'remove_button' in request.POST: 
		form = StudentInfo(request.POST)
		if form.is_valid():
			resp = services.delete_student(form.cleaned_data['student_id'])
			return HttpResponseRedirect("/students/")
		else:
			return HttpResponseRedirect('/error403/')
	else:
		return HttpResponseRedirect('/error403/')
		
	c = {
		"form": form,
	}
	return render(request, 'editstudent.html', c)

# ========== RE-EVALUATING FORMS LINKS ==========	
@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def editassessment(request):
	r = loader.get_template('editassessment.html')
	
	if request.method == 'POST' and 'submit_button' in request.POST:
		form = AssessmentSettingsForm(request.POST)
		aform = AssessmentInfo(request.POST)
		if form.is_valid() and aform.is_valid():
			services.update_assessment(aform.cleaned_data['assessment_id'], form.cleaned_data['description'], form.cleaned_data['due_date'], form.cleaned_data['submissions_permitted'], form.cleaned_data['marks'], form.cleaned_data['test_input'], form.cleaned_data['expected_output'], form.cleaned_data['language'])
			uform = UnitSettingsForm(request.POST)
			if uform.is_valid():
				assessments_list = services.get_assessments(uform.cleaned_data['unit_id'], uform.cleaned_data['year'], uform.cleaned_data['semester'])
				c = {
					"assessments_list": assessments_list,
					"form": uform,
				}
				return render(request, 'assessmentsettings.html', c)
	elif request.method == 'POST' and 'cancel_button' in request.POST:
		form = UnitSettingsForm(request.POST)
		if form.is_valid():
			assessments_list = services.get_assessments(form.cleaned_data['unit_id'], form.cleaned_data['year'], form.cleaned_data['semester'])
			c = {
				"assessments_list": assessments_list,
				"form": form,
			}
			return render(request, 'assessmentsettings.html', c)
	else:
		return HttpResponseRedirect('/error403/')
		
	return render(request, 'editassessment.html')

@login_required	
@user_passes_test(check_admin, login_url='/home/', redirect_field_name=None)
def editunit(request):
	r = loader.get_template('editunit.html')
	
	if request.method == 'POST' and 'submit_button' in request.POST:
		form = UnitSettingsForm(request.POST)
		if form.is_valid():
			services.update_unit(form.cleaned_data['unit_id'], form.cleaned_data['year'], form.cleaned_data['semester'], form.cleaned_data['unit_name'])
			units_list = services.get_units()
			
			c = {
				"units_list": units_list,
			}
			
			return render(request, 'unitsettings.html', c)
	elif request.method == 'POST' and 'cancel_button' in request.POST:
		units_list = services.get_units()
		
		c = {
			"units_list": units_list,
		}
		return render(request, 'unitsettings.html', c)
	else:
		return HttpResponseRedirect('/error403/')
		
	return render(request, 'editunit.html')
	
@login_required
@user_passes_test(check_student, login_url='/home/', redirect_field_name=None)
def editsubmission(request):
	r = loader.get_template('editsubmission.html')
	
	if request.method == 'POST' and 'submit_button' in request.POST:
		form = SubmissionInfo(request.POST)
		if form.is_valid():
			services.update_submission(form.cleaned_data['submission_id'], form.cleaned_data['submission'], form.cleaned_data['mark'], form.cleaned_data['feedback'])
			aform = AssessmentInfo(request.POST)
			uform = UnitSettingsForm(request.POST)
			if aform.is_valid():
				submissions_list = services.get_submissions(aform.cleaned_data['assessment_id'])
				c = {
					"submissions_list": submissions_list,
					"form": uform,
					"aform": aform,
				}
				return render(request, 'submissionsettings.html', c)
	elif request.method == 'POST' and 'cancel_button' in request.POST:
		form = SubmissionInfo(request.POST)
		if form.is_valid():
			submissions_list = services.get_submissions(form.cleaned_data['assessment_id'])
			aform = AssessmentInfo(request.POST)
			uform = UnitSettingsForm(request.POST)
			if aform.is_valid():
				submissions_list = services.get_submissions(aform.cleaned_data['assessment_id'])
				c = {
					"submissions_list": submissions_list,
					"form": uform,
					"aform": aform,
				}
				return render(request, 'submissionsettings.html', c)
	else:
		return HttpResponseRedirect('/error403/')
		
	return render(request, 'editsubmission.html')
	
@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def editstudent(request):
	r = loader.get_template('editstudent.html')
	
	if request.method == 'POST':
		form = StudentInfo(request.POST)
		if form.is_valid():
			services.update_student(form.cleaned_data['student_id'], form.cleaned_data['name'])
			return HttpResponseRedirect("/students/")
	else:
		return HttpResponseRedirect('/error403/')
		
	return render(request, 'editstudent.html')

@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def viewenrolments(request):
	r = loader.get_template('viewenrolments.html')	
	enrolments_list = services.get_enrolments()
	students_list = services.get_students()
	new_student = []
	
	if request.method == 'POST' and 'view_button' in request.POST:
		form = UnitSettingsForm(request.POST)
		
		if form.is_valid():
			for students in students_list:
				for enrolments in enrolments_list:
					if (students['student_id'] == enrolments['student_id']) and (form.cleaned_data['unit_id'] == enrolments['unit_id']) and (int(form.cleaned_data['semester']) == int(enrolments['semester'])) and (int(form.cleaned_data['year']) == int(enrolments['year'])):
						new_student.append(students)
	elif request.method == 'POST' and 'enrol_button' in request.POST: 
		form = UnitSettingsForm(request.POST)
		if form.is_valid():
			c = {
				'form': form,
				'enrolments_list': enrolments_list,
				'students_list': students_list,
			}
			return render(request, 'setenrolment.html', c)
		else:
			return HttpResponseRedirect('/error403/')
	elif request.method == 'POST' and 'remove_button' in request.POST:
		eform = EnrolmentInfo(request.POST)
		form = UnitSettingsForm(request.POST)
		
		if eform.is_valid() and form.is_valid():
			params = {}
			params['unit_id'] = eform.cleaned_data['unit_id']
			params['year'] = eform.cleaned_data['year'] 
			params['semester'] = eform.cleaned_data['semester'] 
			params['student_id'] = eform.cleaned_data['student_id'] 
			enrolment = []
			enrolment.append(params)
			resp = services.delete_enrolment(enrolment)
			enrolments_list = services.get_enrolments()
			new_student = []
			for students in students_list:
				for enrolments in enrolments_list:
					if (students['student_id'] == enrolments['student_id']) and (form.cleaned_data['unit_id'] == enrolments['unit_id']) and (int(form.cleaned_data['semester']) == int(enrolments['semester'])) and (int(form.cleaned_data['year']) == int(enrolments['year'])):
						new_student.append(students)
	elif request.method == 'POST' and 'search_button' in request.POST:
		sform = Search(request.POST)
		form = UnitSettingsForm(request.POST)
		if(sform.is_valid() and form.is_valid()):
			students_list = services.search_students(sform.cleaned_data['filteroption'], sform.cleaned_data['search'])
			for students in students_list:
				for enrolments in enrolments_list:
					if (students['student_id'] == enrolments['student_id']) and (form.cleaned_data['unit_id'] == enrolments['unit_id']) and (int(form.cleaned_data['semester']) == int(enrolments['semester'])) and (int(form.cleaned_data['year']) == int(enrolments['year'])):
						new_student.append(students)
		else:
			if form.is_valid():
				for students in students_list:
					for enrolments in enrolments_list:
						if (students['student_id'] == enrolments['student_id']) and (form.cleaned_data['unit_id'] == enrolments['unit_id']) and (int(form.cleaned_data['semester']) == int(enrolments['semester'])) and (int(form.cleaned_data['year']) == int(enrolments['year'])):
							new_student.append(students)
	else:
		form = UnitSettingsForm()
	
	c = {
		'form': form,
		'students_list': new_student,
	}
	return render(request, 'viewenrolments.html', c)

@login_required	
def viewassessmentsubmissions(request):
	r = loader.get_template('viewassessmentsubmissions.html')
	
	if request.method == 'POST' and 'search_button' in request.POST:
		sform = Search(request.POST)
		aform = AssessmentInfo(request.POST)
		form = UnitSettingsForm(request.POST)
		if sform.is_valid() and aform.is_valid():
			submissions_list = services.search_submissions(sform.cleaned_data['filteroption'], sform.cleaned_data['search'])
		elif aform.is_valid():
			submissions_list = services.search_submissions(sform.cleaned_data['filteroption'], '', aform.cleaned_data['assessment_id'])
		else:
			return HttpResponseRedirect('/error404/')
	else:
		return HttpResponseRedirect('/error404/')
		
	c = {
		"submissions_list": submissions_list,
		"form": form,
		"aform": aform,
	}
	return render(request, 'viewassessmentsubmissions.html', c)
	
@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def downloadassessment(request):
	r = loader.get_template('downloadassessment.html')	
	return render(request, 'downloadassessment.html')

@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def download_csv_submissions(request):
	filename = 'submission_CSV.txt'
	with open(filename, 'rb') as fh:
		response = HttpResponse(fh.read(), content_type='text/plain')
		response['Content-Disposition'] = 'attachment; filename=' + filename
		response['Content-Type'] = 'text/plain; charset=utf-16'
		return response
	
@login_required	
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def download_xml_assessments(request):
	filename = 'assessment_XML.xml'
	with open(filename, 'rb') as fh:
		response = HttpResponse(fh.read(), content_type='text/xml')
		response['Content-Disposition'] = 'attachment; filename=' + filename
		response['Content-Type'] = 'text/xml; charset=utf-16'
		return response
	
@login_required	
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def upload_csv_students(request):
	if(request.method == 'POST' and request.FILES['uploadfile']):
		uploadfile = request.FILES['uploadfile']
		services.processCSVStudents(uploadfile)
		
		students_list = services.get_students()	
		c = {
			'students_list': students_list,
		}
		return render(request, 'students.html', c)
	else:
		return HttpResponseRedirect('/error403/')
		
	return render(request, 'addstudent.html')
	
@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def upload_csv_units(request):
	if(request.method == 'POST' and request.FILES['uploadfile']):
		uploadfile = request.FILES['uploadfile']
		services.processCSVUnits(uploadfile)
		
		units_list = services.get_units()
		c = {
			"units_list": units_list,
		}
		return render(request, 'unitsettings.html', c)	
	else:
		return HttpResponseRedirect('/error403/')
		
	return render(request, 'addunit.html')
	
@login_required
@user_passes_test(check_staff, login_url='/home/', redirect_field_name=None)
def upload_xml_assessments(request):
	if(request.method == 'POST' and request.FILES['uploadfile']):
		uploadfile = request.FILES['uploadfile']
		services.processXMLAssessments(uploadfile)
		
		uform = UnitSettingsForm(request.POST)
		if uform.is_valid():
			assessments_list = services.get_assessments(uform.cleaned_data['unit_id'], uform.cleaned_data['year'], uform.cleaned_data['semester'])
			c = {
				"assessments_list": assessments_list,
				"form": uform,
			}
			return render(request, 'assessmentsettings.html', c)
		else:
			return HttpResponseRedirect('/error403/')
	else:
		return HttpResponseRedirect('/error403/')
		
	return render(request, 'addassessment.html')
	
def error400(request):
	r = loader.get_template('error/400.html')
	return render(request, 'error/400.html')
	
def error403(request):
	r = loader.get_template('error/403.html')
	return render(request, 'error/403.html')
	
def error404(request):
	r = loader.get_template('error/404.html')
	return render(request, 'error/404.html')
	
def error500(request):
	r = loader.get_template('error/500.html')
	return render(request, 'error/500.html')
	
def error503(request):
	r = loader.get_template('error/503.html')
	return render(request, 'error/503.html')