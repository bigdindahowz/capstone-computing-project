#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.template import loader
from django import forms
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.db import models
from .models import Profile

import time
from app import signature_creator

import requests

class SubmissionForm(forms.Form):
	assessment_id = forms.CharField(label='unit_id', max_length=500, required=True)
	answer = forms.CharField(label='answer', max_length=1000, required=True)
	
class AssessmentSettingsForm(forms.Form):
	unit_id = forms.CharField(label='unit_id', max_length=500, required=True)
	due_date = forms.CharField(label='due_date', required=True)
	submissions_permitted = forms.IntegerField(label='submissions_permitted', required=True)
	marks = forms.IntegerField(label='marks', required=True)
	year = forms.IntegerField(label='student_id', required=True)
	semester = forms.IntegerField(label='semester', required=True)
	test_input = forms.CharField(label='test_input', required=True, empty_value='')
	expected_output = forms.CharField(label='expected_output', required=True)
	language = forms.CharField(label='language', required=True)
	description = forms.CharField(label='description', max_length=1000, required=True)
	
class UnitSettingsForm(forms.Form):
	unit_id = forms.CharField(label='unit_id', max_length=500, required=True)
	unit_name = forms.CharField(label='unit_name', max_length=500, required=True)
	year = forms.CharField(label='year', required=True)
	semester = forms.CharField(label='semester', required=True)
	
class AssessmentInfo(forms.Form):
	assessment_id = forms.CharField(label='assessment_id', max_length=500, required=True)
	unit_id = forms.CharField(label='assessment_id', max_length=500, required=True)
	semester = forms.IntegerField(label='semester', required=True)
	description = forms.CharField(label='description', max_length=1000, required=True)
	language = forms.CharField(label='language', required=True)
	marks = forms.IntegerField(label='marks', required=True)
	due_date = forms.CharField(label='due_date', required=True)
	
class SubmissionInfo(forms.Form):
	submission_id = forms.IntegerField(label='submission_id', required=True)
	student_id = forms.IntegerField(label='student_id', required=True)
	assessment_id = forms.IntegerField(label='assessment_id', required=True)
	mark = forms.IntegerField(label='mark')
	feedback = forms.CharField(label='feedback', max_length=1000, empty_value=None)
	submission = forms.CharField(label='submission', max_length=1000, empty_value='')
	
class OutputInfo(forms.Form):
	running_output = forms.CharField(label='running_output', max_length=1000, empty_value='')
	memory_usage = forms.IntegerField(label='memory_usage')
	
class StudentInfo(forms.Form):
	student_id = forms.IntegerField(label='student_id', required=True)
	name = forms.CharField(label='name', max_length=150, empty_value='')
	
class EnrolmentInfo(forms.Form):
	unit_id = forms.CharField(label='unit_id', max_length=500, required=True)
	year = forms.IntegerField(label='year', required=True)
	semester = forms.IntegerField(label='semester', required=True)
	student_id = forms.IntegerField(label='student_id', required=True)
	
class Search(forms.Form):
	filteroption = forms.CharField(label='filteroption', max_length=500, required=True)
	search = forms.CharField(label='search', max_length=500, required=True)

class LoginForm(forms.Form):
	username = forms.CharField(label= "Curtin ID", max_length=8)
	password = forms.CharField(widget=forms.PasswordInput)

class RegistrationForm(UserCreationForm):
	username = forms.CharField(label="Curtin ID", max_length=8)
	ACC_TYPE = (
		('S', 'Student'),
		('L', 'Staff Member'),
		('A', 'Unit Admin')
	)
	status = forms.ChoiceField(label='status', choices=ACC_TYPE, widget=forms.RadioSelect)
	first_name = forms.CharField(label='first_name', max_length=100, required=True)
	last_name = forms.CharField(label='last_name', max_length=100, required=True)
	email = forms.EmailField(label='email', max_length=200, required=True)
#	password1 = forms.CharField(label='password1', widget=forms.PasswordInput, label="Password")
#	password2 = forms.CharField(label='password2', widget=forms.PasswordInput, label="Password (again)")
	field_order = ["username", "status", "first_name", "last_name", "email", "password1", "password2"]

	class Meta:
		model = User
		fields = ["username", "status", "first_name", "last_name", "email", "password1", "password2"]

	def clean_username(self):
		username = self.cleaned_data['username']

		if is_valid_student_id(username) == False and is_valid_staff_id(username) == False:
			raise forms.ValidationError("Please enter a valid Curtin ID")
		else:
			try:
				User.objects.get(username = username)
				raise forms.ValidationError("An account with this username already exists")
			except User.DoesNotExist:
				return username

	def clean_email(self):
		email = self.cleaned_data['email']

		if validateEmail(email) == False:
			raise forms.ValidationError("Please Enter a Valid Email Address")

		else:
			domain = email.split('@')[1]
			if domain == 'student.curtin.edu.au' or domain == 'staff.curtin.edu.au':
				return email
			else:
				raise forms.ValidationError("Please use your Curtin email Address")

	def clean(self):
		form_data = self.cleaned_data
		if form_data['password1'] != form_data['password2']:
			self._errors["password1"] = ["Passwords do not match."]
			del form_data['password1']
		else:
			return form_data

def is_valid_student_id(username):
    if len(username) == 8 and username.isdigit():
        return True
    else:
        return False

def is_valid_staff_id(username):
    if len(username) == 7:
        for i in range(0, len(username) - 1):
            if username[i].isdigit() == False:
                return False
        if username[6].isdigit() == True:
            return False
        return True
    else:
        return False

def validateEmail( email ):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError
    try:
        validate_email( email )
        return True
    except ValidationError:
        return False


class ThemeForm(forms.ModelForm):
	THEME_TYPE = (
		('L', 'Black writing on white background'),
		('D', 'White writing on black background'),
	)
	theme = forms.ChoiceField(label='theme', choices=THEME_TYPE, widget=forms.RadioSelect)
#	TEXT_SIZE = (
#		('14', 'Small text'),
#		('22', 'Medium text'),
#		('30', 'Large text'),
#	)
#	text = forms.ChoiceField(choices=TEXT_SIZE, widget=forms.RadioSelect)

	class Meta:
		model = Profile
		fields = ["theme"] 
