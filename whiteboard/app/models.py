#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.

class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	THEME_TYPE = (
		('L', 'Light'),
		('D', 'Dark'),
	)
	theme = models.CharField(max_length=1, choices=THEME_TYPE, default='L')
	TEXT_SIZE = (
		('14', 'Small'),
		('22', 'Medium'),
		('30', 'Large'),
	)
	text = models.CharField(max_length=2, choices=TEXT_SIZE, default='22')

	def get_theme(self):
		return self.theme

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
	
