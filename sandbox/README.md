# Sandbox

The Sandbox is a subcomponent of the Whiteboard system which is used to retreive, mark and update submissions from the API. The following sections discuss the various requirements and steps for setting up and configuring the Sandbox. Please note, previous experience with Ubuntu is required to configure the Sandbox.


## Requirements

The Sandbox has been tested with the following packages. Different packages may work but the following are what is recommended.

 - Ubuntu Server 16.04
 - Python 3
 - Docker
## Recommended Server Specifications
 - Modern Quad Core Processor (as of 2018)
 - 4GB RAM

## Installation

I will assume that you have already installed a basic installation of Ubuntu Server 16.04. For instructions on how to do so, please look at the following article: https://www.howtoforge.com/tutorial/ubuntu-16.04-xenial-xerus-minimal-server/

**Step 1:** We need to install Python3 and Docker.
Run the following commands as root:

    apt-get update
    apt-get install python3.6
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    apt-get update
    apt-cache policy docker-ce
    apt-get install -y docker-ce

**Step 2:** Next we will need to build the Docker Images.
First, pull the latest master commit from the repository down to the server and change directories to the sandbox/Images directory. From there run the following commands:

    cd C
    docker build -t whiteboard_sandbox_c .
    cd ..
    cd C++
    docker build -t whiteboard_sandbox_cpp .
    cd ..
    cd Java
    docker build -t whiteboard_sandbox_java .
    cd ..
    cd MariaDB
    docker build -t whiteboard_sandbox_mysql .
    cd ..
    cd Python
    docker build -t whiteboard_sandbox_python .

**Step 3:** Finally, we need to ensure that the sandbox directory pulled down from the repository is stored in the root directory and that it is executable. This can be done by changing directory to the root of the pulled down directory and running the following commands:

    mv sandbox /root/
    chmod +x /root/sandbox/CCPSandboxQueue.py

#To run use ./CCPSandboxQueue.py
    

## Optional Configuration
The Sandbox has a configuration file stored at sandbox/configuration.ini. This file has been configured with default values and contains the following fields:

 - MaximumThreads --> Specifies the maximum amount of threads the Sandbox will use. As a rule of thumb, maximum threads should equal the amount of processor cores
 - JavaDockerImageName --> Specifies the name of the Java docker image
 - SQLDockerImageName --> Specifies the name of the SQL docker image
 - CDockerImageName --> Specifies the name of the C docker image
 - PythonDockerImageName --> Specifies the name of the Python docker image
 - CPPDockerImageName --> Specifies the name of the C++ docker image
 - DockerMaxRunTime --> Specifies the amount of time a docker instance can run before being destroyed
 - DockerMaxMemoryUsage --> Specifies the maximum amount of memory that a docker instance is allowed to consume
 - APIURL --> Specifies the link to the API
 - APIKey --> Specifies the API key
 - APISecret --> Specifies the API secret

The configuration file may be tweaked as per requirements.