#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

import subprocess
import os
import time
import threading
import configparser

#Constructs configuration object and reads contents from configuration.ini
Configuration = configparser.ConfigParser()
Configuration.read('configuration.ini')

#Constant strings for dockers
CONST_JAVA_IMAGE = Configuration['Sandbox']['JavaDockerImageName']
CONST_SQL_IMAGE = Configuration['Sandbox']['SQLDockerImageName']
CONST_C_IMAGE = Configuration['Sandbox']['CDockerImageName']
CONST_PYTHON_IMAGE = Configuration['Sandbox']['PythonDockerImageName']
CONST_CPP_IMAGE = Configuration['Sandbox']['CPPDockerImageName']
CONST_DOCKER_MAX_RUN_TIME = int(Configuration['Sandbox']['DockerMaxRunTime'])
CONST_DOCKER_MAX_MEMORYUSAGE = Configuration['Sandbox']['DockerMaxMemoryUsage']

#Function which monitors docker memory usage, runs in a separate thread to the docker
def monitorDockerMemory(containerID, sandbox):
	#Initialises maxMemoryUsage to 0.
	maxMemoryUsage = 0

	#Prints for verbosity.
	print('Monitoring dockers memory usage: ' + containerID)
	
	try:
		#While the sandbox has not finished executing.
		while sandbox.EXECUTION == False:
			#Queries the current memory usage of the docker instance
			currentMem = subprocess.check_output(['docker', 'stats', containerID, '--no-stream=true', '--format', '{{.MemUsage}}'])
			currentMem = currentMem.decode('utf-8').strip()

			tokens = currentMem.split( )

			#Returns the current memory usage of the docker in bytes
			if (tokens[0][-3:] == 'KiB'):
				currentMem = float(tokens[0][:-3])*1000
			elif (tokens[0][-3:] == 'MiB'):
				currentMem = float(tokens[0][:-3])*1000*1000
			elif (tokens[0][-3:] == 'GiB'):
				currentMem = float(tokens[0][:-3])*1000*1000*1000

			#Typecasts the memory usage to an integer
			currentMem = int(currentMem)

			#Updates the maximum memory value if the queried memory value is greater than previous queries.
			if (currentMem > maxMemoryUsage):
				maxMemoryUsage = currentMem

	#Catches error if sandbox timeout occurs and docker suddenly no longer exists
	except subprocess.CalledProcessError:
		pass

	#Prints for verbosity.
	print('Maximum memory usage was: ' + str(maxMemoryUsage))
	
	#Adds the memory usage back to the object
	sandbox.submission.memoryUsage = maxMemoryUsage

#Function which limits docker running time. Runs in a separate thread to the docker instance
def monitorDocker(containerID, sandbox):
	#Prints for verbosity
	print('Monitoring docker:' + containerID)
	
	#Sleeps until the dockers maximum run time is exceeded.
	time.sleep(CONST_DOCKER_MAX_RUN_TIME)
	
	#If the docker has not finished executing
	if not sandbox.EXECUTION:
		#Prints for verbosity.
		print('A timeout has occured.')
		
		#Sets timeout to true and destroys the docker.
		sandbox.TIMEOUT = True
		subprocess.call('docker rm -f ' + containerID, shell=True, stdout=open(os.devnull, 'wb'))

#Function which returns the current time.
def getCurrentTime():
	return int(round(time.time()*1000))

#Class for the Sandbox. Has subclasses extending from the Sandbox class.
class Sandbox:
	#Class variables
	submission = None
	TIMEOUT = False
	EXECUTION = False
	memMonitor = None
	
	#Variable for docker container ID.
	containerID = ""

	#Constructor
	def __init__(self, inSubmission):
		self.submission = inSubmission
		
	#Destructor (removes docker once object goes out of scope)
	def __del__(self):
		if (self.containerID != "" and not self.TIMEOUT):
			subprocess.call('docker rm -f ' + self.containerID, shell=True, stdout=open(os.devnull, 'wb'))

	#Parent method that should be overriden by child class.	
	def createDocker(self):
		pass

	#Method for creating the docker.
	def startDocker(self, containerID):
		#If the docker exists.
		if (self.containerID != ""):
			response = subprocess.check_output(['docker', 'start', containerID])

			#Prints line if docker started correctly, else throws error.
			if (response != ''):
				print ('Docker started successfully.')

				#Start monitoring memory usage
				self.memMonitor = threading.Thread(target=monitorDockerMemory, args=(self.containerID, self))
				self.memMonitor.start()
			else:
				#Error handling.
				raise ValueError('Starting docker did not return an ID.')

	#Method for passing a code snippet to a docker.
	def passCodeSnippet(self):
		#Splits the input into individual lines
		lines = self.submission.submission.splitlines()

		#Creates file inside the docker
		subprocess.call('docker exec ' + self.containerID + ' touch /root/snippet', shell=True)
		
		#For each line, escape double quotes and pass them to the docker through echo.
		#Redirect the output to the /root/snippet file inside the docker
		for line in lines:
			line = line.replace('"',r'\"')
			line = line.replace("'",r"'\''")
			execString = 'docker exec ' + self.containerID + ' bash -c "echo \'' + line + '\' >> /root/snippet"'
			subprocess.call(execString, shell=True)

	#Parent method that should be overridden by child class.
	def compileExecCode(self):
		pass

	#Method for creating and starting the docker, passing the code snippet and executing the code.
	#Returns the output from code execution
	def runSimulation(self):
		self.containerID = self.createDocker()
		self.startDocker(self.containerID)
		print ('Passing code snippet to docker.')
		self.passCodeSnippet()
		print ('Running code simulation.')

		#Start monitor for run time
		monitor = threading.Thread(target=monitorDocker, args=(self.containerID, self))
		monitor.start()

		#Runs the simulation and gets the output.
		snippetOutput = self.compileExecCode()
		
		#Sets execution to true (complete).
		#global EXECUTION
		self.EXECUTION = True
		

		#Spin until the memory monitor has finished.
		while self.memMonitor.isAlive():
			pass

		#If a timeout has not occured, return the output, else return timeout message
		if (not self.TIMEOUT):
			return snippetOutput
		else:
			return ('execution time exceeded threshold')

#Child class for the Java sandbox.
class Java(Sandbox):
	#Overrides the createDocker method. Creates a Java docker.
	def createDocker(self):
		containerID = subprocess.check_output(['docker', 'create', '-m', CONST_DOCKER_MAX_MEMORYUSAGE, '-it', CONST_JAVA_IMAGE])
		containerID = containerID.decode('utf-8').strip()
		self.containerID = containerID
		if (containerID != ''):
			print('Docker created successfully.')
		else:
			raise ValueError('Creating docker did not return an ID.')
		return containerID

	#Compiles and executes the passed code by running /root/procJava.py inside the docker.
	def compileExecCode(self):
		try:
			#Sets list of commands to run to execute the process java script stored inside the Java docker instance.
			execList = ['docker', 'exec', self.containerID, 'python3', '/root/procJava.py']

			#Adds arguments if present to the list of commands (deliminated by space)
			execList = execList + self.submission.testInput.split( )

			#Runs the above execList
			snippetOutput = subprocess.check_output(execList)
			snippetOutput = snippetOutput.decode('utf-8').strip()

			#Retreives the runtime from the docker which is created by the process Java script.
			runtime = subprocess.check_output(['docker', 'exec', self.containerID, 'cat', '/root/runtime'])
			runtime = runtime.decode('utf-8').strip()

			#Adds the runtime to the object.
			self.submission.runtime = runtime

			#Prints the execution time for verbosity.
			print('Execution time: ' + str(self.submission.runtime))

		#Catches runtime errors
		except subprocess.CalledProcessError:
			return 'runtime error'
		return snippetOutput


#Child class for the SQL sandbox.
class SQL(Sandbox):
	#Overrides the createDocker method. Creates a SQL docker.
	def createDocker(self):
		#Creates SQL docker.
		containerID = subprocess.check_output(['docker', 'create', '-m', CONST_DOCKER_MAX_MEMORYUSAGE, '-it', CONST_SQL_IMAGE])
		containerID = containerID.decode('utf-8').strip()
		self.containerID = containerID
		if (containerID != ''):
			print('Docker created successfully.')
		else:
			raise ValueError('Creating docker did not return an ID.')
		return containerID

	#Function which returns the table data
	def retTableData(self):
		return self.submission.testInput

	#Overrides the passCodeSnippet method for the SQL class.
	def passCodeSnippet(self):
		#Retreives the data used to create the table and the query from submission.
		#Always uses the database sample.
		#This is so the person setting up the assesment doesn't have to worry about the database.
		tableData = self.retTableData()
		query = self.submission.submission

		lines = tableData.splitlines()
		
		#Adds the create sample db and use sample db SQL queries to the top of the /root/tableData file.
		lines.insert(0, 'USE sample;')
		lines.insert(0, 'CREATE DATABASE sample;')

		#Touches the /root/tableData and /root/query files.
		subprocess.call('docker exec ' + self.containerID + ' touch /root/tableData', shell=True)
		subprocess.call('docker exec ' + self.containerID + ' touch /root/query', shell=True)
		
		#passes the table data and the query to the docker.
		for line in lines:
			line = line.replace('"',r'\"')
			line = line.replace("'",r"'\''")
			execString = 'docker exec ' + self.containerID + ' bash -c "echo \'' + line + '\' >> /root/tableData"'
			subprocess.call(execString, shell=True)

		lines = query.splitlines()
		lines.insert(0, 'USE sample;')
		for line in lines:
			line = line.replace('"',r'\"')
			line = line.replace("'",r"'\''")
			execString = 'docker exec ' + self.containerID + ' bash -c "echo \'' + line + '\' >> /root/query"'
			subprocess.call(execString, shell=True)
		
	#Overrides the compileExecCode method for the SQL class.
	def compileExecCode(self):
		#Calls the /root/setupTables.sh script which creates the database and table/s.
		#Returns the result of the query.
		subprocess.check_output(['docker', 'exec', self.containerID, 'sh', '/root/setupTables.sh'])

		#Records time execution started
		startTime = getCurrentTime()

		snippetOutput = subprocess.check_output(['docker', 'exec', self.containerID, 'sh', '/root/runQuery.sh'])

		#Calculates the execution time and adds it to the object.
		self.submission.runtime = getCurrentTime() - startTime
		print('Execution time: ' + str(self.submission.runtime))

		snippetOutput = snippetOutput.decode('utf-8').strip()
		return snippetOutput

class C(Sandbox):
	#Overrides the parent createDocker method. Creates a C docker.
	def createDocker(self):
		#Creates a C docker.
		containerID = subprocess.check_output(['docker', 'create', '-m', CONST_DOCKER_MAX_MEMORYUSAGE, '-it', CONST_C_IMAGE])
		containerID = containerID.decode('utf-8').strip()
		self.containerID = containerID
		#Throws an exception if the docker did not create successfully.
		if (containerID != ''):
			print('Docker created successfully.')
		else:
			raise ValueError('Creating docker did not return an ID.')
		return containerID

	#Method which overrides the parent. Compiles and runs the c code. Checks for compile errors and memory erros.
	def compileExecCode(self):
		#Moves the snippet to snippet.c
		subprocess.call('docker exec ' + self.containerID + ' mv /root/snippet /root/snippet.c', shell=True)
		
		#Return compilation error if a compile error occurs.
		try:
			compileOutput = subprocess.check_output(['docker', 'exec', self.containerID, 'gcc', '-o', '/root/compiled', '/root/snippet.c'])
			compileOutput = compileOutput.decode('utf-8').strip()
		except subprocess.CalledProcessError:
			return 'compilation error'

		#Return memory leaks are present if valgrind reports a non 0 exit code.
		try:
			leakOutput = subprocess.check_output(['docker', 'exec', self.containerID, 'sh', '/root/procC.sh'])
			leakOutput = leakOutput.decode('utf-8').strip()
		except subprocess.CalledProcessError:
			return ('memory leaks are present')
			
		#If there are no memory leaks or compilation errors, return the programs output.
		execList = ['docker', 'exec', self.containerID, './compiled']
		execList = execList + self.submission.testInput.split( )

		#Records time execution started
		startTime = getCurrentTime()

		snippetOutput = subprocess.check_output(execList)

		#Record execution time
		self.submission.runtime = getCurrentTime() - startTime
		print('Execution time: ' + str(self.submission.runtime))

		snippetOutput = snippetOutput.decode('utf-8').strip()
		return snippetOutput

class CPP(Sandbox):
	#Overrides the parent createDocker method. Creates a C++ docker.
	def createDocker(self):
		#Creates a C++ docker.
		containerID = subprocess.check_output(['docker', 'create', '-m', CONST_DOCKER_MAX_MEMORYUSAGE, '-it', CONST_CPP_IMAGE])
		containerID = containerID.decode('utf-8').strip()
		self.containerID = containerID
		#Throws an exception if the docker did not create successfully.
		if (containerID != ''):
			print('Docker created successfully.')
		else:
			raise ValueError('Creating docker did not return an ID.')
		return containerID

	#Method which overrides the parent. Compiles and runs the c code. Checks for compile errors and memory erros.
	def compileExecCode(self):
		#Moves the snippet to snippet.cpp
		subprocess.call('docker exec ' + self.containerID + ' mv /root/snippet /root/snippet.cpp', shell=True)
		
		#Return compilation error if a compile error occurs.
		try:
			compileOutput = subprocess.check_output(['docker', 'exec', self.containerID, 'g++', '-o', '/root/compiled', '/root/snippet.cpp'])
			compileOutput = compileOutput.decode('utf-8').strip()
		except subprocess.CalledProcessError:
			return 'compilation error'

		#Return memory leaks are present if valgrind reports a non 0 exit code.
		try:
			leakOutput = subprocess.check_output(['docker', 'exec', self.containerID, 'sh', '/root/procCPP.sh'])
			leakOutput = leakOutput.decode('utf-8').strip()
		except subprocess.CalledProcessError as e:
			#print(e.output)
			return ('memory leaks are present')
			
		#If there are no memory leaks or compilation errors, return the programs output.
		execList = ['docker', 'exec', self.containerID, './compiled']
		execList = execList + self.submission.testInput.split( )

		#Records time execution started
		startTime = getCurrentTime()

		snippetOutput = subprocess.check_output(execList)

		#Record execution time
		self.submission.runtime = getCurrentTime() - startTime
		print('Execution time: ' + str(self.submission.runtime))

		snippetOutput = snippetOutput.decode('utf-8').strip()
		return snippetOutput

class Python2(Sandbox):
	#Overrides the parent createDocker method. Creates a Python docker.
	def createDocker(self):
		#Creates a Python docker.
		containerID = subprocess.check_output(['docker', 'create', '-m', CONST_DOCKER_MAX_MEMORYUSAGE, '-it', CONST_PYTHON_IMAGE])
		containerID = containerID.decode('utf-8').strip()
		self.containerID = containerID
		if (containerID != ''):
			print('Docker created successfully.')
		else:
			raise ValueError('Creating docker did not return an ID.')
		return containerID

	def compileExecCode(self):
		try:
			#Executes the snippet inside the docker.
			execList = ['docker', 'exec', self.containerID, 'python', '/root/snippet']
			execList = execList + self.submission.testInput.split( )

			#Records time execution started
			startTime = getCurrentTime()

			snippetOutput = subprocess.check_output(execList)

			#Record execution time
			self.submission.runtime = getCurrentTime() - startTime
			print('Execution time: ' + str(self.submission.runtime))

			snippetOutput = snippetOutput.decode('utf-8').strip()
		#Handles runtime errors
		except subprocess.CalledProcessError:
			return 'runtime error' #No compilation errors with python as it is an interpreted language.
		return snippetOutput

class Python3(Sandbox):
	#Overrides the parent createDocker method. Creates a Python docker.
	def createDocker(self):
		#Creates a Python docker.
		containerID = subprocess.check_output(['docker', 'create', '-m', CONST_DOCKER_MAX_MEMORYUSAGE, '-it', CONST_PYTHON_IMAGE])
		containerID = containerID.decode('utf-8').strip()
		self.containerID = containerID
		if (containerID != ''):
			print('Docker created successfully.')
		else:
			raise ValueError('Creating docker did not return an ID.')
		return containerID

	def compileExecCode(self):
		try:
			#Executes the snippet inside the docker.
			execList = ['docker', 'exec', self.containerID, 'python3', '/root/snippet']
			execList = execList + self.submission.testInput.split( )

			#Records time execution started
			startTime = getCurrentTime()

			snippetOutput = subprocess.check_output(execList)

			#Record execution time
			self.submission.runtime = getCurrentTime() - startTime
			print('Execution time: ' + str(self.submission.runtime))

			snippetOutput = snippetOutput.decode('utf-8').strip()
		#Handles runtime errors
		except subprocess.CalledProcessError:
			return 'runtime error' #No compilation errors with python as it is an interpreted language.
		return snippetOutput

#Method for constructing the appropriate Sandbox based on the submission.
#This method can be extended if more classes are added.
def sandboxFactory(submission):
	sandbox = None
	if (submission.language == 'java'):
		sandbox = Java(submission)
	elif (submission.language == 'sql'):
		sandbox = SQL(submission)
	elif (submission.language == 'c'):
		sandbox = C(submission)
	elif (submission.language == 'python2'):
		sandbox = Python2(submission)
	elif (submission.language == 'python3'):
		sandbox = Python3(submission)
	elif (submission.language == 'cpp'):
		sandbox = CPP(submission)
	else:
		raise ValueError('An invalid language was specified.')

	return sandbox;