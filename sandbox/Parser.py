#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

import re

def strip_comments(code,lang):
	#Strip strings
	code = re.sub('\"[^\"^\n]+\"', '', code)

	#Strip carriage returns
	code = re.sub('\r', '', code)

	# Strip regex and other characters
	code = re.sub('\'[^\'^\n]+\'', '', code)

	# Strip comments depending on language
	if (lang == 'java'):
		code = re.sub('//[^\n]+\n', '\n', code)
		code = re.sub('/\*[^\*/]+\*/', '', code)

	elif (lang == 'sql'):
		code = re.sub('--[^\n]+\n', '\n', code)
		code = re.sub('/\*[^\*/]+\*/', '', code)

	elif (lang == 'c'):
		code = re.sub('//[^\n]+\n', '\n', code)
		code = re.sub('/\*[^\*/]+\*/', '', code)

	elif (lang == 'python2'):
		code = re.sub('#[^\n]+\n', '\n', code)

	elif (lang == 'python3'):
		code = re.sub('#[^\n]+\n', '\n', code)

	elif (lang == 'cpp'):
		code = re.sub('//[^\n]+\n', '\n', code)
		code = re.sub('/\*[^\*/]+\*/', '', code)

	return code

# Checks code for illegal arguments
def parse_snippet(code, lang):
	error = ''
	index = 0
	stack = []
	code = strip_comments(code,lang)
	print(code)

	words = re.split(' |\n',code)
		# Check indentation
	lines = re.split('\n',code)
	spaces = -1 # -1 is unset, 0 is spaces, 1 is tabs
	indentationSize = 0 # number of spaces/tabs per indent
	for line in lines:

		if(spaces == -1):
			if(line.startswith(' ')):
				spaces = 0
				indentationSize = len(line) - len(line.lstrip()) # index of first non-whitespace character
			elif(line.startswith('\t')):
				spaces = 1
				indentationSize = len(line) - len(line.lstrip())

		else: # Spaces/Tabs auto detected
			if(line.startswith(' ') and spaces == 0):
				if(((len(line) - len(line.lstrip())) % indentationSize) != 0):
					error = 3
			elif(line.startswith('\t') and spaces == 1):
				if(((len(line) - len(line.lstrip())) % indentationSize) != 0):
					error = 3

	for cur in words:
		cur = cur.strip()
		
		# modify stack to add or remove a code block accordingly
		if '{' in cur:
			stack.append(cur)
		if '}' in cur:
			if not stack:
				# error = "code blocks do not line up"
				print("code blocks do not line up")
			else:
				# pop twice if the block was a switch statement
				if stack.pop() is "switch":
					stack.pop()
		# check for illegal functions
		if "goto" in cur:
			print("contains goto")
			error = "code contains illegal goto statement"
		if "switch" in cur:
			stack.append("switch")
		if "break" in cur:
			if len(stack) >= 2:
				# Check if inside the correct level of a switch statement
				last = stack.pop()
				last2 = stack.pop() 
				if last2 != "switch":
					print("contains illegal break")
					error = "code contains illegal break statement"
				# re stack code blocks
				stack.append(last)
				stack.append(last2)
			else:
				print("contains illegal break")
				error = "code contains illegal break"
		++index

	# Check that the stack is empty if not compiler will error anyway
	if stack:
		# error = 1
		print("Not all block structures are closed")

	# Check indentation
	lines = re.split('\n',code)
	spaces = -1 # -1 is unset, 0 is spaces, 1 is tabs
	indentationSize = 0 # number of spaces/tabs per indent
	for line in lines:

		if(spaces == -1):
			if(line.startswith(' ')):
				spaces = 0
				indentationSize = len(line) - len(line.lstrip()) # index of first non-whitespace character
			elif(line.startswith('\t')):
				spaces = 1
				indentationSize = len(line) - len(line.lstrip())

		else: # Spaces/Tabs auto detected
			if(line.startswith(' ') and spaces == 0):
				if(((len(line) - len(line.lstrip())) % indentationSize) != 0):
					error = "incorrect indentation consistency"
			elif(line.startswith('\t') and spaces == 1):
				if(((len(line) - len(line.lstrip())) % indentationSize) != 0):
					error = "incorrect indentation consistency"
	return error


def test_code(code,lang):
	error = parse_snippet(code,lang)
	if error == '':
		print("valid")
	else:
		print("illegal convention "+error)
