#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

import APIInterface
import time
import threading
import re
import configparser
from Parser import parse_snippet
from Sandbox import Sandbox, sandboxFactory
from Submission import Submission

#Constructs configuration object and reads contents from configuration.ini
Configuration = configparser.ConfigParser()
Configuration.read('configuration.ini')

#Defines sleep between certain parts in program for verbosity
CONST_SUBMISSION_CHECK_SLEEP = 1

#Reads max threads from configuration file
CONST_MAX_THREADS = int(Configuration['Queue']['MaximumThreads'])

WHITE  = '\033[0m'
RED  = '\033[31m'
GREEN  = '\033[32m'
ORANGE  = '\033[33m'
BLUE  = '\033[34m'
PURPLE  = '\033[35m'

#Function which strips spaces from string and converts to lowercase.
def convertString(inString):
	#Removes spaces
	outString = inString.replace(" ", "")
	
	#Converts to lowercase
	outString = outString.lower()
	
	#Removes carrige returns
	outSring = outString.replace("\r","")
	
	#Removes new lines
	outString = outString.replace("\n","")
	
	#Removes tabs
	outString = outString.replace("\t","")
	
	return outString

#Function which is run by threads.
def runSubmission(submission):
	#Prints to console
	print(GREEN, end='')
	print('Running submission ' + str(submission.get('submission_id')) + '...')
	print(WHITE, end='')

	#Gets the assessment corresponding to the submission from the API
	assessment = APIInterface.getAssessment(submission.get('assessment_id'))

	#Adds the required data to the submission container object.
	submissionContainer = Submission(submission)
	submissionContainer.language = assessment.get('language')
	submissionContainer.expectedOutput = assessment.get('expected_output')
	submissionContainer.testInput = assessment.get('test_input')
	
	#Prints the submission content.
	print(BLUE, end='')
	submissionContainer.printSubmission()
	print(WHITE, end='')

	# Checks for illegal coding conventions
	error = parse_snippet(submissionContainer.submission, submissionContainer.language)
	if error == "":
	
		#Constructs a sandbox determined on the language of the assessment.
		sandbox = sandboxFactory(submissionContainer)
		
		#Run the code simulation
		try:
			sandboxOutput = sandbox.runSimulation()
		except ValueError:
			print('There was an error running the simulation.')
			raise

		#Print expected output to console.
		print(GREEN, end='')
		print('Expected Output: ' + submissionContainer.expectedOutput)
		print(WHITE, end='')

		submissionContainer.output = sandboxOutput

	#Check and print outcome to console.
	strippedExpectedOutput = convertString(submissionContainer.expectedOutput)
	strippedOutput = convertString(sandboxOutput)

	#If parser reported an error
	if error != "":
		print(RED, end='')
		print('Illegal coding conventions')
		submissionContainer.marks=0
		submissionContainer.runtime=0
		submissionContainer.memeoryUsage=0
		submissionContainer.feedback='Submission incorrect.'
		submissionContainer.output = 'Pending manual evaluation - '+error
		submissionContainer.isMarked = 1
		print(WHITE, end='')
	#Else if the output matches the expected
	elif(strippedExpectedOutput == strippedOutput):
		print(GREEN, end='')
		print('Output matches expected output. Marking correct.')

		#Set marks to the assessments max marks
		submissionContainer.marks=assessment.get('marks')

		#Set running output to required format
		submissionContainer.output = 'Output (' + sandboxOutput + ') matched expected output'

		#Set marked to complete
		submissionContainer.isMarked = 1

		print(WHITE, end='')
	#Else (Output does not match expected)
	else:
		print(RED, end='')
		print('Output does not match. Marking incorrect')

		#Set marks to  0
		submissionContainer.marks=0

		#Set running output to required format
		submissionContainer.output = 'Pending manual evaluation - output (' + sandboxOutput + ') does not match expected'

		#Set marked to requires manual marking
		submissionContainer.isMarked = 1
		print(WHITE, end='')

	#Prints the submission content.
	print(BLUE, end='')
	submissionContainer.printSubmission()
	print(WHITE, end='')
	print(GREEN, end='')

	#Updates the submission by passing data to the API
	print('Passing submission data to API')
	APIInterface.updateSubmission(submissionContainer.submissionID,
		submissionContainer.marks,
		submissionContainer.feedback,
		submissionContainer.isMarked,
		submissionContainer.runtime,
		submissionContainer.output,
		submissionContainer.memoryUsage)
	print(WHITE, end='')

	print(ORANGE, end='')
	print('Thread exitings.')
	print(WHITE, end='')

#Function which removes completed workers if they have finished running
def removeCompletedWorkers(workers):
	#List of workers to remove
	completedWorkers = []

	#If the worker is not alive, add it to the list
	for worker in workers:
		if not worker.isAlive():
			completedWorkers.append(worker)

	#Prints for verbosity.
	print('Removing completed workers.')
	
	#Iterates over the list and removes the workers.
	for worker in completedWorkers:
		print('Removing worker')
		workers.remove(worker)

#Class for the SandboxQueue
class SandboxQueue:
	#Constructor
	def __init__(self, testModeEnabled):
		#Grabs initial unmarked submissions
		submissions = self.getSubmissions()

		#Initialises variables
		runCount = 0
		successCount = 0
		testingEnabled = testModeEnabled
		
		#Stores list of workers currently running
		workers = []

		#Loops continuously
		while True:

			#While there are submissions in the queue
			while not submissions.empty():
				print('Submission found.')
				time.sleep(CONST_SUBMISSION_CHECK_SLEEP)

				#Remove submission from queue
				submission = submissions.get()
				print('Retreived submission (id: ' + str(submission.get('submission_id')) + ')')

				#Spin until thread is available
				while (len(workers) == CONST_MAX_THREADS):
					print('There are no more threads available.')
					time.sleep(1)
					
					#Check if any workers have finished and remove them if so.
					removeCompletedWorkers(workers)

				#Create worker
				worker = threading.Thread(target=runSubmission, args=(submission,))
				workers.append(worker)
				worker.start()
				time.sleep(1)

			#Spin until workers is empty.
			while workers:
				removeCompletedWorkers(workers)
				time.sleep(1)

			#Prints verbose message if testing is enabled
			if (testingEnabled):
				if(runCount == successCount):
					print(GREEN, end='')
					print('All simulations completed successfully!')
					print(WHITE, end='')
				else:
					print(RED, end='')
					print('Not all simulations completed successfully!')
					print(WHITE, end='')
				break

			#Prints for verbosity.
			print('There are no more submissions, getting more submissions')
			
			#Gets more submissions
			submissions = self.getSubmissions()
			time.sleep(CONST_SUBMISSION_CHECK_SLEEP)



	#Grabs submissions from the API as a queue.
	def getSubmissions(self):
		submissions = APIInterface.getSubmissions()
		return submissions
