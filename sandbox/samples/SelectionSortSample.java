import java.util.Random;

public class SelectionSortSample {
	public static int[] selSort(int[] arr){
         
        for (int i = 0; i < arr.length - 1; i++)
        {
            int index = i;
            for (int j = i + 1; j < arr.length; j++)
                if (arr[j] < arr[index]) 
                    index = j;
      
            int smallerNumber = arr[index];  
            arr[index] = arr[i];
            arr[i] = smallerNumber;
        }
        return arr;
    }

	public static void main(String[] args) {
		Random rand = new Random();
		int[] data = new int[10000];

		for (int i = 0; i < data.length; i++)
		{
			data[i] = rand.nextInt(100) + 1;
		}

		selSort(data);

		for (int i = 0; i < data.length; i++)
		{
			System.out.println(data[i]);
		}


	}
}