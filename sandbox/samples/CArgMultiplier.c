#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	int result = 0;

	if(argc > 1) {
		result = 1;
	}

	for (int i = 1; i < argc; i++)
	{
		result = result * atoi(argv[i]);
	}
	
	printf("%d\n", result);

	return 0;
}