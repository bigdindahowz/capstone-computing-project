public class JavaArgMultiplier {
	public static void main(String[] args) {
		int result = 0;

		if (args.length > 0) {
			result = 1;
		}

		for (String arg: args) {
			result = result * Integer.parseInt(arg);
		}

		System.out.println(result);
	}
}