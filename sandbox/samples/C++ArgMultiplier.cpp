#include <stdlib.h>
#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
	int result = 0;

	if(argc > 1) {
		result = 1;
	}

	for (int i = 1; i < argc; i++)
	{
		result = result * atoi(argv[i]);
	}
	
	cout << result;
	cout << '\n';

	return 0;
}