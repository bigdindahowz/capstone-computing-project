from sys import argv

argv.pop(0)

result = 0

if (len(argv) > 0):
	result = 1

for arg in argv:
	result = result * int(arg)

print (result)