#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

import unittest
import re
import _thread
import time
from Sandbox import Sandbox, sandboxFactory
from Submission import Submission

#Test class for testing objects in the Sandbox.py python file.
class SandboxTest(unittest.TestCase):
    #Runs a basic hello world test. Passes a java program to the docker to print.
    #Checks that the output matches 'Hello world!'
    def javaHelloWorld(self):
        print('====\nRunning Java Hello World Test.')
        submission = Submission(None)

        submission.submission = open('samples/JavaHelloWorld.java', 'r').read()
        submission.language = 'java'

        testSandbox = sandboxFactory(submission)
        
        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        
        self.assertEqual(snippetOutput, 'Hello world!')
        print('Running Time: ' + str(submission.runtime))
        print('Memory Usage: ' + str(submission.memoryUsage))
        time.sleep(10)
        print('Test Succeeded.\n====\n')

    #Runs a basic maths test. Passes a math program to the docker.
    #Compares the output with expected.
    def javaMath(self):
        print('====\nRunning Java Math Test.')
        submission = Submission(None)

        submission.submission = open('samples/JavaMath.java', 'r').read()
        submission.language = 'java'

        testSandbox = sandboxFactory(submission)

        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        self.assertEqual(snippetOutput, '1000')
        print('Test Succeeded.\n====\n')

    #Runs a Java selection sort to see how long a program with lots of instructions takes.
    def javaSelectionSortSample(self):
        print('====\nRunning Java Selection Sort Test.')
        submission = Submission(None)
        submission.submission = open('samples/SelectionSortSample.java', 'r').read()
        submission.language = 'java'
        testSandbox = sandboxFactory(submission)
        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        print('Test Succeeded.\n====\n')

    #Runs an SQL test. Passes a file containing the table structure.
    #Runs a select query and compares result with expected.
    def sqlSelectStudents(self):
        print('====\nRunning SQL Select Test.')
        
        submission = Submission(None)
        submission.submission = 'SELECT * FROM Students;'
        submission.language = 'sql'

        testSandbox = sandboxFactory(submission)

        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise

        strippedOutput = re.sub(r"[\n\t\s]*", "", snippetOutput)
        self.assertEqual(strippedOutput, 'StudentIDStudentFirstnameStudentLastname1ChrisElliott2StephTravaglini3GeorgeHaynes')
        print('Test Succeeded.\n====\n')

    #Runs C program to do basic math.
    def cMath(self):
        print('====\nRunning C Math Test.')
        
        submission = Submission(None)
        submission.submission = open('samples/CMath.c', 'r').read()
        submission.language = 'c'

        testSandbox = sandboxFactory(submission)

        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise

        strippedOutput = re.sub(r"[\n\t\s]*", "", snippetOutput)
        self.assertEqual(strippedOutput, '54')
        print('Test Succeeded.\n====\n')

    #Runs C program with invalid syntax.
    def cInvalidSyntax(self):
        print('====\nRunning C Invalid Syntax Test.')
        
        submission = Submission(None)
        submission.submission = open('samples/CInvalidSyntax.c', 'r').read()
        submission.language = 'c'

        testSandbox = sandboxFactory(submission)

        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise

        self.assertEqual(snippetOutput, 'compilation error')
        print('Test Succeeded.\n====\n')

    #Runs C program with memory leaks.
    def cLeaky(self):
        print('====\nRunning C Leaky Test.')
        
        submission = Submission(None)
        submission.submission = open('samples/CLeaky.c', 'r').read()
        submission.language = 'c'

        testSandbox = sandboxFactory(submission)

        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise

        self.assertEqual(snippetOutput, 'memory leaks are present')
        print('Test Succeeded.\n====\n')

    #Runs python2 hello world.
    def python2HelloWorld(self):
        print('====\nRunning Python2 Hello World Test.')
        submission = Submission(None)

        submission.submission = open('samples/PythonHelloWorld.py', 'r').read()
        submission.language = 'python2'

        testSandbox = sandboxFactory(submission)
        
        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        
        self.assertEqual(snippetOutput, 'Hello world!')
        print('Test Succeeded.\n====\n') 

    #Runs python2 math.
    def python2SimpleMath(self):
        print('====\nRunning Python2 Simple Math Test.')
        submission = Submission(None)

        submission.submission = open('samples/PythonSimpleMath.py', 'r').read()
        submission.language = 'python2'

        testSandbox = sandboxFactory(submission)
        
        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        
        self.assertEqual(snippetOutput, '59')
        print('Test Succeeded.\n====\n') 

    #Runs python2 runtime error.
    def python2FaultyCode(self):
        print('====\nRunning Python2 Faulty Code.')
        submission = Submission(None)

        submission.submission = open('samples/PythonFaultyCode.py', 'r').read()
        submission.language = 'python2'

        testSandbox = sandboxFactory(submission)
        
        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        
        self.assertEqual(snippetOutput, 'runtime error')
        print('Test Succeeded.\n====\n') 

    #Runs python3 hello world.
    def python3HelloWorld(self):
        print('====\nRunning Python3 Hello World Test.')
        submission = Submission(None)

        submission.submission = open('samples/PythonHelloWorld.py', 'r').read()
        submission.language = 'python3'

        testSandbox = sandboxFactory(submission)
        
        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        
        self.assertEqual(snippetOutput, 'Hello world!')
        print('Test Succeeded.\n====\n') 

    #Runs python3 math.
    def python3SimpleMath(self):
        print('====\nRunning Python3 Simple Math Test.')
        submission = Submission(None)

        submission.submission = open('samples/PythonSimpleMath.py', 'r').read()
        submission.language = 'python3'

        testSandbox = sandboxFactory(submission)
        
        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        
        self.assertEqual(snippetOutput, '59.85')
        print('Test Succeeded.\n====\n')

    #Runs python3 runtime error.
    def python3FaultyCode(self):
        print('====\nRunning Python3 Faulty Code.')
        submission = Submission(None)

        submission.submission = open('samples/PythonFaultyCode.py', 'r').read()
        submission.language = 'python3'

        testSandbox = sandboxFactory(submission)
        
        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        
        self.assertEqual(snippetOutput, 'runtime error')
        print('Test Succeeded.\n====\n')

    #Runs C++ hello world.
    def cppHelloWorld(self):
        print('====\nRunning C++ Hello World.')
        submission = Submission(None)

        submission.submission = open('samples/C++HelloWorld.cpp', 'r').read()
        submission.language = 'cpp'

        testSandbox = sandboxFactory(submission)
        
        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        
        self.assertEqual(snippetOutput, 'Hello World!')
        print('Test Succeeded.\n====\n')

    #Runs C++ basic math.
    def cppBasicMath(self):
        print('====\nRunning C++ Basic Math.')
        submission = Submission(None)

        submission.submission = open('samples/C++BasicMath.cpp', 'r').read()
        submission.language = 'cpp'

        testSandbox = sandboxFactory(submission)
        
        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        
        self.assertEqual(snippetOutput, '2050')
        print('Test Succeeded.\n====\n')

    #Runs C++ compilation error.
    def cppInvalidSyntax(self):
        print('====\nRunning C++ Invalid Syntax.')
        submission = Submission(None)

        submission.submission = open('samples/C++InvalidSyntax.cpp', 'r').read()
        submission.language = 'cpp'

        testSandbox = sandboxFactory(submission)
        
        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        
        self.assertEqual(snippetOutput, 'compilation error')
        print('Test Succeeded.\n====\n')

    #Runs C++ memory leak.
    def cppLeaky(self):
        print('====\nRunning C++ Memory Leak.')
        submission = Submission(None)

        submission.submission = open('samples/C++Leaky.cpp', 'r').read()
        submission.language = 'cpp'

        testSandbox = sandboxFactory(submission)
        
        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        
        self.assertEqual(snippetOutput, 'memory leaks are present')
        print('Test Succeeded.\n====\n')

    #Runs python3 infinite spinning code.
    def python3Spinning(self):
        print('====\nRunning Python3 Spinning Code.')
        submission = Submission(None)

        submission.submission = open('samples/PythonSpinning.py', 'r').read()
        submission.language = 'python3'

        testSandbox = sandboxFactory(submission)
        
        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        
        self.assertEqual(snippetOutput, 'execution time exceeded threshold')
        print('Test Succeeded.\n====\n')

    #Runs C infinite spinning code.
    def cLoop(self):
        print('====\nRunning C Loop Test.')
        
        submission = Submission(None)
        submission.submission = open('samples/CLoop.c', 'r').read()
        submission.language = 'c'

        testSandbox = sandboxFactory(submission)

        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise

        self.assertEqual(snippetOutput, 'execution time exceeded threshold')
        print('Test Succeeded.\n====\n')

    #Runs Java infinite spinning code.
    def javaLoop(self):
        print('====\nRunning Java Loop Test.')
        submission = Submission(None)

        submission.submission = open('samples/JavaLoop.java', 'r').read()
        submission.language = 'java'

        testSandbox = sandboxFactory(submission)
        
        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        
        self.assertEqual(snippetOutput, 'execution time exceeded threshold')
        print('Test Succeeded.\n====\n')

    #Runs C program with arguments.
    def cArgMultiplier(self, inTestInput, inExpectedResult):
        print('====\nRunning C Argument Multiplier Test.')
        
        submission = Submission(None)
        submission.submission = open('samples/CArgMultiplier.c', 'r').read()
        submission.language = 'c'
        submission.testInput = inTestInput

        testSandbox = sandboxFactory(submission)

        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise

        strippedOutput = re.sub(r"[\n\t\s]*", "", snippetOutput)
        self.assertEqual(strippedOutput, inExpectedResult)
        print('Test Succeeded.\n====\n')

    #Runs C++ program with arguments.
    def cppArgMultiplier(self, inTestInput, inExpectedResult):
        print('====\nRunning C++ Argument Multiplier Test.')
        
        submission = Submission(None)
        submission.submission = open('samples/C++ArgMultiplier.cpp', 'r').read()
        submission.language = 'cpp'
        submission.testInput = inTestInput

        testSandbox = sandboxFactory(submission)

        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise

        strippedOutput = re.sub(r"[\n\t\s]*", "", snippetOutput)
        self.assertEqual(strippedOutput, inExpectedResult)
        print('Test Succeeded.\n====\n')

    #Runs Java program with arguments.
    def javaArgMultiplier(self, inTestInput, inExpectedResult):
        print('====\nRunning Java Argument Multiplier Test.')
        
        submission = Submission(None)
        submission.submission = open('samples/JavaArgMultiplier.java', 'r').read()
        submission.language = 'java'
        submission.testInput = inTestInput

        testSandbox = sandboxFactory(submission)

        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise

        strippedOutput = re.sub(r"[\n\t\s]*", "", snippetOutput)
        self.assertEqual(strippedOutput, inExpectedResult)
        print('Test Succeeded.\n====\n')

    #Runs python2 program with arguments.
    def python2ArgMultiplier(self, inTestInput, inExpectedResult):
        print('====\nRunning Python2 Argument Multiplier Test.')
        
        submission = Submission(None)
        submission.submission = open('samples/PythonArgMultiplier.py', 'r').read()
        submission.language = 'python2'
        submission.testInput = inTestInput

        testSandbox = sandboxFactory(submission)

        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise

        strippedOutput = re.sub(r"[\n\t\s]*", "", snippetOutput)
        self.assertEqual(strippedOutput, inExpectedResult)
        print('Test Succeeded.\n====\n')

    #Runs python3 program with arguments.
    def python3ArgMultiplier(self, inTestInput, inExpectedResult):
        print('====\nRunning Python3 Argument Multiplier Test.')
        
        submission = Submission(None)
        submission.submission = open('samples/PythonArgMultiplier.py', 'r').read()
        submission.language = 'python2'
        submission.testInput = inTestInput

        testSandbox = sandboxFactory(submission)

        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise

        strippedOutput = re.sub(r"[\n\t\s]*", "", snippetOutput)
        self.assertEqual(strippedOutput, inExpectedResult)
        print('Test Succeeded.\n====\n')

    #Runs C program which infinitely allocates memory.
    def cMemoryHog(self):
        print('====\nRunning C Memory Hog Test.')
        
        submission = Submission(None)
        submission.submission = open('samples/CMemoryHog.c', 'r').read()
        submission.language = 'c'

        testSandbox = sandboxFactory(submission)

        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise

        self.assertEqual(snippetOutput, 'execution time exceeded threshold')
        print('Test Succeeded.\n====\n')

    #Runs Java program which includes a package.
    def javaPackageInclude(self):
        print('====\nRunning Java Package Include Test.')
        submission = Submission(None)

        submission.submission = open('samples/JavaPackages.java', 'r').read()
        submission.language = 'java'

        testSandbox = sandboxFactory(submission)
        
        try:
            snippetOutput = testSandbox.runSimulation()
        except ValueError:
            print('There was an error running the simulation.')
            raise
        
        self.assertEqual(snippetOutput, 'Some Program')
        print('Running Time: ' + str(submission.runtime))
        print('Memory Usage: ' + str(submission.memoryUsage))
        time.sleep(10)
        print('Test Succeeded.\n====\n')

#Main method for coordinating everything.
def main():
    sandbox = SandboxTest()
    
    #sandbox.cMemoryHog()
    sandbox.javaPackageInclude()
    sandbox.javaLoop()
    sandbox.javaHelloWorld()
    sandbox.javaMath()
    sandbox.javaSelectionSortSample()
    #sandbox.sqlSelectStudents()
    sandbox.cLoop()
    sandbox.cMath()
    sandbox.cInvalidSyntax()
    sandbox.cLeaky()
    sandbox.python2HelloWorld()
    sandbox.python2SimpleMath()
    sandbox.python2FaultyCode()
    sandbox.python3HelloWorld()
    sandbox.python3SimpleMath()
    sandbox.python3FaultyCode()
    sandbox.python3Spinning()
    sandbox.cppHelloWorld()
    sandbox.cppBasicMath()
    sandbox.cppInvalidSyntax()
    sandbox.cppLeaky()
    sandbox.cArgMultiplier('8 10', '80')
    sandbox.cArgMultiplier('', '0')
    sandbox.cArgMultiplier('90', '90')
    sandbox.cArgMultiplier('1 2 3 4 5 6 7 8 9', '362880')
    sandbox.cppArgMultiplier('8 10', '80')
    sandbox.cppArgMultiplier('', '0')
    sandbox.cppArgMultiplier('90', '90')
    sandbox.cppArgMultiplier('1 2 3 4 5 6 7 8 9', '362880')
    sandbox.javaArgMultiplier('8 10', '80')
    sandbox.javaArgMultiplier('', '0')
    sandbox.javaArgMultiplier('90', '90')
    sandbox.javaArgMultiplier('1 2 3 4 5 6 7 8 9', '362880')
    sandbox.python2ArgMultiplier('8 10', '80')
    sandbox.python2ArgMultiplier('', '0')
    sandbox.python2ArgMultiplier('90', '90')
    sandbox.python2ArgMultiplier('1 2 3 4 5 6 7 8 9', '362880')
    sandbox.python3ArgMultiplier('8 10', '80')
    sandbox.python3ArgMultiplier('', '0')
    sandbox.python3ArgMultiplier('90', '90')
    sandbox.python3ArgMultiplier('1 2 3 4 5 6 7 8 9', '362880')
    print('All tests passed successfully.')

if __name__ == "__main__":
    main()