#!/usr/bin/python3

#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

import os
from SandboxQueue import SandboxQueue

#Changes the directory to the sandbox directory in the root account.
os.chdir('/root/sandbox')

def main():
        #Create SandboxQueue object with testing enabled.
        queue = SandboxQueue(False)

if __name__ == "__main__":
    main()
