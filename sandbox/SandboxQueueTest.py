#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

from SandboxQueue import SandboxQueue

def main():
	#Create SandboxQueue object with testing enabled.
	queue = SandboxQueue(True)

if __name__ == "__main__":
    main()