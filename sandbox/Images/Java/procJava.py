#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

import os
import shutil
import subprocess
import time
from sys import argv

def getCurrentTime():
	return int(round(time.time()*1000))

CONST_SNIPPET_PATH = '/root/snippet'
CONST_MOVE_DEST = '/root/'
CONST_SCRIPT_PATH = '/root/'

argv.pop(0)

os.chdir(CONST_SCRIPT_PATH)

snippet = open(CONST_SNIPPET_PATH,"r")
classNameFound = 'false'
className = ''

while (classNameFound == 'false'):
        line = snippet.readline()
        if 'public class' in line:
                classNameFound = 'true'
                className = line.split()[2]

        if not line:
                break

if (className == ''):
        print('Valid class name was not found in uploaded Java file')
        exit()

shutil.move(CONST_SNIPPET_PATH, CONST_MOVE_DEST + className + '.java')

compileOutput = subprocess.check_output(['javac', '-cp', '/root/packages/*', className + '.java'])

if (compileOutput.decode('utf-8').strip() != ''):
        print(compileOutput)
        exit()

execList = ['java', className]
execList = execList + argv

startTime = getCurrentTime()

snippetOutput = subprocess.check_output(execList)

runtime = getCurrentTime() - startTime

file = open('/root/runtime', 'w' )
file.write(str(runtime))
file.close()

snippetOutput = snippetOutput.decode('utf-8').strip()

print(snippetOutput)
