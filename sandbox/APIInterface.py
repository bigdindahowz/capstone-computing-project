#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

import json
import time
import requests
import signature_creator
import queue
import configparser

#Constructs configuration object and reads contents from configuration.ini
Configuration = configparser.ConfigParser()
Configuration.read('configuration.ini')

#Reads configuration variables in from file
CONST_TEST_KEY = Configuration['API']['APIKey']
CONST_TEST_SECRET = Configuration['API']['APISecret']
CONST_URI = Configuration['API']['APIUrl']

#Function which gets a submission from the API based on ID
def getSubmission(id):
	params = {'submission_id': id,'signature_timestamp': int(time.time())}
	signature = signature_creator.SignatureCreator(CONST_TEST_SECRET).sign_request(params)
	header = {"Authorization":CONST_TEST_KEY, "Signature":signature}
	result = requests.get(CONST_URI+'submission', params=params, headers=header)
	return eval(result.text)

#Function which gets an assessment from the API based on ID
def getAssessment(id):
	params = {'assessment_id': id,'signature_timestamp': int(time.time())}
	signature = signature_creator.SignatureCreator(CONST_TEST_SECRET).sign_request(params)
	header = {"Authorization":CONST_TEST_KEY, "Signature":signature}
	result = requests.get(CONST_URI+'assessment', params=params, headers=header)
	return eval(result.text)

#Function for updating a submission in the API. The submission which is updated is based on the id passed in.
def updateSubmission(id, mark, feedback, is_marked, running_time, running_output, memory_usage):
	params = {'submission_id': id, 'mark': mark, 'is_marked': is_marked, 'running_time': running_time, 'running_output': running_output, 'memory_usage': memory_usage, 'signature_timestamp': int(time.time())}
	signature = signature_creator.SignatureCreator(CONST_TEST_SECRET).sign_request(params)
	header = {"Authorization":CONST_TEST_KEY, "Signature":signature}
	result = requests.put(CONST_URI+'submission', params=params, headers=header)

#Function which gets all submissions from the API, checks whether they are unmarked and if so, adds them to a queue object.
def getSubmissions():
	params = {'signature_timestamp': int(time.time())}
	signature = signature_creator.SignatureCreator(CONST_TEST_SECRET).sign_request(params)
	header = {"Authorization":CONST_TEST_KEY, "Signature":signature}
	result = requests.get(CONST_URI+'submissions', params=params, headers=header)
	
	#all submissions from API
	submissions =  eval(result.text)
	submissionsQueue = queue.Queue()
	
	#for each submission, if it is yet to be marked, add it to the queue.
	for submission in submissions:
		if submission.get('is_marked') == 0:
			submissionsQueue.put(submission)

	return submissionsQueue
