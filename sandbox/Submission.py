#
# The Following Source Code is under a Creative Commons Attribution 4.0 International license (CC BY 4.0)
# https://creativecommons.org/licenses/by/4.0/
# You are free to Share - copy and redistribute the material in any medium or format
# Adapt - remix, transform, and build upon the material
# For any purpose, even commercially
#

class Submission:
	#Class variables
	submissionID = ""
	studentID = ""
	assessmentID = ""
	isMarked = ""
	marks = ""
	feedback = ""
	submission = ""
	output = ""
	runtime = ""
	memoryUsage = ""
	attemptsMade = ""

	#Class variables outside the submissions table.
	testInput=""
	expectedOutput=""
	language = ""

	#Constructor
	def __init__(self, submission):
		if (submission is not None):
			self.submissionID = submission.get('submission_id')
			self.studentID = submission.get('student_id')
			self.assessmentID = submission.get('assessment_id')
			self.isMarked = submission.get('is_marked')
			self.marks = submission.get('marks')
			self.feedback = submission.get('feedback')
			self.submission = submission.get('submission')
			self.output = submission.get('running_output')
			self.runtime = submission.get('running_time')
			self.memoryUsage = submission.get('memory_usage')
			self.attemptsMade = submission.get('attempts_made')

	#Prints Submission to shell
	def printSubmission(self):
		print('Submission ID: ' + str(self.submissionID))
		print('Student ID: ' + str(self.studentID))
		print('Assessment ID: ' + str(self.assessmentID))
		print('Marked: ' + str(bool(self.isMarked)))
		print('Marks: ' + str(self.marks))
		print('Feedback: ' + self.feedback)
		print('Submission: ' + self.submission)
		print('Output: ' + self.output)
		print('Runtime: ' + str(self.runtime))
		print('Memory Usage: ' + str(self.memoryUsage))
		print('Attempts Made: ' + str(self.attemptsMade))
		print('Test Input: ' + self.testInput)
		print('Expected Output: ' + self.expectedOutput)
		print('Language: ' + self.language)